#
# This set of functions is for initial conditions and debugging
#

import numpy as np
from numpy import *
from scipy import *
from numpy import fft
from scipy import fftpack
from numpy.linalg import inv
import matplotlib.pyplot as plt
import CosineSineTransforms as cst
import pdb #to pause execution for debugging use pdb.set_trace()


I = complex(0.,1.)


def setProblem_Layers(x_grid, z_grid, Nx, Nz, Lx, Lz, ParkRun, DiffusionFactor, nvars, RandomPert, PertFactor, N2=1):
 
    # Set physical constants
    g = 9.81
    ct = 2.0*10**(-4.)
    cs = 7.6*10**(-4.)

    # Physical Diffusion:
    nu = 1.*10**(-6.)
    kappat = 1.4*10**(-7.)
    kappas = 1.4*10**(-7.)
    nu = nu * DiffusionFactor
    kappat = kappat * DiffusionFactor
    kappas = kappas * DiffusionFactor

    #Typical density perturbations for Park et al lab run 13.
    #Scaled off plot:
    dgamma = 100./3
    dz_b = 2./100
    a0 = 100.
    z_a = Lz/2.
    rhoprime13 = dgamma*z_a + a0*dz_b + dgamma/2*dz_b

    #We now scale run 13 perturbations using the background 
    #density field:
    drho0_dz13 = -122.09
 
    if ParkRun == 14:
        N2 = 0.35			#Taken from Park et al paper
        drho0_dz = -31.976		#Scaled off Park et al plot
        rho0 = -g/N2*drho0_dz
        rhoprime = rhoprime13 * drho0_dz/drho0_dz13
    if ParkRun == 18:
        N2 = 3.83			#Taken from Park et al paper
        drho0_dz = -425.9		#Scaled off Park et al plot
        rho0 = -g/N2*drho0_dz
        rhoprime = rhoprime13 * drho0_dz/drho0_dz13
    if ParkRun < 1:			#For arbitrary N^2
        #Set ref density constant:
        N2_13 = 1.19               	#Taken from Park et al paper
        rho0_13 = -g/N2_13*drho0_dz13
        rho0 = rho0_13

        #assume perturbations are constant fraction of background density field:
        const = rhoprime13/drho0_dz13
    
        #Find initial background density field given some 
        #user defined N^2 and the constant ref density: 
        drho0_dz = -N2*rho0/g
        rhoprime = drho0_dz*const

    bs = -(1./(rho0*cs)*drho0_dz)
    if nvars == 3:
        #Make bt a small fraction of bs for testing.
        #Temperature serves to make fluid stably stratified but with minimal effect:
        DDfactor = 1000.
        bt = -bs/DDfactor
    else:
        bt = 0

    params = np.array([g, rho0, ct, cs, bt, bs, nu, kappat, kappas])

    if nvars == 3:
        Spert0 = (rhoprime/rho0)*1./(cs-ct/DDfactor)
        Tpert0 = Spert0/DDfactor
    else:
        Spert0 = rhoprime * 1./(rho0*cs)
        Tpert0 = 0

    if RandomPert == 1:
        RandomSample = np.loadtxt('./RandomSample_080_180.txt')
        RandomSample = RandomSample/np.max(RandomSample)
        Spert0_array = RandomSample*Spert0*PertFactor
        Tpert0_array = RandomSample*Tpert0*PertFactor
    else:
        #Impose a simple wave field
        N_waves_x = 2
        N_waves_z = 2
        z_array = np.zeros((Nx,Nz)) + z_grid
        x_array = np.zeros((Nx,Nz)) + x_grid[:,np.newaxis]

        Spert0_array = np.sin(N_waves_x*2*np.pi/Lx*x_array)*np.sin(N_waves_z*2*np.pi/Lz*z_array)*Spert0/PertFactor
        Tpert0_array = np.sin(N_waves_x*2*np.pi/Lx*x_array)*np.sin(N_waves_z*2*np.pi/Lz*z_array)*Tpert0/PertFactor 
        #rhoprime_array = np.sin(N_waves_x*2*np.pi/Lx*x_array)*np.sin(N_waves_z*2*np.pi/Lz*z_array)*rhoprime/PertFactor 

    STop = Spert0*PertFactor
    Sbase = -bs*(z_grid-Lz) + STop
    #Note this range of salinity is physical (i.e., exists in Thalassic series)
    #It is not possible to use a physical range of pure water temperature to mimic the Park et al
    #lab experiment, where salinity was used to stratify a thermally uniform fluid.
    #I am not certain what they did in the lab, but I assume it was easiest for them to
    #increase salinity from pure water to stratify their fluid (i.e., set STop=0).
 
    #This part is only for plotting purposes - the model uses perturbation fields.
    SS_ff = Spert0_array + Sbase
    #The Python syntax means that this operation will add the vector Sbase to each row of Sprime0_array.
    #Since Python array order is (rows,columns) and we have (Nx,Nz), this operation is doing what we want.
    #It operates in a loop over i like Sprime0_array[i,:] + Sbase (each row of Sprime0_array has Nz values).

    if nvars == 3: 
        TTop = 0 
        Tbase = -bt*(z_grid-Lz) + TTop
        TT_ff  = Tpert0_array + Tbase

    Psi = np.zeros((Nx,Nz))
    TT  = Tpert0_array
    SS  = Spert0_array

    return np.array([TT, SS, Psi, params])
  

def setProblem_KelvinHelmholtz(x_grid, z_grid, Nx, Nz, Lx, Lz, ParkRun, MolecularDiffusion):
  
    #Barotropic or baroclinic Kelvin Helmholtz case, with w=0.

    # Set physical constants
    g = 9.81
    ct = 2.0*10**(-4.)
    cs = 7.6*10**(-4.)

    if MolecularDiffusion == 1:
        nu = 1.*10**(-6.)
        kappat = 1.4*10**(-7.)
        kappas = 1.4*10**(-7.)
    else:
        nu = 1.*10**(-6.)
        kappat = 1.4*10**(-7.)
        kappas = 1.4*10**(-7.)
        nu = nu * 1.
        kappat = kappat * 1.
        kappas = kappas * 1.

    #Choose which Park et al lab run to use:
    if ParkRun == 18: rho0 = 1090.95075
    if ParkRun == 14: rho0 = 896.2416

    bt = 0.
    bs = 0.

    params = np.array([g, rho0, ct, cs, bt, bs, nu, kappat, kappas])
  
    TanhFnc = 1
    if TanhFnc == 1:
        speed = 0.01
        #dz_u = 0.004
        dz_u = 0.001
        du = speed*2  
        z_array = np.zeros((Nx,Nz)) + (z_grid-Lz/2.)/dz_u

        #The wind has a profile of the form: 
        wind_x = du/2.*np.tanh(z_array)
        #plt.plot(wind_x[0,:])
        #plt.show()

        #Define background psi (integral of wind profile):
        Psi = du*dz_u/2.*( np.log(2) - Lz/(2.*dz_u) + np.log(np.cosh(z_array)) )

        AddPert = 1
        if AddPert == 1:
            PsiAbsMax = np.max(np.abs(Psi))
            A1 = 0.05
            Psi_prime_max = PsiAbsMax*A1
            A2 = 0.2
            wavelength = Lx*A2  #wave perturbation is 4 cm long
            dx = Lx/Nx
            np4wave = int(wavelength/dx)  #for Nx=80, A2=0.2, we have 8 points for our wavelength
            k1 = 2*np.pi/wavelength
            Psi_prime = np.zeros((Nx,Nz))
            x_array = np.zeros((Nx,Nz)) + x_grid[:,np.newaxis]
            #Psi_prime[int(Nx/2.)-np4wave/2:int(Nx/2.)+np4wave/2-1,int(Nz/2.)] = Psi_prime_max*np.sin(k1*x_array[int(Nx/2.)-np4wave/2:int(Nx/2.)+np4wave/2-1,int(Nz/2.)])  
            Psi_prime[:,int(Nz/2.)] = Psi_prime_max*np.sin(k1*x_array[:,int(Nz/2.)])  

            Psi = Psi + Psi_prime
    else:
        speed = 0.005
        Psi = np.zeros((Nx,Nz)) 
        Psi[:,0:Nz/2-1] = -speed * Lz
        Psi[:,Nz/2:Nz]	= speed * Lz  

    TT  = np.zeros((Nx,Nz))
    SS  = np.zeros((Nx,Nz))

    return np.array([TT, SS, Psi, params])


def setProblem_SimpleWave(x_grid, z_grid, Nx, Nz, Lx, Lz):
    """ This function sets an intial condition and parameters for the 
    simple wave function for debugging"""
    TTop = .5
    TBot = -.5
    T0 = 0.
    STop = -1.
    SBot = 1.
    S0 = 0.
    DeltaTT = TTop-TBot
    DeltaSS = STop-SBot
    h0 = .5
    ct = .2
    cs = .01
    bt = 1.5
    bs = 0.5
    grav = 9.8
    rho0 = 1.
    nu = .25
    kappat = .2
    kappas = .2

    params = np.array([grav, rho0, ct, cs, bt, bs, nu, kappat, kappas])
#    TT  = set_TT_initial(DeltaTT,h0,Lz,x_grid,z_grid,Nx,Nz,T0,TTop,TBot,Lx)
#    SS  = set_SS_initial(DeltaSS,h0,Lz,x_grid,z_grid,Nx,Nz,S0,STop,SBot,Lx)
    Psi = set_f_simple(x_grid, z_grid, Nx, Nz, Lx, Lz)
    TT = set_f_simple(x_grid, z_grid, Nx, Nz, Lx, Lz)
    SS = set_f_simple(x_grid, z_grid, Nx, Nz, Lx, Lz)

    return np.array([TT, SS, Psi, params])

def set_v1_initial(x_grid,z_grid,N1,N2,Lx,Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (- sin(2.*np.pi*x_grid[k1]/Lx)*cos(np.pi*z_grid[k2]/Lz) 
                       +.7*cos(3.*2.*np.pi*x_grid[k1]/Lx)*cos(4.*np.pi*z_grid[k2]/Lz))
    return f

def set_v1_der_exact_x_z(x_grid,z_grid,N1,N2,Lx,Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] =  (2.*np.pi**2/Lx/Lz*cos(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz) 
                        +.7*(3.*2.*np.pi/Lx)*(4.*np.pi/Lz)*sin(3.*2.*np.pi*x_grid[k1]/Lx)*sin(4.*np.pi*z_grid[k2]/Lz))
    return f

def set_v1_der2_exact(x_grid,z_grid,N1,N2,Lx,Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] =  (-(2.*np.pi**2/Lx/Lz)**2*sin(2.*np.pi*x_grid[k1]/Lx)*cos(np.pi*z_grid[k2]/Lz) 
                        +.7*(3.*2.*np.pi/Lx)**2*(4.*np.pi/Lz)**2*cos(3.*2.*np.pi*x_grid[k1]/Lx)*cos(4.*np.pi*z_grid[k2]/Lz))
    return f

def set_v1_x_exact(x_grid,z_grid,N1,N2,Lx,Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] =  (-2.*np.pi/Lx*cos(2.*np.pi*x_grid[k1]/Lx)*cos(np.pi*z_grid[k2]/Lz) 
                        -.7*(3.*2.*np.pi/Lx)*sin(3.*2.*np.pi*x_grid[k1]/Lx)*cos(4.*np.pi*z_grid[k2]/Lz))
    return f

def set_v1_xx_exact(x_grid,z_grid,N1,N2,Lx,Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] =  ((2.*np.pi/Lx)**2*sin(2.*np.pi*x_grid[k1]/Lx)*cos(np.pi*z_grid[k2]/Lz) 
                        -.7*(3.*2.*np.pi/Lx)**2*cos(3.*2.*np.pi*x_grid[k1]/Lx)*cos(4.*np.pi*z_grid[k2]/Lz))
    return f

def set_v2_initial(x_grid,z_grid,N1,N2,Lx,Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (- cos(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz) +
                        .3*sin(2.*2.*np.pi*x_grid[k1]/Lx)*sin(5.*np.pi*z_grid[k2]/Lz))
    return f

def set_v2_der_exact_x_z(x_grid,z_grid,N1,N2,Lx,Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (2.*np.pi/Lx*sin(2.*np.pi*x_grid[k1]/Lx)*np.pi/Lz*cos(np.pi*z_grid[k2]/Lz) 
                        + .3*2.*2.*np.pi/Lx*5.*np.pi/Lz*cos(2.*2.*np.pi*x_grid[k1]/Lx)*cos(5.*np.pi*z_grid[k2]/Lz))
    return f

def set_v2_der2_exact(x_grid,z_grid,N1,N2,Lx,Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (-(2.*np.pi/Lx)**2*cos(2.*np.pi*x_grid[k1]/Lx)*(np.pi/Lz)**2*sin(np.pi*z_grid[k2]/Lz) 
                        + .3*(2.*2.*np.pi/Lx)**2*(5.*np.pi/Lz)**2*sin(2.*2.*np.pi*x_grid[k1]/Lx)*sin(5.*np.pi*z_grid[k2]/Lz))
    return f

def set_v2_x_exact(x_grid,z_grid,N1,N2,Lx,Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (2.*np.pi/Lx*sin(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz) 
                        + .3*2.*2.*np.pi/Lx*cos(2.*2.*np.pi*x_grid[k1]/Lx)*sin(5.*np.pi*z_grid[k2]/Lz))
    return f

def set_v2_xx_exact(x_grid,z_grid,N1,N2,Lx,Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = ((2.*np.pi/Lx)**2*cos(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz) 
                        - .3*(2.*2.*np.pi/Lx)**2*sin(2.*2.*np.pi*x_grid[k1]/Lx)*sin(5.*np.pi*z_grid[k2]/Lz))
    return f


#
#  These routines are for the full range of tanh 
#

def set_TT_initial(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,T0,TTop,TBot,Lx):
    f = np.zeros((N1,N2))
#
#   The linear stratification is then the last bit of each of the following equations and looks like,
#
#   T_bar = z/Lz T_top + (1-z/Lz) T_Bot
#
#   For this problem we are really working on the half interval because of the symmetries of the Fourier Cosine and Sine 
#   Series. I do this by passing a denser grid array. See group of functions below these T & S routines
#
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (DeltaTT/2.*tanh(2.*(z_grid[k2]-Lz/2.)/h0)-T0-z_grid[k2]/Lz*TTop-(1.-z_grid[k2]/Lz)*TBot)*sin(2.*np.pi*x_grid[k1]/Lx)
    return f

def set_TT_der(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,T0,TTop,TBot,Lx):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (DeltaTT/2.*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*2./h0-1./Lz*TTop+1./Lz*TBot)*2.*np.pi/Lx*cos(2.*np.pi*x_grid[k1]/Lx)
    return f

def set_TT_der2(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,T0,TTop,TBot,Lx):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = 4.*DeltaTT/h0/h0*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*tanh(2.*(z_grid[k2]-Lz/2.)/h0)*(2.*np.pi/Lx)**2*sin(2.*np.pi*x_grid[k1]/Lx)
    return f

def set_TT_z(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,T0,TTop,TBot,Lx):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (DeltaTT/2.*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*2./h0-1./Lz*TTop+1./Lz*TBot)*sin(2.*np.pi*x_grid[k1]/Lx)
    return f

def set_TT_zz(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,T0,TTop,TBot,Lx):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = -4.*DeltaTT/h0/h0*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*tanh(2.*(z_grid[k2]-Lz/2.)/h0)*sin(2.*np.pi*x_grid[k1]/Lx)
    return f

def set_SS_initial(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,S0,STop,SBot,Lx):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (-DeltaSS/2.*tanh(2.*(z_grid[k2]-Lz/2.)/h0)-S0+z_grid[k2]/Lz*STop+(1.-z_grid[k2]/Lz)*SBot)*sin(2.*np.pi*x_grid[k1]/Lx)
    return f

def set_SS_der(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,S0,STop,SBot,Lx):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (-DeltaSS/2.*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*2./h0+(1./Lz*STop-1./Lz*SBot))*2.*np.pi/Lx*cos(2.*np.pi*x_grid[k1]/Lx)
    return f

def set_SS_der2(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,S0,STop,SBot,Lx):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = -4.*DeltaSS/h0/h0*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*tanh(2.*(z_grid[k2]-Lz/2.)/h0)*(2.*np.pi/Lx)**2*sin(2.*np.pi*x_grid[k1]/Lx)
    return f

def set_SS_z(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,S0,STop,SBot,Lx):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (-DeltaSS/2.*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*2./h0+(1./Lz*STop-1./Lz*SBot))*sin(2.*np.pi*x_grid[k1]/Lx)
    return f

def set_SS_zz(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,S0,STop,SBot,Lx):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = 4.*DeltaSS/h0/h0*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*tanh(2.*(z_grid[k2]-Lz/2.)/h0)*sin(2.*np.pi*x_grid[k1]/Lx)
    return f

#
# These initial conditions are for the linear equation
#
def set_psi_linear_initial(x_grid,z_grid,N1,N2,Lx,Lz,t,Re,FrS,FrT,cs,ct):
    alpha = 1.
    n = 1.
    k = np.sqrt(np.pi*np.pi*(alpha*alpha+n*n))
    pp = -k*k/2./Re+np.sqrt(k**4/Re \
                            -4.*(cs*alpha*alpha*pi*pi/FrS/FrS)\
                                +ct*n*n*pi*pi/FrT/FrT)
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = - np.exp(pp*t)*\
                       sin(2.*np.pi*alpha*x_grid[k1]/Lx)*sin(n*np.pi*z_grid[k2]/Lz)
    return f

def set_TT_linear_initial(x_grid,z_grid,N1,N2,Lx,Lz,t,Re,FrS,FrT,cs,ct):
    alpha = 1.
    n = 1.
    k = np.sqrt(np.pi*np.pi*(alpha*alpha+n*n))
    pp = -k*k/2./Re+np.sqrt(k**4/Re \
                            -4.*(cs*alpha*alpha*pi*pi/FrS/FrS)\
                                +ct*n*n*pi*pi/FrT/FrT)
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = - np.exp(pp*t)*\
                       cos(2.*np.pi*alpha*x_grid[k1]/Lx)*sin(n*np.pi*z_grid[k2]/Lz)
    return f

def set_SS_linear_initial(x_grid,z_grid,N1,N2,Lx,Lz,t,Re,FrS,FrT,cs,ct):
    alpha = 1.
    n = 1.
    k = np.sqrt(np.pi*np.pi*(alpha*alpha+n*n))
    pp = -k*k/2./Re+np.sqrt(k**4/Re \
                            -4.*(cs*alpha*alpha*pi*pi/FrS/FrS)\
                                +ct*n*n*pi*pi/FrT/FrT)
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = - np.exp(pp*t)*\
                       cos(2.*np.pi*alpha*x_grid[k1]/Lx)*sin(n*np.pi*z_grid[k2]/Lz)
    return f
#
# These are for debugging the elliptic solver
#

def set_psi_rhs(x_grid,z_grid,N1,N2,Lx,Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = -((2.*np.pi/Lx)**2+(np.pi/Lz)**2)*sin(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz)
    return f

def set_psi_exact(x_grid, z_grid, N1, N2, Lx, Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = sin(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz)
#            f[k1,k2] = (- sin(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz) 
#                       +.7*cos(3.*2.*np.pi*x_grid[k1]/Lx)*sin(4.*np.pi*z_grid[k2]/Lz))
    return f

def set_rhsPsi(x_grid, z_grid, N1, N2, Lx, Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
#            f[k1,k2] = -( (2.*np.pi/Lx)**2 + (np.pi/Lz)**2 ) * \
#                       sin(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz)
            f[k1,k2] = ( ((2.*np.pi/Lx)**2+(np.pi/Lz)**2)*
                                       sin(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz) 
                       -.7*((4.*np.pi/Lz)**2+(3.*2.*np.pi/Lx)**2)*
                                       cos(3.*2.*np.pi*x_grid[k1]/Lx)*sin(4.*np.pi*z_grid[k2]/Lz))
    return f

def set_f_simple(x_grid, z_grid, N1, N2, Lx, Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = cos(2.*np.pi*x_grid[k1]/Lx)*sin(4*z_grid[k2]*np.pi/Lz)
#            f[k1,k2] = (- sin(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz) 
#                       +.7*cos(3.*2.*np.pi*x_grid[k1]/Lx)*sin(4.*np.pi*z_grid[k2]/Lz))
    return f

def f_simple_exact_decay(t, x_grid, z_grid, N1, N2, Lx, Lz, kk, kk_cosine, f0_hat, params, eqType, HyperTypes):
    """ This function returns the spectral coefficients for the decay problem. """
    f = np.zeros((N1,N2))
    fac = np.zeros((N1,N2))*complex(0,1)
    g, rho0, ct, cs, bt, bs, nu, kappat, kappas = params
    hyper_Psi, hyper_T, hyper_S = HyperTypes

    if eqType == 'TT':
        visc = kappat
        hyper = hyper_T
    elif eqType == 'SS':
        visc = kappas
        hyper = hyper_S
    else:
        visc = nu
        hyper = hyper_Psi

    for i in range(N1):
        for j in range(N2):
            if (kk[i] == 0) and (kk_cosine[j] == 0):
                fac[i,j] = 0.
            else:
                if hyper == 2:
                    fac[i,j] = f0_hat[i,j]*np.exp(-visc*(kk[i]*kk[i]+kk_cosine[j]*kk_cosine[j])*t)
                elif hyper == 4:
                    fac[i,j] = f0_hat[i,j]*np.exp(visc*(kk[i]**4+kk_cosine[j]**4)*t)
                elif hyper == 6:
                    fac[i,j] = f0_hat[i,j]*np.exp(-visc*(kk[i]**6+kk_cosine[j]**6)*t)
                elif hyper == 8:
                    fac[i,j] = f0_hat[i,j]*np.exp(visc*(kk[i]**8+kk_cosine[j]**8)*t)
                        

    return cst.iFFT_FST(N1, N2, fac)

def S0_simple_exact(t, x_grid, z_grid, N1, N2, Lx, Lz, kk, kk_cosine, Psi0_hat, TT0_hat, SS0_hat, params):
    """ This function returns the spectral coefficients for the case when there is
        no viscosity and the salinity is zero problem. """
    f = np.zeros((N1,N2))
    facPsi = np.zeros((N1,N2))*complex(0,1)
    facTT  = np.zeros((N1,N2))*complex(0,1)
    facSS  =  np.zeros((N1,N2))*complex(0,1)
    g, rho0, ct, cs, bt, bs, nu, kappat, kappas = params

    psi_1 = np.sqrt(bt*ct*g)
    psi_2 = np.sqrt(ct*g/bt)

    for i in range(N1):
        for j in range(N2):
            if (kk[i] == 0) and (kk_cosine[j] == 0):
                facPsi[i,j] = 0.
                facTT[i,j]  = 0.
            else:
                SqBigKK = np.sqrt(kk[i]*kk[i]+kk_cosine[j]*kk_cosine[j])
                Term1 = .5*(np.exp(psi_1*kk[i]/SqBigKK*t)+np.exp(-psi_1*kk[i]/SqBigKK*t))
                Term2 = .5*1j*psi_2/SqBigKK*(np.exp(psi_1*kk[i]/SqBigKK*t)-np.exp(-psi_1*kk[i]/SqBigKK*t))
                Term3 = Term1
                Term4 = -.5*1j*SqBigKK/psi_2*(np.exp(psi_1*kk[i]/SqBigKK*t)-np.exp(-psi_1*kk[i]/SqBigKK*t))
                facPsi[i,j] = Psi0_hat[i,j]*Term1 + \
                              TT0_hat[i,j] *Term2
                facTT[i,j]  = Psi0_hat[i,j]*Term3 + \
                              TT0_hat[i,j] *Term4

    return np.array([cst.iFFT_FST(N1, N2, facPsi), cst.iFFT_FST(N1, N2, facTT), cst.iFFT_FST(N1, N2, facSS)])

def T0_simple_exact(t, x_grid, z_grid, N1, N2, Lx, Lz, kk, kk_cosine, Psi0_hat, TT0_hat, SS0_hat, params):
    """ This function returns the spectral coefficients for the case when there is
        no viscosity and the salinity is zero problem. """
    f = np.zeros((N1,N2))
    facPsi = np.zeros((N1,N2))*complex(0,1)
    facTT  = np.zeros((N1,N2))*complex(0,1)
    facSS  =  np.zeros((N1,N2))*complex(0,1)
    g, rho0, ct, cs, bt, bs, nu, kappat, kappas = params

    psi_3 = np.sqrt(bs*cs*g)
    psi_4 = np.sqrt(cs*g/bs)

    for i in range(N1):
        for j in range(N2):
            if (kk[i] == 0) and (kk_cosine[j] == 0):
                facPsi[i,j] = 0.
                facSS[i,j]  = 0.
            else:
                SqBigKK = np.sqrt(kk[i]*kk[i]+kk_cosine[j]*kk_cosine[j])
                facPsi[i,j] = Psi0_hat[i,j]*np.cos(psi_3*kk[i]/SqBigKK*t) - \
                              1j*SS0_hat[i,j]*psi_4/SqBigKK*np.sin(psi_3*kk[i]/SqBigKK*t)
                facSS[i,j]  = -1j*Psi0_hat[i,j]*SqBigKK/psi_4*np.sin(psi_3*kk[i]/SqBigKK*t) + \
                              SS0_hat[i,j]*np.cos(psi_3*kk[i]/SqBigKK*t)

    return np.array([cst.iFFT_FST(N1, N2, facPsi), cst.iFFT_FST(N1, N2, facTT), cst.iFFT_FST(N1, N2, facSS)])
