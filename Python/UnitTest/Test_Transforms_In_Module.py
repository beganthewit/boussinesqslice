
from numpy import *
from scipy import *
from numpy import fft
from scipy import fftpack
from numpy.linalg import inv
import matplotlib.pyplot as plt
import time
from mpl_toolkits.mplot3d import Axes3D
import InitialConditions as ics
from InitialConditions import *
import CosineSineTransforms as cst
from CosineSineTransforms import *

I = complex(0.,1.)

N1 = 32
N2 = 32
v1 = random.random_sample((N1,N2))
v2 = random.random_sample((N1,N2))
v1_dx_dz = random.random_sample((N1,N2))
v1_z = random.random_sample((N1,N2))
v1_x = random.random_sample((N1,N2))
v1_xx = random.random_sample((N1,N2))
v1_x_exact = random.random_sample((N1,N2))
v1_xx_exact = random.random_sample((N1,N2))
v1_dx_dz_exact = random.random_sample((N1,N2))
v1_dxdx = zeros((N1,N2))
v2_dzdz = zeros((N1,N2))
v1_der2 = zeros((N1,N2))
v1_der2_exact = zeros((N1,N2))
v2_dx_dz = random.random_sample((N1,N2))
v2_x = random.random_sample((N1,N2))
v2_x_exact = random.random_sample((N1,N2))
v2_z = random.random_sample((N1,N2))
v2_xx = random.random_sample((N1,N2))
v2_xx_exact = random.random_sample((N1,N2))
v2_dx_dz_exact = random.random_sample((N1,N2))
v2_der2 = np.zeros((N1,N2))
v2_dxdx = np.zeros((N1,N2))
v2_dzdz = np.zeros((N1,N2))
v2_der2_exact = zeros((N2,N2))
TT = random.random_sample((N1,N2))
SS = random.random_sample((N1,N2))
TT_der =random.random_sample((N1,N2))
TT_z =random.random_sample((N1,N2))
TT_der_exact = random.random_sample((N1,N2))
TT_z_exact = random.random_sample((N1,N2))
TT_der2 = np.zeros((N1,N2))
TT_zz = np.zeros((N1,N2))
TT_der2_exact = zeros((N1,N2))
TT_zz_exact = zeros((N1,N2))
SS_der = random.random_sample((N1,N2))
SS_z = random.random_sample((N1,N2))
SS_der_exact = random.random_sample((N1,N2))
SS_z_exact = random.random_sample((N1,N2))
SS_der2 = np.zeros((N1,N2))
SS_zz = np.zeros((N1,N2))
SS_der2_exact = np.zeros((N1,N2))
SS_zz_exact = np.zeros((N1,N2))

v1_hat = np.fft.fft2(v1)
v2_hat = np.fft.fft2(v2)
TT_hat = np.fft.fft2(TT)
SS_hat = np.fft.fft2(SS)

k1 = 3
k2 = 12
TT_hat_val = 0
for m1 in range(N1):
    for m2 in range(N2):
        TT_hat_val = TT_hat_val + \
         exp(-2.*np.pi*1j*(k1*m1/float(N1)+k2*m2/float(N2))) * TT[m1,m2]
print abs(TT_hat_val - TT_hat[k1,k2])

#
TT_hat_test = TT_hat*complex(0.0,0.0)

for l in range(N1):
    TT_hat_test[l,:] = np.fft.fft(TT[l,:])
        
for q in range(N2):
    TT_hat_test[:,q] = np.fft.fft(TT_hat_test[:,q])
    
print max(reshape(TT_hat_test - TT_hat,-1))
print TT_hat[1:5,5]
print N1
print TT_hat_test[1:5,5]

TT_back = np.fft.ifft2(TT_hat)
TT_back_test = TT_back*complex(0.0,0.0)

for l in range(N1):
    TT_back_test[l,:] = np.fft.ifft(TT_hat_test[l,:])
        
for q in range(N2):
    TT_back_test[:,q] = np.fft.ifft(TT_back_test[:,q])
    
print max(reshape(TT_back_test - TT_back,-1))


# Set two lengths for width and height
Lx = np.float_(10.)
Lz = np.float_(5.)
delta_x = Lx/np.float_(N1)
delta_z = Lz/(np.float_(N2)-1.)
#
# The other important thing to compute is the wave number array (frequencies)
#
kk = np.fft.fftfreq(N1,Lx/N1)*2*np.pi
print kk
#
# And for the cosine series
#
kk_cosine = np.arange((N2))*np.pi/Lz
print kk_cosine
#
# We need a Fourier Grid, we need a z-grid for the cosine/sine series 
#
x_grid = np.arange((float(N1)))
z_grid = np.arange((float(N2)))
z_grid_twice = np.zeros((N2*2))
x_grid[0] = 0.
z_grid[0] = 0.
for k in arange(1,N1):
        x_grid[k] = x_grid[k-1] + delta_x
print 'x_grid',x_grid
for k in arange(1,N2):
        z_grid[k] = z_grid[k-1] + delta_z
print 'z_grid',z_grid


# Assign the initial conditions. Allow v1 to be odd in the x direction but test the even-ness of the function in the y direction.

# <codecell>

TTop = .5
TBot = -.5
T0 = 0.
STop = -1.
SBot = 1.
S0 = 0.
DeltaTT = TTop-TBot
DeltaSS = STop-SBot
h0=1.
TT = ics.set_TT_initial(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,T0,TTop,TBot,Lx)
SS = ics.set_SS_initial(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,S0,STop,SBot,Lx)

TT_der_exact = ics.set_TT_der(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,T0,TTop,TBot,Lx)
SS_der_exact = ics.set_SS_der(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,S0,STop,SBot,Lx)
TT_z_exact   = set_TT_z(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,T0,TTop,TBot,Lx)
SS_z_exact   = set_SS_z(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,S0,STop,SBot,Lx)

print TT[1,:]
print SS[1,:]

v1 = ics.set_v1_initial(x_grid,z_grid,N1,N2,Lx,Lz)
v2 = ics.set_v2_initial(x_grid,z_grid,N1,N2,Lx,Lz)

v1_dx_dz_exact = ics.set_v1_der_exact_x_z(x_grid,z_grid,N1,N2,Lx,Lz)
v1_der2_exact  = ics.set_v1_der2_exact(x_grid,z_grid,N1,N2,Lx,Lz)
v1_x_exact     = ics.set_v1_x_exact(x_grid,z_grid,N1,N2,Lx,Lz)
v1_xx_exact    = ics.set_v1_xx_exact(x_grid,z_grid,N1,N2,Lx,Lz)
v2_dx_dz_exact = ics.set_v2_der_exact_x_z(x_grid,z_grid,N1,N2,Lx,Lz)
v2_der2_exact  = ics.set_v2_der2_exact(x_grid,z_grid,N1,N2,Lx,Lz)
v2_x_exact     = ics.set_v2_x_exact(x_grid,z_grid,N1,N2,Lx,Lz)
v2_xx_exact    = ics.set_v2_xx_exact(x_grid,z_grid,N1,N2,Lx,Lz)
TT_der2_exact  = ics.set_TT_der2(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,T0,TTop,TBot,Lx)
TT_zz_exact    = ics.set_TT_zz(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,T0,TTop,TBot,Lx)
SS_der2_exact  = ics.set_SS_der2(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,S0,STop,SBot,Lx)
TT_der2_exact  = ics.set_TT_der2(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,T0,TTop,TBot,Lx)
SS_zz_exact    = ics.set_SS_zz(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,S0,STop,SBot,Lx)


fig_TS = plt.figure()
#fig,ax = plt.subplots()
p1, = plt.plot(TT_der_exact[1,:],z_grid)
p2, = plt.plot(SS_der_exact[1,:],z_grid)
plt.legend([p1, p2], ["TT_der_exact", "SS_der_Exact"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('Tder and Sder profiles down a line')
plt.xlabel('Tder and Sder -- antisymmetric')
plt.ylabel('z')
plt.show()


fig_velocities = plt.figure()
#fig,ax = plt.subplots()
p3, = plt.plot(x_grid,v1[:,5])
p4, = plt.plot(x_grid,v2[:,5])
plt.legend([p3, p4], ["v1","v2"])
plt.xlabel('x')
plt.title('x dependency of v1 and v2')
plt.ylabel('v1 or v2')
plt.show()

fig_velocities_der = plt.figure()
#fig,ax =plt.subplots()
p3, = plt.plot(x_grid,v1_dx_dz_exact[:,5])
p4, = plt.plot(x_grid,v2_dx_dz_exact[:,5])
plt.legend([p3, p4], ["v1 der","v2 der"])
plt.xlabel('x')
plt.title('x dependency of v1der  and v2der')
plt.ylabel('v1der or v2der')
plt.show()

#
# Testing 2D fourier transforms of real functions before doing so with the cosin function
#

v1_hat = np.fft.fft2(v1)
v2_hat = np.fft.fft2(v2)

print max(abs(reshape(v1_hat,-1)))

#
# For the -sin(x) cos(z) we should have two wave number one functions
#
print v1_hat[0,1],v1_hat[1,1],v1_hat[1,0]
#
# For the -cos(z)  function that would be just in one index
#
print v2_hat[0,1],v2_hat[1,0],v2_hat[1,1]

#
#
#

v1_hat_test = np.zeros((N1,N2))*complex(0.0,0.0)
v2_hat_test = np.zeros((N1,N2))*complex(0.0,0.0)


for l in range(N1):
    v1_hat_test[l,:] = np.fft.fft(v1[l,:])
        
for q in range(N2):
    v1_hat_test[:,q] = np.fft.fft(v1_hat_test[:,q])
    
print 'error is ',max(reshape(v1_hat_test - v1_hat,-1))

print 'max is ',max(abs(reshape(v1_hat_test,-1)))

#
# Testing 2D transforms with cosine transform in z

#
# THE Trick IS IN THE ORDERING
#

#
# On the way 'there' you do cosine or real transformation first since the data is real
#
for l in range(N2):
    v1_hat_test[l,:] = fftpack.dct(v1[l,:],type=1)/(np.float_(N2)-1.)
#
# In step two you transform with Fourier.
#
for q in range(N1):
    v1_hat_test[:,q] = np.fft.fft(v1_hat_test[:,q])    
#
# And now go backwards to see if we get the same thing
#

v1_back_test = v1_hat_test*complex(0.0,0.0)
v1_back_test_real = np.zeros((N1,N2))

#
# Here  first take the ifft since you'll get Reals back from the complex coefficients.
#
for k in range(N2):
    v1_back_test[:,k] = np.fft.ifft(v1_hat_test[:,k])
v1_back_test_real = real(v1_back_test)
    
print max(reshape(v1_back_test_real-real(v1_back_test),-1))
#
# Now that you have Reals take the inverse cosine transform
#
for l in range(N1):
    v1_back_test_real[l,:] = fftpack.idct(v1_back_test_real[l,:],type=1)*.5
    
#print v1[1,:]
print max(reshape(v1_back_test_real - v1,-1))

# Test and evaluate the new transforms that we now read in

# <codecell>

v1             = ics.set_v1_initial(x_grid,z_grid,N1,N2,Lx,Lz)
v1_dx_dz_exact = ics.set_v1_der_exact_x_z(x_grid,z_grid,N1,N2,Lx,Lz)
v2             = ics.set_v2_initial(x_grid,z_grid,N1,N2,Lx,Lz)
v2_dx_dz_exact = ics.set_v2_der_exact_x_z(x_grid,z_grid,N1,N2,Lx,Lz)
TT             = ics.set_TT_initial(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,T0,TTop,TBot,Lx)
TT_der_exact   = ics.set_TT_der(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,T0,TTop,TBot,Lx)
SS             = ics.set_SS_initial(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,S0,STop,SBot,Lx)
SS_der_exact   = ics.set_SS_der(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,S0,STop,SBot,Lx)


v1_hat = complex(0.,0.)*np.zeros((N1,N2))
v1_der_hat = np.zeros((N1,N2))
v2_hat = complex(0.,0.)*np.zeros((N1,N2))
TT_hat = complex(0.,0.)*np.zeros((N1,N2))
SS_hat = complex(0.,0.)*np.zeros((N1,N2))

#
# Transform to spectral space
#
v1_hat = cst.FFT_FCT(N1,N2,v1)
v2_hat = cst.FFT_FST(N1,N2,v2)
TT_hat = cst.FFT_FST(N1,N2,TT)
SS_hat = cst.FFT_FST(N1,N2,SS)


#
#  Go back to physical space and see what everything looks like
#
v1_return = cst.iFFT_FCT(N1,N2,v1_hat)
v2_return = cst.iFFT_FST(N1,N2,v2_hat)
TT_return = cst.iFFT_FST(N1,N2,TT_hat)
SS_return = cst.iFFT_FST(N1,N2,SS_hat)


print 'v1 function max error = ',max(reshape(v1 - v1_return,-1))
print 'v2 function max error = ',max(reshape(v2 - v2_return,-1))
print 'TT function max error = ',max(reshape(TT - TT_return,-1))
print 'SS function max error = ',max(reshape(SS - SS_return,-1))

#
# Take Derivatives and go back to physical space
#
#
#        First for the cosine vertical derivative
#
for k in range(N2):
    v1_dx_dz[:,k] = np.fft.ifft(1j*kk*v1_hat[:,k]).real
for ll in range(N1):
    v1_dx_dz[ll,1:N2-1] = fftpack.idst(-kk_cosine[1:N2-1]*v1_dx_dz[ll,1:N2-1],type=1)*.5
    v1_dx_dz[ll,0]=0.
    v1_dx_dz[ll,N2-1] = 0.    
    
print 'v1 der max error = ',max(reshape(v1_dx_dz - v1_dx_dz_exact,-1))

#
#        Then for the sine vertical derivative
#

for k in range(N2):
    v2_dx_dz[:,k] = np.fft.ifft(1j*kk*v2_hat[:,k]).real
    v2_dx_dz[:,0] = 0.
    v2_dx_dz[:,N2-1] = 0.
for ll in range(N1):
    v2_dx_dz[ll,:] = fftpack.idct(kk_cosine[:]*v2_dx_dz[ll,:],type=1)*.5

print 'v2 der max error = ',max(reshape(v2_dx_dz - v2_dx_dz_exact,-1))


#
#        Then for the TT vertical derivative
#
for k in range(N2):
    TT_der[:,k] = np.fft.ifft(1j*kk*TT_hat[:,k]).real
for l in range(N1):
    TT_der[l,:] = fftpack.idct(kk_cosine[:]*TT_der[l,:],type=1)*.5

print 'TT der max error = ',max(reshape(TT_der - TT_der_exact,-1))


#
#        Then for the SS vertical derivative
#
for k in range(N2):
    SS_der[:,k] = np.fft.ifft(1j*kk*SS_hat[:,k]).real
for l in range(N1):
    SS_der[l,:] = fftpack.idct(kk_cosine[:]*SS_der[l,:],type=1)*.5

print 'SS der max error = ',max(reshape(SS_der - SS_der_exact,-1))

fig_v1_der_return_error = plt.figure()
p1v1, = plt.plot(v1[1,:],z_grid)
p2v1, = plt.plot(v1_dx_dz_exact[1,:],z_grid)
p3v1, = plt.plot(v1_dx_dz[1,:],z_grid)
#p4, = plot(SS[1,:],z_grid)
plt.legend([p1v1, p1v1, p3v1],["v1", "v1_der_exact", "v1_der"])
plt.title('v1 profiles down a line -- on return from the fft')
plt.xlabel('v1')
plt.ylabel('z')
plt.show()

fig_v2_der_return_error = plt.figure()
p1v1, = plt.plot(v2[1,:],z_grid)
p2v1, = plt.plot(v2_dx_dz_exact[1,:],z_grid)
p3v1, = plt.plot(v2_dx_dz[1,:],z_grid)
#p4, = plot(SS[1,:],z_grid)
plt.legend([p1v1, p1v1, p3v1],["v2", "v2_der_exact", "v2_der"])
plt.title('v2 profiles down a line -- on return from the fft')
plt.xlabel('v2')
plt.ylabel('z')
plt.show()



v1_der_error = v1_dx_dz - v1_dx_dz_exact
fig_v1_error = plt.figure()
cnt = plt.contourf(x_grid,z_grid,v1_der_error)

fig_v1_dx_dz = plt.figure()
cnt = plt.contourf(x_grid,z_grid,v1_dx_dz)

fig_v1_dx_dz_exact = plt.figure()
cnt = plt.contourf(x_grid,z_grid,v1_dx_dz_exact)


v2_der_error = v2_dx_dz - v2_dx_dz_exact
fig_v1_error = plt.figure()
cnt = plt.contourf(x_grid,z_grid,v2_der_error)

#
# Make some plots
#
fig_T_return = plt.figure()
p1, = plt.plot(TT_return[1,:],z_grid)
#p2, = plt.plot(SS_return[1,:],z_grid)
p3, = plt.plot(TT[1,:],z_grid)
#p4, = plt.plot(SS[1,:],z_grid)
plt.legend([p1, p3],["TT_return", "TT"])
#legend([p1, p2, p3, p4], ["TT_return", "SS_return", "TT", "SS"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('T and S profiles down a line -- on return from the fft')
plt.xlabel('T and S -- antisymmetric')
plt.ylabel('z')
plt.show()

fig_T_der_return = plt.figure()
p1, = plt.plot(TT[1,:],z_grid)
p2, = plt.plot(TT_der_exact[1,:],z_grid)
p3, = plt.plot(TT_der[1,:],z_grid)
#p4, = plt.plot(SS[1,:],z_grid)
plt.legend([p1, p1, p3],["TT", "TT_der_exact", "TT_der"])
#legend([p1, p2, p3, p4], ["TT_return", "SS_return", "TT", "SS"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('Tprofiles down a line -- on return from the fft')
plt.xlabel('T  -- antisymmetric')
plt.ylabel('z')
plt.show()

fig_S_der_return = plt.figure()
p1, = plt.plot(SS[1,:],z_grid)
p2, = plt.plot(SS_der_exact[1,:],z_grid)
p3, = plt.plot(SS_der[1,:],z_grid)
#p4, = plt.plot(SS[1,:],z_grid)
plt.legend([p1, p1, p3],["SS", "SS_der_exact", "SS_der"])
#legend([p1, p2, p3, p4], ["TT_return", "SS_return", "TT", "SS"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('S profiles down a line -- on return from the fft')
plt.xlabel('S  -- antisymmetric')
plt.ylabel('z')
plt.show()


fig_S_return = plt.figure()
p2, = plt.plot(SS_return[1,:],z_grid)
p4, = plt.plot(SS[1,:],z_grid)
plt.legend([p2, p4],["SS_return", "SS"])

fig_v1 = plt.figure()
p5, = plt.plot(x_grid,v1_return[:,5])
p6, = plt.plot(x_grid,v1[:,5])
plt.legend([p5, p6], ["v1_return","v1"])

fig_v2 = plt.figure()
p7, = plt.plot(x_grid,v2_return[:,5])
p8, = plt.plot(x_grid,v2[:,5])
plt.legend([p7, p8], ["v2_return","v2"])
plt.xlabel('x')
plt.title('x dependency of v1_return and v2_return')
plt.ylabel('v1 or v2')
plt.show()


# Test and evalute second derivatives

#
#        First for the cosine vertical derivative
#
for k in range(N2):
    v1_der2[:,k] = np.fft.ifft(-kk*kk*v1_hat[:,k]).real
for l in range(N1):
    v1_der2[l,:] = fftpack.idct(-kk_cosine[:]*kk_cosine[:]*v1_der2[l,:],type=1)*.5
      
print 'v1 der max error = ',max(reshape(v1_der2 - v1_der2_exact,-1))

#
#        Then for the sine vertical derivative
#
for k in range(N2):
    v2_der2[:,k] = np.fft.ifft(-kk*kk*v2_hat[:,k]).real
for ll in range(N1):
    v2_der2[ll,1:N2-1] = fftpack.idst(-kk_cosine[1:N2-1]*kk_cosine[1:N2-1]*v2_der2[ll,1:N2-1],type=1)*.5
    v2_der2[ll,0]=0.
    v2_der2[ll,N2-1] = 0.
   
print 'v2 der max error = ',max(reshape(v2_der2 - v2_der2_exact,-1))

#
#        Then for the TT vertical derivative
#
for k in range(N2):
    TT_der2[:,k] = np.fft.ifft(-kk*kk*TT_hat[:,k]).real
for l in range(N1):
    TT_der2[l,1:N2-1] = fftpack.idst(-kk_cosine[1:N2-1]*kk_cosine[1:N2-1]*TT_der2[l,1:N2-1],type=1)*.5

print 'TT der max error = ',max(reshape(TT_der2 - TT_der2_exact,-1))

#
#        Then for the SS vertical derivative
#
for k in range(N2):
    SS_der2[:,k] = np.fft.ifft(-kk*kk*SS_hat[:,k]).real
for l in range(N1):
    SS_der2[l,1:N2-1] = fftpack.idst(-kk_cosine[1:N2-1]*kk_cosine[1:N2-1]*SS_der2[l,1:N2-1],type=1)*.5

print 'SS der max error = ',max(reshape(SS_der2 - SS_der2_exact,-1))

# <codecell>

fig_v1_der2_return_error = plt.figure()
p1v1der2, = plt.plot(v1[1,:],z_grid)
p2v1der2, = plt.plot(v1_der2_exact[1,:],z_grid)
p3v1der2, = plt.plot(v1_der2[1,:],z_grid)
#p4, = plt.plot(SS[1,:],z_grid)
plt.legend([p1v1, p1v1, p3v1],["v1", "v1_der2_exact", "v1_der2"])
plt.title('v1 der2 profiles down a line -- on return from the fft')
plt.xlabel('v1 der2')
plt.ylabel('z')
plt.show()

fig_v2_der2_return_error = plt.figure()
p1v1der2, = plt.plot(v2[1,:],z_grid)
p2v1der2, = plt.plot(v2_der2_exact[1,:],z_grid)
p3v1der2, = plt.plot(v2_der2[1,:],z_grid)
#p4, = plt.plot(SS[1,:],z_grid)
plt.legend([p1v1, p1v1, p3v1],["v2", "v2_der_exact", "v2_der"],bbox_to_anchor=(1.5, 1.05))
plt.title('v2 profiles down a line -- on return from the fft')
plt.xlabel('v2')
plt.ylabel('z')
plt.show()

v2_der2_error = v2_der2 - v2_der2_exact
fig_v2_error = plt.figure()
cntv2der2 = plt.contourf(x_grid,z_grid,v2_der2_error)

fig_SS_der2_return_error = plt.figure()
p1SSder2, = plt.plot(SS[1,:],z_grid)
p2SSder2, = plt.plot(SS_der2_exact[1,:],z_grid)
p3SSder2, = plt.plot(SS_der2[1,:],z_grid)
plt.legend([p1SSder2, p1SSder2, p3SSder2],["SS", "SS_der2_exact", "SS_der2"],bbox_to_anchor=(1.5, 1.05))
plt.title('SS der profiles down a line -- on return from the fft')
plt.xlabel('SS')
plt.ylabel('z')
plt.show()

fig_TT_der2_return_error = plt.figure()
p1TTder2, = plt.plot(TT[1,:],z_grid)
p2TTder2, = plt.plot(TT_der2_exact[1,:],z_grid)
p3TTder2, = plt.plot(TT_der2[1,:],z_grid)
plt.legend([p1TTder2, p1TTder2, p3TTder2],["TT", "TT_der2_exact", "TT_der2"],bbox_to_anchor=(1.5, 1.05))
plt.title('TT der profiles down a line -- on return from the fft')
plt.xlabel('TT')
plt.ylabel('z')
plt.show()

print TT_der2_exact[5,:]

print TT_der2[5,:]

# Test the first derivative functions, using already the initial values of SS and so forth.  First the vertical derivatives.

SS_z = cst.iFFT_FCT_Z(N1,N2,SS_hat,kk_cosine)
TT_z = cst.iFFT_FCT_Z(N1,N2,TT_hat,kk_cosine)
SS_zz = cst.iFFT_FST_ZZ(N1,N2,SS_hat,kk_cosine)
TT_zz = cst.iFFT_FST_ZZ(N1,N2,TT_hat,kk_cosine)

print 'SS_z max error = ',max(reshape(SS_z - SS_z_exact,-1))
print 'TT_z max error = ',max(reshape(TT_z - TT_z_exact,-1))
print 'SS_zz max error = ',max(reshape(SS_zz - SS_zz_exact,-1))
print 'TT_zz max error = ',max(reshape(TT_zz - TT_zz_exact,-1))

fig_S_z = plt.figure()
p1, = plt.plot(SS[1,:],z_grid)
p2, = plt.plot(SS_z_exact[1,:],z_grid)
p3, = plt.plot(SS_z[1,:],z_grid)
plt.legend([p1, p1, p3],["SS", "SS_z_exact", "SS_z"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('S profiles down a line')
plt.xlabel('S  -- antisymmetric')
plt.ylabel('z')
plt.show()

fig_SS_z = plt.figure()
p1, = plt.plot(SS[1,:],z_grid)
p2, = plt.plot(SS_zz_exact[1,:],z_grid)
p3, = plt.plot(SS_zz[1,:],z_grid)
plt.legend([p1, p1, p3],["SS", "SS_zz_exact", "SS_zz"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('S profiles down a line')
plt.xlabel('S  -- antisymmetric')
plt.ylabel('z')
plt.show()

fig_T_z = plt.figure()
p1, = plt.plot(TT[1,:],z_grid)
p2, = plt.plot(TT_z_exact[1,:],z_grid)
p3, = plt.plot(TT_z[1,:],z_grid)
plt.legend([p1, p1, p3],["TT", "TT_z_exact", "TT_z"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('T profiles down a line')
plt.xlabel('T  -- antisymmetric')
plt.ylabel('z')
plt.show()

fig_TT_z = plt.figure()
p1, = plt.plot(TT[1,:],z_grid)
p2, = plt.plot(TT_zz_exact[1,:],z_grid)
p3, = plt.plot(TT_zz[1,:],z_grid)
plt.legend([p1, p1, p3],["TT", "TT_zz_exact", "TT_zz"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('T profiles down a line')
plt.xlabel('T  -- antisymmetric')
plt.ylabel('z')
plt.show()

# <markdowncell>

# Then the horizontal derivatives. IN this case you don't use opposite FCT FST, because the derivatives are horiztonal.

# <codecell>

v1_x = iFFT_FCT_X(N1,N2,v1_hat,kk)
v2_x = iFFT_FST_X(N1,N2,v2_hat,kk)
v1_xx = iFFT_FCT_XX(N1,N2,v1_hat,kk)
v2_xx = iFFT_FST_XX(N1,N2,v2_hat,kk)

print 'v1_x max error = ',max(reshape(v1_x - v1_x_exact,-1))
print 'v2_x max error = ',max(reshape(v2_x - v2_x_exact,-1))
print 'v1_xx max error = ',max(reshape(v1_xx - v1_xx_exact,-1))
print 'v2_xx max error = ',max(reshape(v2_xx - v2_xx_exact,-1))


# <

