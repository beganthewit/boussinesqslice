
import numpy as np
from numpy import *
from scipy import *
from numpy import fft
from scipy import fftpack
from numpy.linalg import inv
import matplotlib.pyplot as plt
import time
from mpl_toolkits.mplot3d import Axes3D
#
# Note that you need to put a symbolic link into this directory that
# points to one directory above
#
# ln -s ../CosineSineTransforms.py
#
import CosineSineTransforms as cst
from CosineSineTransforms import *


def set_psi_exact(x_grid,z_grid,N1,N2,Lx,Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
#            f[k1,k2] = sin(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz)
            f[k1,k2] = (- sin(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz) 
                       +.7*cos(3.*2.*np.pi*x_grid[k1]/Lx)*sin(4.*np.pi*z_grid[k2]/Lz))
    return f

def set_rhs(x_grid,z_grid,N1,N2,Lx,Lz):
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
#            f[k1,k2] = -( (2.*np.pi/Lx)**2 + (np.pi/Lz)**2 ) * \
#                       sin(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz)
            f[k1,k2] = ( ((2.*np.pi/Lx)**2+(np.pi/Lz)**2)*
                                       sin(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz) 
                       -.7*((4.*np.pi/Lz)**2+(3.*2.*np.pi/Lx)**2)*
                                       cos(3.*2.*np.pi*x_grid[k1]/Lx)*sin(4.*np.pi*z_grid[k2]/Lz))
    return f


N1 = 64
N2 = 32

# Set two lengths for width and height
Lx = np.float_(10.)
Lz = np.float_(5.)
delta_x = Lx/np.float_(N1)
delta_z = Lz/(np.float_(N2)-1.)
#
# The other important thing to compute is the wave number array (frequencies)
#
kk = np.fft.fftfreq(N1,Lx/N1)*2*np.pi
#
# And for the cosine series
#
kk_cosine = np.arange((N2))*np.pi/Lz
#
# We need a Fourier Grid, we need a z-grid for the cosine/sine series 
#
x_grid = np.arange((float(N1)))
z_grid = np.arange((float(N2)))
z_grid_twice = np.zeros((N2*2))
x_grid[0] = 0.
z_grid[0] = 0.

for k in arange(1,N1):
        x_grid[k] = x_grid[k-1] + delta_x

for k in arange(1,N2):
        z_grid[k] = z_grid[k-1] + delta_z



# Assign the initial conditions. Allow v1 to be odd in the x direction but test the even-ness of the function in the y direction.


f = set_rhs(x_grid, z_grid, N1, N2, Lx, Lz)
psi_exact = set_psi_exact(x_grid, z_grid, N1, N2, Lx, Lz)
psi = random.random_sample((N1,N2))


f_hat = cst.FFT_FST(N1, N2, f)
psi = cst.iFFT_FST_Helm(N1, N2, f_hat, kk, kk_cosine)


fig_psi = plt.figure(1)

plt.subplot(1, 3, 1)
cnt = plt.contourf(z_grid, x_grid, psi, cmap=plt.get_cmap('RdBu'))
plt.colorbar(cnt)
plt.title('psi')

plt.subplot(1, 3, 2)
cnt = plt.contourf(z_grid, x_grid, psi_exact, cmap=plt.get_cmap('RdBu'))
plt.colorbar(cnt)
plt.title('psi exact')

plt.subplot(1, 3, 3)
cnt = plt.contourf(z_grid, x_grid, psi_exact-psi, cmap=plt.get_cmap('RdBu'))
plt.colorbar(cnt)
plt.title('error')

plt.show()

print 'psi max error = ', max(reshape(psi - psi_exact,-1))


