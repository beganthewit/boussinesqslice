# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

from numpy import *
from scipy import *
from numpy import fft
from scipy import fftpack
from numpy.linalg import inv
import matplotlib.pyplot as plt
import time
from mpl_toolkits.mplot3d import Axes3D

# <codecell>

I = complex(0.,1.)

# <markdowncell>

# Represent a doubly periodic function as
# $
# \mathbf{u}\left( x,y, t \right) = \sum_{k~l} 
# e^{i (k~x + l~y )} \mathbf{\hat{u}}_{k~l} \left( t \right)
# $. 
# Or represent function periodic in x with free slip boundaries in y as
# 
# $
# \mathbf{u}\left( x,y, t \right) = \sum_{k~l} 
# \mathbf{\hat{u}}(k,l) \left( t \right) ~e^{i (k~x)} ~C_l(y),
# $ where $C_l(y)$ is the Fourier Cosine Transform. 
# 

# <markdowncell>

# Test FFT conventions

# <codecell>

N1 = 32
N2 = 32
v1 = random.random_sample((N1,N2))
v2 = random.random_sample((N1,N2))
v1_dx_dz = random.random_sample((N1,N2))
v1_z = random.random_sample((N1,N2))
v1_x = random.random_sample((N1,N2))
v1_xx = random.random_sample((N1,N2))
v1_x_exact = random.random_sample((N1,N2))
v1_xx_exact = random.random_sample((N1,N2))
v1_dx_dz_exact = random.random_sample((N1,N2))
v1_dxdx = zeros((N1,N2))
v2_dzdz = zeros((N1,N2))
v1_der2 = zeros((N1,N2))
v1_der2_exact = zeros((N1,N2))
v2_dx_dz = random.random_sample((N1,N2))
v2_x = random.random_sample((N1,N2))
v2_x_exact = random.random_sample((N1,N2))
v2_z = random.random_sample((N1,N2))
v2_xx = random.random_sample((N1,N2))
v2_xx_exact = random.random_sample((N1,N2))
v2_dx_dz_exact = random.random_sample((N1,N2))
v2_der2 = np.zeros((N1,N2))
v2_dxdx = np.zeros((N1,N2))
v2_dzdz = np.zeros((N1,N2))
v2_der2_exact = zeros((N2,N2))
TT = random.random_sample((N1,N2))
SS = random.random_sample((N1,N2))
TT_der =random.random_sample((N1,N2))
TT_z =random.random_sample((N1,N2))
TT_der_exact = random.random_sample((N1,N2))
TT_z_exact = random.random_sample((N1,N2))
TT_der2 = np.zeros((N1,N2))
TT_zz = np.zeros((N1,N2))
TT_der2_exact = zeros((N1,N2))
TT_zz_exact = zeros((N1,N2))
SS_der = random.random_sample((N1,N2))
SS_z = random.random_sample((N1,N2))
SS_der_exact = random.random_sample((N1,N2))
SS_z_exact = random.random_sample((N1,N2))
SS_der2 = np.zeros((N1,N2))
SS_zz = np.zeros((N1,N2))
SS_der2_exact = np.zeros((N1,N2))
SS_zz_exact = np.zeros((N1,N2))

v1_hat = np.fft.fft2(v1)
v2_hat = np.fft.fft2(v2)
TT_hat = np.fft.fft2(TT)
SS_hat = np.fft.fft2(SS)

k1 = 3
k2 = 12
TT_hat_val = 0
for m1 in range(N1):
    for m2 in range(N2):
        TT_hat_val = TT_hat_val + \
         exp(-2.*np.pi*1j*(k1*m1/float(N1)+k2*m2/float(N2))) * TT[m1,m2]
print abs(TT_hat_val - TT_hat[k1,k2])

# <markdowncell>

# Test nested 1D FFTs 
# 
# Consider the following representation of a spectral approximation,
# \begin{equation}
# A(x,z)=\sum_{l_{1}=0}^{N_{1}-1}\sum_{l_{2}=0}^{N_{2}-1}\hat{A}(l_{1},l_{2})\,\phi_{l_{1}}(x)\,\psi_{l_{2}}(z),
# \end{equation}
# and there are also discrete grid points defined for the Fourier method
# at $(x_{p},z_{q})$. In this case $\phi$ is a complex exponential,
# and $\psi$ can either be the complex exponential or the Fourier Cosine
# series. To see how the algorithm works consider,
# 
# For $p=0..N_{1}$
# 
# For $q=0..N_{2}$
# 
# \begin{align*}
# A(x_{p},\, z_{q}) & =\sum_{l_{1}=0}^{N_{1}-1}\,\phi_{l_{1}}(x_{p})\left(\sum_{l_{2}=0}^{N_{2}-1}\hat{A}(l_{1},\, l_{2})\,\psi_{l_{2}}(z_{q})\right).
# \end{align*}
# Then if we define a new variable,
# 
# For $q=0..N_{2}$
# 
# For $l_{1}=0..N_{1}$
# 
# \begin{align*}
# a(l_{1},\, z_{q}) & =\sum_{l_{2}=0}^{N_{2}-1}\hat{A}(l_{1},\, l_{2})\,\psi_{l_{2}}(z_{q}),
# \end{align*}
#  and separately,
# 
# For $p=0..N_{1}$-1
# 
# For $q=0..N_{2}$-1
# \begin{align*}
# A(x_{p},\, z_{q}) & =\sum_{l_{1}=0}^{N_{1}-1}\,\phi_{l_{1}}(x_{p})\, a(l_{1},\, z_{q}),
# \end{align*}
# then we are able to do the sums faster. With the FFT method we also
# have the basis functions stored at the quadrature points so we can
# make use of software. I think this will look like the following algorithm
# for python,
# 
# For $l_{1}=0..N_{1}$-1
# 
# \begin{equation}a[l_{1},\,:]=FFT(\hat{A}[l_{1},\,:])\end{equation}
# 
# end for
# 
# 
# For $q=0..N_{2}$-1
# 
# $A[:,\, z_{q}]=FFT(a[:,\, q])$
# 
# end for
# 
# end for. 
# 
# Note to myself: This algorithm of 1D nested FFTs works. We'll just do it again to go back too.
# 

# <codecell>

TT_hat_test = TT_hat*complex(0.0,0.0)

for l in range(N1):
    TT_hat_test[l,:] = np.fft.fft(TT[l,:])
        
for q in range(N2):
    TT_hat_test[:,q] = np.fft.fft(TT_hat_test[:,q])
    
print max(reshape(TT_hat_test - TT_hat,-1))
print TT_hat[1:5,5]
print N1
print TT_hat_test[1:5,5]

# <codecell>

TT_back = np.fft.ifft2(TT_hat)
TT_back_test = TT_back*complex(0.0,0.0)

for l in range(N1):
    TT_back_test[l,:] = np.fft.ifft(TT_hat_test[l,:])
        
for q in range(N2):
    TT_back_test[:,q] = np.fft.ifft(TT_back_test[:,q])
    
print max(reshape(TT_back_test - TT_back,-1))



# <markdowncell>

# Note to self: Test nested back transforms are just fine. Now test 1D Cosine transforms

# <markdowncell>

# Set up a grid

# <codecell>

# Set two lengths for width and height
Lx = np.float_(10.)
Lz = np.float_(5.)
delta_x = Lx/np.float_(N1)
delta_z = Lz/(np.float_(N2)-1.)
#
# The other important thing to compute is the wave number array (frequencies)
#
kk = np.fft.fftfreq(N1,Lx/N1)*2*np.pi
print kk
#
# And for the cosine series
#
kk_cosine = np.arange((N2))*np.pi/Lz
print kk_cosine
#
# We need a Fourier Grid, we need a z-grid for the cosine/sine series 
#
x_grid = np.arange((float(N1)))
z_grid = np.arange((float(N2)))
z_grid_twice = np.zeros((N2*2))
x_grid[0] = 0.
z_grid[0] = 0.
for k in arange(1,N1):
        x_grid[k] = x_grid[k-1] + delta_x
print 'x_grid',x_grid
for k in arange(1,N2):
        z_grid[k] = z_grid[k-1] + delta_z
print 'z_grid',z_grid

# <markdowncell>

# Set up some initial conditions.

# <codecell>

def set_v1_initial(x_grid,z_grid,N1,N2,f,Lx,Lz):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (- sin(2.*np.pi*x_grid[k1]/Lx)*cos(np.pi*z_grid[k2]/Lz) 
                       +.7*cos(3.*2.*np.pi*x_grid[k1]/Lx)*cos(4.*np.pi*z_grid[k2]/Lz))
    return 
def set_v1_der_exact_x_z(x_grid,z_grid,N1,N2,f,Lx,Lz):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] =  (2.*np.pi**2/Lx/Lz*cos(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz) 
                        +.7*(3.*2.*np.pi/Lx)*(4.*np.pi/Lz)*sin(3.*2.*np.pi*x_grid[k1]/Lx)*sin(4.*np.pi*z_grid[k2]/Lz))
    return 
def set_v1_der2_exact(x_grid,z_grid,N1,N2,f,Lx,Lz):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] =  (-(2.*np.pi**2/Lx/Lz)**2*sin(2.*np.pi*x_grid[k1]/Lx)*cos(np.pi*z_grid[k2]/Lz) 
                        +.7*(3.*2.*np.pi/Lx)**2*(4.*np.pi/Lz)**2*cos(3.*2.*np.pi*x_grid[k1]/Lx)*cos(4.*np.pi*z_grid[k2]/Lz))
    return 
def set_v1_x_exact(x_grid,z_grid,N1,N2,f,Lx,Lz):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] =  (-2.*np.pi/Lx*cos(2.*np.pi*x_grid[k1]/Lx)*cos(np.pi*z_grid[k2]/Lz) 
                        -.7*(3.*2.*np.pi/Lx)*sin(3.*2.*np.pi*x_grid[k1]/Lx)*cos(4.*np.pi*z_grid[k2]/Lz))
    return 
def set_v1_xx_exact(x_grid,z_grid,N1,N2,f,Lx,Lz):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] =  ((2.*np.pi/Lx)**2*sin(2.*np.pi*x_grid[k1]/Lx)*cos(np.pi*z_grid[k2]/Lz) 
                        -.7*(3.*2.*np.pi/Lx)**2*cos(3.*2.*np.pi*x_grid[k1]/Lx)*cos(4.*np.pi*z_grid[k2]/Lz))
    return 
def set_v2_initial(x_grid,z_grid,N1,N2,f,Lx,Lz):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (- cos(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz) +
                        .3*sin(2.*2.*np.pi*x_grid[k1]/Lx)*sin(5.*np.pi*z_grid[k2]/Lz))
    return f
def set_v2_der_exact_x_z(x_grid,z_grid,N1,N2,f,Lx,Lz):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (2.*np.pi/Lx*sin(2.*np.pi*x_grid[k1]/Lx)*np.pi/Lz*cos(np.pi*z_grid[k2]/Lz) 
                        + .3*2.*2.*np.pi/Lx*5.*np.pi/Lz*cos(2.*2.*np.pi*x_grid[k1]/Lx)*cos(5.*np.pi*z_grid[k2]/Lz))
    return 
def set_v2_der2_exact(x_grid,z_grid,N1,N2,f,Lx,Lz):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (-(2.*np.pi/Lx)**2*cos(2.*np.pi*x_grid[k1]/Lx)*(np.pi/Lz)**2*sin(np.pi*z_grid[k2]/Lz) 
                        + .3*(2.*2.*np.pi/Lx)**2*(5.*np.pi/Lz)**2*sin(2.*2.*np.pi*x_grid[k1]/Lx)*sin(5.*np.pi*z_grid[k2]/Lz))
    return 
def set_v2_x_exact(x_grid,z_grid,N1,N2,f,Lx,Lz):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (2.*np.pi/Lx*sin(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz) 
                        + .3*2.*2.*np.pi/Lx*cos(2.*2.*np.pi*x_grid[k1]/Lx)*sin(5.*np.pi*z_grid[k2]/Lz))
    return 
def set_v2_xx_exact(x_grid,z_grid,N1,N2,f,Lx,Lz):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = ((2.*np.pi/Lx)**2*cos(2.*np.pi*x_grid[k1]/Lx)*sin(np.pi*z_grid[k2]/Lz) 
                        - .3*(2.*2.*np.pi/Lx)**2*sin(2.*2.*np.pi*x_grid[k1]/Lx)*sin(5.*np.pi*z_grid[k2]/Lz))
    return 


#
#  These routines are for the full range of tanh 
#

def set_TT_initial(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,f,T0,TTop,TBot,Lx):
#
#   The linear stratification is then the last bit of each of the following equations and looks like,
#
#   T_bar = z/Lz T_top + (1-z/Lz) T_Bot
#
#   For this problem we are really working on the half interval because of the symmetries of the Fourier Cosine and Sine 
#   Series. I do this by passing a denser grid array. See group of functions below these T & S routines
#
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (DeltaTT/2.*tanh(2.*(z_grid[k2]-Lz/2.)/h0)-T0-z_grid[k2]/Lz*TTop-(1.-z_grid[k2]/Lz)*TBot)*sin(2.*np.pi*x_grid[k1]/Lx)
    return 
def set_TT_der(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,f,T0,TTop,TBot,Lx):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (DeltaTT/2.*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*2./h0-1./Lz*TTop+1./Lz*TBot)*2.*np.pi/Lx*cos(2.*np.pi*x_grid[k1]/Lx)
    return 
def set_TT_der2(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,f,T0,TTop,TBot,Lx):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = 4.*DeltaTT/h0/h0*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*tanh(2.*(z_grid[k2]-Lz/2.)/h0)*(2.*np.pi/Lx)**2*sin(2.*np.pi*x_grid[k1]/Lx)
    return 
def set_TT_z(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,f,T0,TTop,TBot,Lx):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (DeltaTT/2.*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*2./h0-1./Lz*TTop+1./Lz*TBot)*sin(2.*np.pi*x_grid[k1]/Lx)
    return 
def set_TT_zz(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,f,T0,TTop,TBot,Lx):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = -4.*DeltaTT/h0/h0*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*tanh(2.*(z_grid[k2]-Lz/2.)/h0)*sin(2.*np.pi*x_grid[k1]/Lx)
    return 
def set_SS_initial(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,f,S0,STop,SBot,Lx):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (-DeltaSS/2.*tanh(2.*(z_grid[k2]-Lz/2.)/h0)-S0+z_grid[k2]/Lz*STop+(1.-z_grid[k2]/Lz)*SBot)*sin(2.*np.pi*x_grid[k1]/Lx)
    return 
def set_SS_der(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,f,S0,STop,SBot,Lx):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (-DeltaSS/2.*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*2./h0+(1./Lz*STop-1./Lz*SBot))*2.*np.pi/Lx*cos(2.*np.pi*x_grid[k1]/Lx)
    return 
def set_SS_der2(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,f,S0,STop,SBot,Lx):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = -4.*DeltaSS/h0/h0*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*tanh(2.*(z_grid[k2]-Lz/2.)/h0)*(2.*np.pi/Lx)**2*sin(2.*np.pi*x_grid[k1]/Lx)
    return 
def set_SS_z(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,f,S0,STop,SBot,Lx):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = (-DeltaSS/2.*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*2./h0+(1./Lz*STop-1./Lz*SBot))*sin(2.*np.pi*x_grid[k1]/Lx)
    return 
def set_SS_zz(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,f,S0,STop,SBot,Lx):
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = 4.*DeltaSS/h0/h0*(1.-tanh(2.*(z_grid[k2]-Lz/2.)/h0)**2)*tanh(2.*(z_grid[k2]-Lz/2.)/h0)*sin(2.*np.pi*x_grid[k1]/Lx)
    return 

# <markdowncell>

# Assign the initial conditions. Allow v1 to be odd in the x direction but test the even-ness of the function in the y direction.

# <codecell>

TTop = .5
TBot = -.5
T0 = 0.
STop = -1.
SBot = 1.
S0 = 0.
DeltaTT = TTop-TBot
DeltaSS = STop-SBot
h0=1.
set_TT_initial(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,TT,T0,TTop,TBot,Lx)
set_SS_initial(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,SS,S0,STop,SBot,Lx)
set_TT_der(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,TT_der_exact,T0,TTop,TBot,Lx)
set_SS_der(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,SS_der_exact,S0,STop,SBot,Lx)
set_TT_z(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,TT_z_exact,T0,TTop,TBot,Lx)
set_SS_z(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,SS_z_exact,S0,STop,SBot,Lx)
print TT[1,:]
print SS[1,:]

set_v1_initial(x_grid,z_grid,N1,N2,v1,Lx,Lz)
set_v2_initial(x_grid,z_grid,N1,N2,v2,Lx,Lz)

set_v1_der_exact_x_z(x_grid,z_grid,N1,N2,v1_dx_dz_exact,Lx,Lz)
set_v1_der2_exact(x_grid,z_grid,N1,N2,v1_der2_exact,Lx,Lz)
set_v1_x_exact(x_grid,z_grid,N1,N2,v1_x_exact,Lx,Lz)
set_v1_xx_exact(x_grid,z_grid,N1,N2,v1_xx_exact,Lx,Lz)
set_v2_der_exact_x_z(x_grid,z_grid,N1,N2,v2_dx_dz_exact,Lx,Lz)
set_v2_der2_exact(x_grid,z_grid,N1,N2,v2_der2_exact,Lx,Lz)
set_v2_x_exact(x_grid,z_grid,N1,N2,v2_x_exact,Lx,Lz)
set_v2_xx_exact(x_grid,z_grid,N1,N2,v2_xx_exact,Lx,Lz)
set_TT_der2(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,TT_der2_exact,T0,TTop,TBot,Lx)
set_TT_zz(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,TT_zz_exact,T0,TTop,TBot,Lx)
set_SS_der2(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,SS_der2_exact,S0,STop,SBot,Lx)
set_TT_der2(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,TT_der2_exact,T0,TTop,TBot,Lx)
set_SS_zz(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,SS_zz_exact,S0,STop,SBot,Lx)


fig_TS = pylab.figure()
fig,ax = subplots()
p1, = plot(TT_der_exact[1,:],z_grid)
p2, = plot(SS_der_exact[1,:],z_grid)
legend([p1, p2], ["TT_der_exact", "SS_der_Exact"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('Tder and Sder profiles down a line')
plt.xlabel('Tder and Sder -- antisymmetric')
plt.ylabel('z')
pylab.show()


fig_velocities = pylab.figure()
fig,ax =subplots()
p3, = plot(x_grid,v1[:,5])
p4, = plot(x_grid,v2[:,5])
legend([p3, p4], ["v1","v2"])
plt.xlabel('x')
plt.title('x dependency of v1 and v2')
plt.ylabel('v1 or v2')
pylab.show()

fig_velocities_der = pylab.figure()
fig,ax =subplots()
p3, = plot(x_grid,v1_dx_dz_exact[:,5])
p4, = plot(x_grid,v2_dx_dz_exact[:,5])
legend([p3, p4], ["v1 der","v2 der"])
plt.xlabel('x')
plt.title('x dependency of v1der  and v2der')
plt.ylabel('v1der or v2der')
pylab.show()

# <markdowncell>

# Do the cosine transform for 1 dimension

# <codecell>

f1D = random.random_sample((N1))
    
for k1 in range(N1):
    f1D[k1] = cos(2.*np.pi*z_grid[k1]/Lz)

print f1D
    
f1DT = fftpack.dct(f1D,type=1)/(np.float_(N2)-1.)

print f1DT

f1D_back = fftpack.idct(f1DT,type=1)*.5

print f1D_back

print 'max error is ',max(abs(reshape(f1D-f1D_back,-1)))
fig_1D = pylab.figure()
fig,ax = subplots()
p1, = plot(z_grid,f1D)
p2, = plot(z_grid,f1D_back)


# <markdowncell>

# Now set up the new 2D FFT/FCT calls

# <codecell>

#
# Testing 2D fourier transforms of real functions before doing so with the cosin function
#

v1_hat = np.fft.fft2(v1)
v2_hat = np.fft.fft2(v2)

print max(abs(reshape(v1_hat,-1)))

#
# For the -sin(x) cos(z) we should have two wave number one functions
#
print v1_hat[0,1],v1_hat[1,1],v1_hat[1,0]
#
# For the -cos(z)  function that would be just in one index
#
print v2_hat[0,1],v2_hat[1,0],v2_hat[1,1]

#
#
#

v1_hat_test = np.zeros((N1,N2))*complex(0.0,0.0)
v2_hat_test = np.zeros((N1,N2))*complex(0.0,0.0)


for l in range(N1):
    v1_hat_test[l,:] = np.fft.fft(v1[l,:])
        
for q in range(N2):
    v1_hat_test[:,q] = np.fft.fft(v1_hat_test[:,q])
    
print 'error is ',max(reshape(v1_hat_test - v1_hat,-1))

print 'max is ',max(abs(reshape(v1_hat_test,-1)))



# <codecell>

fig_v1 = pylab.figure()
fig, ax = subplots()
cnt=contourf(x_grid,z_grid,v1, cmap=cm.RdBu)
cb = fig.colorbar(cnt)
plt.title('v1 contours')

fig_v1_der = pylab.figure()
fig, ax = subplots()
cntv1der=contourf(x_grid,z_grid,v1_dx_dz_exact, cmap=cm.RdBu)
cb = fig.colorbar(cntv1der)
plt.title('v1_der contours')

fig_v2= pylab.figure()
fig, ax = subplots()
#cnt2=contour(x_grid,z_grid,v2, cmap=cm.RdBu)
cnt2v2=contourf(x_grid,z_grid,v2, cmap=cm.RdBu)
cb2 = fig.colorbar(cnt2v2)
plt.title('v2 contours')

fig_v2_der= pylab.figure()
fig, ax = subplots()
#cnt2=contour(x_grid,z_grid,v2, cmap=cm.RdBu)
cnt2der=contourf(x_grid,z_grid,v2_dx_dz_exact, cmap=cm.RdBu)
cb2v1der = fig.colorbar(cnt2der)
plt.title('v2_der contours')

fig_velocities_der2 = pylab.figure()
fig,ax =subplots()
p3, = plot(x_grid,v1_dx_dz_exact[:,5])
p4, = plot(x_grid,v2_dx_dz_exact[:,5])
legend([p3, p4], ["v1 der","v2 der"])
plt.xlabel('x')
plt.title('x dependency of v1der  and v2der')
plt.ylabel('v1der or v2der')
pylab.show()

fig_TT= pylab.figure()
fig, ax = subplots()
#cnt2=contour(x_grid,z_grid,v2, cmap=cm.RdBu)
cnt2TT=contourf(x_grid,z_grid,TT, cmap=cm.RdBu)
cb2 = fig.colorbar(cnt2TT)
plt.title('TT contours')


fig_SS=pylab.figure()
fig, ax = subplots()
#cnt2=contour(x_grid,z_grid,v2, cmap=cm.RdBu)
cnt2SS=contourf(x_grid,z_grid,SS, cmap=cm.RdBu)
cb2 = fig.colorbar(cnt2SS)
plt.title('SS contours')

# <codecell>

#
# Testing 2D transforms with cosine transform in z

#
# THE Trick IS IN THE ORDERING
#

#
# On the way 'there' you do cosine or real transformation first since the data is real
#
for l in range(N2):
    v1_hat_test[l,:] = fftpack.dct(v1[l,:],type=1)/(np.float_(N2)-1.)
#
# In step two you transform with Fourier.
#
for q in range(N1):
    v1_hat_test[:,q] = np.fft.fft(v1_hat_test[:,q])    
#
# And now go backwards to see if we get the same thing
#

v1_back_test = v1_hat_test*complex(0.0,0.0)
v1_back_test_real = np.zeros((N1,N2))

#
# Here  first take the ifft since you'll get Reals back from the complex coefficients.
#
for k in range(N2):
    v1_back_test[:,k] = np.fft.ifft(v1_hat_test[:,k])
v1_back_test_real = real(v1_back_test)
    
print max(reshape(v1_back_test_real-real(v1_back_test),-1))
#
# Now that you have Reals take the inverse cosine transform
#
for l in range(N1):
    v1_back_test_real[l,:] = fftpack.idct(v1_back_test_real[l,:],type=1)*.5
    
#print v1[1,:]
print max(reshape(v1_back_test_real - v1,-1))
    

# <codecell>

#
# FFT in X, Cosine FFT in Z (cosine transform)
#
def FFT_FCT(N1,N2,F):
    F_hat = complex(0.,0.)*np.zeros((N1,N2))
#
#   Cosine Transform first
#
    for l in range(N1):
        F_hat[l,:] = fftpack.dct(F[l,:],type=1)/(np.float_(N2)-1.)
#
#  Then Fourier Transform
#
    for q in range(N2):
        F_hat[:,q] = np.fft.fft(F_hat[:,q])    
#
#  No normalization in this configuration
#
    return F_hat

#
# Inverse of FFT in X, Cosine FFT in Z
#

def iFFT_FCT(N1,N2,F_hat):
    F = np.zeros((N1,N2))
    for k in range(N2):
        F[:,k] = np.fft.ifft(F_hat[:,k]).real
#
#   The F's are reals once we take the ifft, but they have complex parts that won't work
#   with the consine transform, so we set them all real
#
#    F = real(F)
    
#
# Now that you have Reals take the inverse cosine transform
#
    for l in range(N1):
        F[l,:] = fftpack.idct(F[l,:],type=1)*.5
#
# Normalize for the inverse cosine transform
#
    
    return F
#
# FFT in X, SINE FFT in Z (sine transform)
#
def FFT_FST(N1,N2,F):
    F_hat = complex(0.,0.)*np.zeros((N1,N2))
#
#   Sine Transform first
#
    for l in range(N1):
        F_hat[l,1:N1-1] = fftpack.dst(F[l,1:N1-1],type=1)/(np.float_(N2)-1.)
#
#  Then Fourier Transform
#
    for q in range(N2):
        F_hat[:,q] = np.fft.fft(F_hat[:,q])    
#
#  No normalization in this configuration
#
    return F_hat

#
# Inverse of FFT in X, SINE FFT in Z
#

def iFFT_FST(N1,N2,F_hat):
    F = np.zeros((N1,N2))
    for k in range(N1):
        F[:,k] = np.fft.ifft(F_hat[:,k]).real
#
#   The F's are reals once we take the ifft, but they have complex parts that won't work
#   with the sine transform, so we set them all real
#
#    F = real(F)
    
#
# Now that you have Reals take the inverse sine transform
#
    for l in range(N2):
        F[l,1:N2-1] = fftpack.idst(F[l,1:N2-1],type=1)*.5
#
# Normalize for the inverse cosine transform
#
#    F = F/2./N2

    return F

# <markdowncell>

# Test and evaluate the new transforms

# <codecell>

set_v1_initial(x_grid,z_grid,N1,N2,v1,Lx,Lz)
set_v1_der_exact_x_z(x_grid,z_grid,N1,N2,v1_dx_dz_exact,Lx,Lz)
set_v2_initial(x_grid,z_grid,N1,N2,v2,Lx,Lz)
set_v2_der_exact_x_z(x_grid,z_grid,N1,N2,v2_dx_dz_exact,Lx,Lz)
set_TT_initial(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,TT,T0,TTop,TBot,Lx)
set_TT_der(DeltaTT,h0,Lz,x_grid,z_grid,N1,N2,TT_der_exact,T0,TTop,TBot,Lx)
set_SS_initial(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,SS,S0,STop,SBot,Lx)
set_SS_der(DeltaSS,h0,Lz,x_grid,z_grid,N1,N2,SS_der_exact,S0,STop,SBot,Lx)


v1_hat = complex(0.,0.)*np.zeros((N1,N2))
v1_der_hat = np.zeros((N1,N2))
v2_hat = complex(0.,0.)*np.zeros((N1,N2))
TT_hat = complex(0.,0.)*np.zeros((N1,N2))
SS_hat = complex(0.,0.)*np.zeros((N1,N2))

#
# Transform to spectral space
#
v1_hat = FFT_FCT(N1,N2,v1)
v2_hat = FFT_FST(N1,N2,v2)
TT_hat = FFT_FST(N1,N2,TT)
SS_hat = FFT_FST(N1,N2,SS)


#
#  Go back to physical space and see what everything looks like
#
v1_return = iFFT_FCT(N1,N2,v1_hat)
v2_return = iFFT_FST(N1,N2,v2_hat)
TT_return = iFFT_FST(N1,N2,TT_hat)
SS_return = iFFT_FST(N1,N2,SS_hat)


print 'v1 function max error = ',max(reshape(v1 - v1_return,-1))
print 'v2 function max error = ',max(reshape(v2 - v2_return,-1))
print 'TT function max error = ',max(reshape(TT - TT_return,-1))
print 'SS function max error = ',max(reshape(SS - SS_return,-1))

#
# Take Derivatives and go back to physical space
#
#
#        First for the cosine vertical derivative
#
for k in range(N2):
    v1_dx_dz[:,k] = np.fft.ifft(1j*kk*v1_hat[:,k]).real
for ll in range(N1):
    v1_dx_dz[ll,1:N2-1] = fftpack.idst(-kk_cosine[1:N2-1]*v1_dx_dz[ll,1:N2-1],type=1)*.5
    v1_dx_dz[ll,0]=0.
    v1_dx_dz[ll,N2-1] = 0.
#print 'v1derhat',v1_der_hat[2,:]
    
    
print 'v1 der max error = ',max(reshape(v1_dx_dz - v1_dx_dz_exact,-1))

#
#        Then for the sine vertical derivative
#
for k in range(N2):
    v2_dx_dz[:,k] = np.fft.ifft(1j*kk*v2_hat[:,k]).real
    v2_dx_dz[:,0] = 0.
    v2_dx_dz[:,N2-1] = 0.
for ll in range(N1):
    v2_dx_dz[ll,:] = fftpack.idct(kk_cosine[:]*v2_dx_dz[ll,:],type=1)*.5

print 'v2 der max error = ',max(reshape(v2_dx_dz - v2_dx_dz_exact,-1))


#
#        Then for the TT vertical derivative
#
for k in range(N2):
    TT_der[:,k] = np.fft.ifft(1j*kk*TT_hat[:,k]).real
for l in range(N1):
    TT_der[l,:] = fftpack.idct(kk_cosine[:]*TT_der[l,:],type=1)*.5

print 'TT der max error = ',max(reshape(TT_der - TT_der_exact,-1))


#
#        Then for the SS vertical derivative
#
for k in range(N2):
    SS_der[:,k] = np.fft.ifft(1j*kk*SS_hat[:,k]).real
for l in range(N1):
    SS_der[l,:] = fftpack.idct(kk_cosine[:]*SS_der[l,:],type=1)*.5

print 'SS der max error = ',max(reshape(SS_der - SS_der_exact,-1))

fig_v1_der_return_error = pylab.figure()
fig,ax = subplots()
p1v1, = plot(v1[1,:],z_grid)
p2v1, = plot(v1_dx_dz_exact[1,:],z_grid)
p3v1, = plot(v1_dx_dz[1,:],z_grid)
#p4, = plot(SS[1,:],z_grid)
legend([p1v1, p1v1, p3v1],["v1", "v1_der_exact", "v1_der"])
plt.title('v1 profiles down a line -- on return from the fft')
plt.xlabel('v1')
plt.ylabel('z')
pylab.show()

fig_v2_der_return_error = pylab.figure()
fig,ax = subplots()
p1v1, = plot(v2[1,:],z_grid)
p2v1, = plot(v2_dx_dz_exact[1,:],z_grid)
p3v1, = plot(v2_dx_dz[1,:],z_grid)
#p4, = plot(SS[1,:],z_grid)
legend([p1v1, p1v1, p3v1],["v2", "v2_der_exact", "v2_der"])
plt.title('v2 profiles down a line -- on return from the fft')
plt.xlabel('v2')
plt.ylabel('z')
pylab.show()



v1_der_error = v1_dx_dz - v1_dx_dz_exact
fig_v1_error = pylab.figure()
fig, ax = subplots()
cnt=contourf(x_grid,z_grid,v1_der_error, cmap=cm.RdBu)
cb = fig.colorbar(cnt)

fig_v1_dx_dz = pylab.figure()
fig, ax = subplots()
cnt=contourf(x_grid,z_grid,v1_dx_dz, cmap=cm.RdBu)
cb = fig.colorbar(cnt)

fig_v1_dx_dz_exact = pylab.figure()
fig, ax = subplots()
cnt=contourf(x_grid,z_grid,v1_dx_dz_exact, cmap=cm.RdBu)
cb = fig.colorbar(cnt)

v2_der_error = v2_dx_dz - v2_dx_dz_exact
fig_v1_error = pylab.figure()
fig, ax = subplots()
cnt=contourf(x_grid,z_grid,v2_der_error, cmap=cm.RdBu)
cb = fig.colorbar(cnt)
#
# Make some plots
#
fig_T_return = pylab.figure()
fig,ax = subplots()
p1, = plot(TT_return[1,:],z_grid)
#p2, = plot(SS_return[1,:],z_grid)
p3, = plot(TT[1,:],z_grid)
#p4, = plot(SS[1,:],z_grid)
legend([p1, p3],["TT_return", "TT"])
#legend([p1, p2, p3, p4], ["TT_return", "SS_return", "TT", "SS"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('T and S profiles down a line -- on return from the fft')
plt.xlabel('T and S -- antisymmetric')
plt.ylabel('z')
pylab.show()

fig_T_der_return = pylab.figure()
fig,ax = subplots()
p1, = plot(TT[1,:],z_grid)
p2, = plot(TT_der_exact[1,:],z_grid)
p3, = plot(TT_der[1,:],z_grid)
#p4, = plot(SS[1,:],z_grid)
legend([p1, p1, p3],["TT", "TT_der_exact", "TT_der"])
#legend([p1, p2, p3, p4], ["TT_return", "SS_return", "TT", "SS"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('Tprofiles down a line -- on return from the fft')
plt.xlabel('T  -- antisymmetric')
plt.ylabel('z')
pylab.show()

fig_S_der_return = pylab.figure()
fig,ax = subplots()
p1, = plot(SS[1,:],z_grid)
p2, = plot(SS_der_exact[1,:],z_grid)
p3, = plot(SS_der[1,:],z_grid)
#p4, = plot(SS[1,:],z_grid)
legend([p1, p1, p3],["SS", "SS_der_exact", "SS_der"])
#legend([p1, p2, p3, p4], ["TT_return", "SS_return", "TT", "SS"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('S profiles down a line -- on return from the fft')
plt.xlabel('S  -- antisymmetric')
plt.ylabel('z')
pylab.show()


fig_S_return = pylab.figure()
fig,ax = subplots()
p2, = plot(SS_return[1,:],z_grid)
p4, = plot(SS[1,:],z_grid)
legend([p2, p4],["SS_return", "SS"])
fig_v1 = pylab.figure()

fig,ax =subplots()
p5, = plot(x_grid,v1_return[:,5])
p6, = plot(x_grid,v1[:,5])
legend([p5, p6], ["v1_return","v1"])

fig_v2 = pylab.figure()
fig,ax =subplots()
p7, = plot(x_grid,v2_return[:,5])
p8, = plot(x_grid,v2[:,5])
legend([p7, p8], ["v2_return","v2"])
plt.xlabel('x')
plt.title('x dependency of v1_return and v2_return')
plt.ylabel('v1 or v2')
pylab.show()

# <markdowncell>

# Test and evalute second derivatives

# <codecell>


#
#        First for the cosine vertical derivative
#
for k in range(N2):
    v1_der2[:,k] = np.fft.ifft(-kk*kk*v1_hat[:,k]).real
for l in range(N1):
    v1_der2[l,:] = fftpack.idct(-kk_cosine[:]*kk_cosine[:]*v1_der2[l,:],type=1)*.5
      
print 'v1 der max error = ',max(reshape(v1_der2 - v1_der2_exact,-1))

#
#        Then for the sine vertical derivative
#
for k in range(N2):
    v2_der2[:,k] = np.fft.ifft(-kk*kk*v2_hat[:,k]).real
for ll in range(N1):
    v2_der2[ll,1:N2-1] = fftpack.idst(-kk_cosine[1:N2-1]*kk_cosine[1:N2-1]*v2_der2[ll,1:N2-1],type=1)*.5
    v2_der2[ll,0]=0.
    v2_der2[ll,N2-1] = 0.
   
print 'v2 der max error = ',max(reshape(v2_der2 - v2_der2_exact,-1))

#
#        Then for the TT vertical derivative
#
for k in range(N2):
    TT_der2[:,k] = np.fft.ifft(-kk*kk*TT_hat[:,k]).real
for l in range(N1):
    TT_der2[l,1:N2-1] = fftpack.idst(-kk_cosine[1:N2-1]*kk_cosine[1:N2-1]*TT_der2[l,1:N2-1],type=1)*.5

print 'TT der max error = ',max(reshape(TT_der2 - TT_der2_exact,-1))

#
#        Then for the SS vertical derivative
#
for k in range(N2):
    SS_der2[:,k] = np.fft.ifft(-kk*kk*SS_hat[:,k]).real
for l in range(N1):
    SS_der2[l,1:N2-1] = fftpack.idst(-kk_cosine[1:N2-1]*kk_cosine[1:N2-1]*SS_der2[l,1:N2-1],type=1)*.5

print 'SS der max error = ',max(reshape(SS_der2 - SS_der2_exact,-1))

# <codecell>

fig_v1_der2_return_error = pylab.figure()
fig,ax = subplots()
p1v1der2, = plot(v1[1,:],z_grid)
p2v1der2, = plot(v1_der2_exact[1,:],z_grid)
p3v1der2, = plot(v1_der2[1,:],z_grid)
#p4, = plot(SS[1,:],z_grid)
legend([p1v1, p1v1, p3v1],["v1", "v1_der2_exact", "v1_der2"])
plt.title('v1 der2 profiles down a line -- on return from the fft')
plt.xlabel('v1 der2')
plt.ylabel('z')
pylab.show()

fig_v2_der2_return_error = pylab.figure()
fig,ax = subplots()
p1v1der2, = plot(v2[1,:],z_grid)
p2v1der2, = plot(v2_der2_exact[1,:],z_grid)
p3v1der2, = plot(v2_der2[1,:],z_grid)
#p4, = plot(SS[1,:],z_grid)
legend([p1v1, p1v1, p3v1],["v2", "v2_der_exact", "v2_der"],bbox_to_anchor=(1.5, 1.05))
plt.title('v2 profiles down a line -- on return from the fft')
plt.xlabel('v2')
plt.ylabel('z')
pylab.show()

v2_der2_error = v2_der2 - v2_der2_exact
fig_v2_error = pylab.figure()
fig, ax = subplots()
cntv2der2=contourf(x_grid,z_grid,v2_der2_error, cmap=cm.RdBu)
cb = fig.colorbar(cntv2der2)

fig_SS_der2_return_error = pylab.figure()
fig,ax = subplots()
p1SSder2, = plot(SS[1,:],z_grid)
p2SSder2, = plot(SS_der2_exact[1,:],z_grid)
p3SSder2, = plot(SS_der2[1,:],z_grid)
legend([p1SSder2, p1SSder2, p3SSder2],["SS", "SS_der2_exact", "SS_der2"],bbox_to_anchor=(1.5, 1.05))
plt.title('SS der profiles down a line -- on return from the fft')
plt.xlabel('SS')
plt.ylabel('z')
pylab.show()

fig_TT_der2_return_error = pylab.figure()
fig,ax = subplots()
p1TTder2, = plot(TT[1,:],z_grid)
p2TTder2, = plot(TT_der2_exact[1,:],z_grid)
p3TTder2, = plot(TT_der2[1,:],z_grid)
legend([p1TTder2, p1TTder2, p3TTder2],["TT", "TT_der2_exact", "TT_der2"],bbox_to_anchor=(1.5, 1.05))
plt.title('TT der profiles down a line -- on return from the fft')
plt.xlabel('TT')
plt.ylabel('z')
pylab.show()

# <codecell>

print TT_der2_exact[5,:]

# <codecell>

print TT_der2[5,:]

# <markdowncell>

# Now put all these derivatives in defs and test again!

# <codecell>

#
# The following are for computing first derivatives
#


def iFFT_FST_X(N1,N2,F_hat):
    F = np.zeros((N1,N2))
    for k in range(N2):
        F[:,k] = np.fft.ifft(1j*kk*F_hat[:,k]).real
    
    for ll in range(N1):
        F[ll,1:N2-1] = fftpack.idst(F[ll,1:N2-1],type=1)*.5
        F[ll,0]=0.
        F[ll,N2-1] = 0.
        
    return F

def iFFT_FCT_X(N1,N2,F_hat):
    F = np.zeros((N1,N2))
    for k in range(N2):
        F[:,k] = np.fft.ifft(1j*kk*F_hat[:,k]).real
    
    for ll in range(N1):
        F[ll,:] = fftpack.idct(F[ll,:],type=1)*.5
        
    return F

def iFFT_FST_Z(N1,N2,F_hat):
    F = np.zeros((N1,N2))
    for k in range(N2):
        F[:,k] = np.fft.ifft(F_hat[:,k]).real
    for ll in range(N1):
        F[ll,1:N2-1] = fftpack.idst(kk_cosine[1:N2-1]*F[ll,1:N2-1],type=1)*.5
        F[ll,0]=0.
        F[ll,N2-1] = 0.
        
    return F


def iFFT_FCT_Z(N1,N2,F_hat):
    F = np.zeros((N1,N2))
    
    for k in range(N2):
        F[:,k] = np.fft.ifft(F_hat[:,k]).real
        
    for l in range(N1):
        F[l,:] = fftpack.idct(kk_cosine[:]*F[l,:],type=1)*.5
        
    return F

#
# The following are for the second derivatives
#


def iFFT_FST_XX(N1,N2,F_hat):
    F = np.zeros((N1,N2))
    for k in range(N2):
        F[:,k] = np.fft.ifft(-kk*kk*F_hat[:,k]).real
    
    for ll in range(N1):
        F[ll,1:N2-1] = fftpack.idst(F[ll,1:N2-1],type=1)*.5
        F[ll,0]=0.
        F[ll,N2-1] = 0.
        
    return F

def iFFT_FCT_XX(N1,N2,F_hat):
    F = np.zeros((N1,N2))
    for k in range(N2):
        F[:,k] = np.fft.ifft(-kk*kk*F_hat[:,k]).real
    
    for ll in range(N1):
        F[ll,:] = fftpack.idct(F[ll,:],type=1)*.5
        
    return F

def iFFT_FST_ZZ(N1,N2,F_hat):
    F = np.zeros((N1,N2))
    for k in range(N2):
        F[:,k] = np.fft.ifft(F_hat[:,k]).real
    for ll in range(N1):
        F[ll,1:N2-1] = fftpack.idst(-kk_cosine[1:N2-1]*kk_cosine[1:N2-1]*F[ll,1:N2-1],type=1)*.5
        F[ll,0]=0.
        F[ll,N2-1] = 0.
        
    return F


def iFFT_FCT_ZZ(N1,N2,F_hat):
    F = np.zeros((N1,N2))
    
    for k in range(N2):
        F[:,k] = np.fft.ifft(F_hat[:,k]).real
        
    for l in range(N1):
        F[l,:] = fftpack.idct(-kk_cosine[1:N2-1]*kk_cosine[:]*F[l,:],type=1)*.5
        
    return F

# <markdowncell>

# Test the first derivative functions, using already the initial values of SS and so forth.  First the vertical derivatives.

# <codecell>

SS_z = iFFT_FCT_Z(N1,N2,SS_hat)
TT_z = iFFT_FCT_Z(N1,N2,TT_hat)
SS_zz = iFFT_FST_ZZ(N1,N2,SS_hat)
TT_zz = iFFT_FST_ZZ(N1,N2,TT_hat)

print 'SS_z max error = ',max(reshape(SS_z - SS_z_exact,-1))
print 'TT_z max error = ',max(reshape(TT_z - TT_z_exact,-1))
print 'SS_zz max error = ',max(reshape(SS_zz - SS_zz_exact,-1))
print 'TT_zz max error = ',max(reshape(TT_zz - TT_zz_exact,-1))

fig_S_z = pylab.figure()
fig,ax = subplots()
p1, = plot(SS[1,:],z_grid)
p2, = plot(SS_z_exact[1,:],z_grid)
p3, = plot(SS_z[1,:],z_grid)
legend([p1, p1, p3],["SS", "SS_z_exact", "SS_z"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('S profiles down a line')
plt.xlabel('S  -- antisymmetric')
plt.ylabel('z')
pylab.show()

fig_SS_z = pylab.figure()
fig,ax = subplots()
p1, = plot(SS[1,:],z_grid)
p2, = plot(SS_zz_exact[1,:],z_grid)
p3, = plot(SS_zz[1,:],z_grid)
legend([p1, p1, p3],["SS", "SS_zz_exact", "SS_zz"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('S profiles down a line')
plt.xlabel('S  -- antisymmetric')
plt.ylabel('z')
pylab.show()

fig_T_z = pylab.figure()
fig,ax = subplots()
p1, = plot(TT[1,:],z_grid)
p2, = plot(TT_z_exact[1,:],z_grid)
p3, = plot(TT_z[1,:],z_grid)
legend([p1, p1, p3],["TT", "TT_z_exact", "TT_z"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('T profiles down a line')
plt.xlabel('T  -- antisymmetric')
plt.ylabel('z')
pylab.show()

fig_TT_z = pylab.figure()
fig,ax = subplots()
p1, = plot(TT[1,:],z_grid)
p2, = plot(TT_zz_exact[1,:],z_grid)
p3, = plot(TT_zz[1,:],z_grid)
legend([p1, p1, p3],["TT", "TT_zz_exact", "TT_zz"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('T profiles down a line')
plt.xlabel('T  -- antisymmetric')
plt.ylabel('z')
pylab.show()

# <markdowncell>

# Then the horizontal derivatives. IN this case you don't use opposite FCT FST, because the derivatives are horiztonal.

# <codecell>

v1_x = iFFT_FCT_X(N1,N2,v1_hat)
v2_x = iFFT_FST_X(N1,N2,v2_hat)
v1_xx = iFFT_FCT_XX(N1,N2,v1_hat)
v2_xx = iFFT_FST_XX(N1,N2,v2_hat)

print 'v1_x max error = ',max(reshape(v1_x - v1_x_exact,-1))
print 'v2_x max error = ',max(reshape(v2_x - v2_x_exact,-1))
print 'v1_xx max error = ',max(reshape(v1_xx - v1_xx_exact,-1))
print 'v2_xx max error = ',max(reshape(v2_xx - v2_xx_exact,-1))


# <codecell>


