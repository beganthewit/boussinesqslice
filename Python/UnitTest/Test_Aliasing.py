#
# This program unit tests the Aliasing functionality
#

import numpy as np
from numpy import fft,less,logical_and
from numpy.linalg import inv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import CosineSineTransforms as cst
from CosineSineTransforms import *
import string as ss


def set_f_exact(x_grid,z_grid,N1,N2,Lx,Lz,nx1,nz1,nx2,nz2,sinx_coeff,cosx_coeff):
    """ This function is used for testing the dealiasing. It provides functionality for different wave numbers in z and x. 

    x_grid = a fourier grid of points
    z_grid = a sine series grid of points
    N1 = number of horizontal (x) points
    N2 = number of vertical (z) points
    nx1 = x wave number of first term
    nz1 = z wave number of first term
    nx2 = x wave number of second term
    nz2 = z wave number of second term
    sinx_coeff = coefficient of the sin sin term (first term)
    cosx_coeff = coefficient of the sin cos term (second term)

    """
    
    f = np.zeros((N1,N2))
    for k1 in range(N1):
        for k2 in range(N2):
            f[k1,k2] = sinx_coeff*sin(2.*np.pi*x_grid[k1]*nx1/Lx)*sin(np.pi*z_grid[k2]*nz1/Lz) + \
                       cosx_coeff*cos(2.*np.pi*x_grid[k1]*nx2/Lx)*sin(np.pi*z_grid[k2]*nz2/Lz)
    return f

def check_aliasing(N1, N2, Lx, Lz, kk, kk_cosine, f, f_aliased):
    """ This function checks to see if the 2/3 truncation rule is working """
    N1_trunc = np.int(N1/3)
    N2_trunc = np.int(2*N2/3)
    for k1 in range(N1):
        for k2 in range(N2):
            if np.abs(kk[k1]*Lx/(2*np.pi)) >= N1_trunc and kk_cosine[k2]*Lz/np.pi >= N2_trunc:
                if np.abs(f_aliased[k1,k2]) > 0.:
                    print "De Aliasing Not working", kk[k1]*Lx/(2*np.pi),k2*Lz/np.pi,f_aliased[k1,k2],f[k1,k2]
    return

def coefficient_visual(N1, N2, Lx, Lz, kk, kk_cosine, f):
    """ This function will print out a visual grid of where a 2D complex spectra has values 
        Because this function is special for complex exponential series in the x direction and
        a fourier sine series in the z (vertical) direction, it will print out a visual that starts with 

      z = N2  X.0 X.X .. (etc)
        .
        .
        .
       z = 0 (-N1/2 <=  k <= N1/2-1 )


       Where there is a capitol X there is also a nonzero value. Where there is  a blank space there is a very small or zero
       value.  The format is (REAL.IMAG)

    """
    graphic_string_array = np.array(range(N1), dtype='<S4')
    epsilon = 1.e-10
    kk_shifted = np.fft.fftshift(kk*Lx/(2*np.pi))
    print "x = ",kk_shifted[:]
    for k2 in range(N2):
        graphic_string_array[:] = 'Null'
        for k1 in range(N1):
            # print " wave numbers " , k2, kk_shifted[k1]
            if np.abs(f[k1,k2].real) > epsilon:
                if np.abs(f[k1,k2].imag) > epsilon:
                    coef_string = 'X:X'
                else:
                    coef_string = 'X: '
            else:
                if np.abs(f[k1,k2].imag) > epsilon:
                    coef_string = ' :X'
                else:
                    coef_string = ' : '
            #print "coef_string = ", coef_string
            graphic_string_array[k1] = coef_string                
        print "z = ",k2, ss.join(np.fft.fftshift(graphic_string_array[:]),' ')


N1 = 16
N2 = 16

# Set two lengths for width and height
Lx = np.float_(10.)
Lz = np.float_(5.)
delta_x = Lx/np.float_(N1)
delta_z = Lz/(np.float_(N2)-1.)
#
# The other important thing to compute is the wave number array (frequencies)
#
kk = np.fft.fftfreq(N1,Lx/N1)*2*np.pi
#
# And for the cosine series
#
kk_cosine = np.arange((N2))*np.pi/Lz
#
# Because we have all the wave number data we can set up the aliasing
#
#
# The following is to dealias a 2D array. It is a
# Logical expression that, when multiplied, selects part of the elements
# to keep, and to set others to zero. To use dealias spectral coefficients
# F_hat use
#
# F_hat_dealiased = DeAlias*F_hat
#
#
kkx,kkz=np.meshgrid(kk,kk_cosine, indexing='ij')
DeAlias = np.logical_and(
    np.less(np.abs(kkx*Lx/(2.*np.pi)),int(N1/3)),
    np.less(np.abs(kkz*Lz/(np.pi)),int(2*N2/3)))
#DeAlias=1.0 # Use this when not using dealiasing
                                                                        
#
# We need a Fourier Grid, we need a z-grid for the cosine/sine series 
#
x_grid = np.arange((float(N1)))
z_grid = np.arange((float(N2)))
x_grid[0] = 0.
z_grid[0] = 0.

for k in arange(1,N1):
        x_grid[k] = x_grid[k-1] + delta_x

for k in arange(1,N2):
        z_grid[k] = z_grid[k-1] + delta_z

#
# Test a sin-sin and cos-sin FFT
#
f = set_f_exact(x_grid, z_grid, N1, N2, Lx, Lz, 1., 1., 7., 8., 1., 1.)
f_rand = random.random_sample((N1,N2))*complex(.5,.5)
print f_rand

f_hat_full = cst.FFT_FST(N1, N2, f)
f_rand_hat_full = cst.FFT_FST(N1, N2, f_rand)


f_hat_aliased = DeAlias*f_hat_full
f_rand_hat_aliased = DeAlias*f_rand_hat_full

# Check to make sure the de-aliased parts of the spectrum have been set to zero

check_aliasing(N1, N2, Lx, Lz, kk, kk_cosine, f_hat_full, f_hat_aliased)
check_aliasing(N1, N2, Lx, Lz, kk, kk_cosine, f_rand_hat_full, f_rand_hat_aliased)


print "The following is a visual for the full, unaliased array"
coefficient_visual(N1, N1, Lx, Lz, kk, kk_cosine, f_rand_hat_full)

print "The following is a visual for the de-aliased array. Cut-offs should be in place for:"
print "z >= ",2*N2/3, "Notice there is no z = 0 mode for sine series in the vertical."
print "x >= ",N1/3
coefficient_visual(N1, N2, Lx, Lz, kk, kk_cosine, f_rand_hat_aliased)




