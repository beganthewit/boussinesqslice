#Python program for reading in results from files and plotting curves

import numpy as np
from numpy import *
from scipy import *
from numpy import fft
from scipy import fftpack
import matplotlib.pyplot as plt
import sys
import pdb #to pause execution use pdb.set_trace()
import json
import ast


#Program control section:

TimeSlice = 1
Staircase = 0
ProblemType = 'Layers'
#ProblemType = 'KelvinHelmholtz'

results = 'State'
#results = 'StateFullFields'
#results = 'Wind'
#results = 'Vorticity'
#results = 'SpectralCoef'
#results = 'NaturalBasis'


# Set up domain and grid
# Set two lengths for width and height
Lx = 0.2
Lz = 0.45
#Set up grid size
Nx = 80
Nz = 180

dx = float(Lx)/Nx
dz = float(Lz)/(Nz-1)

x_grid = np.arange(Nx)*dx
z_grid = np.arange(Nz)*dz


if TimeSlice == 1:

    Nx = 80 
    Nz = 180
    #Nx = 40
    #Nz = 90
    nt = 10

    #Initialise required arrays: 
    if results == 'SpectralCoef' or results == 'NaturalBasis': 
        arr1 = np.zeros((Nx,Nz,nt))*1j
        arr2 = np.zeros((Nx,Nz,nt))*1j
        arr3 = np.zeros((Nx,Nz,nt))*1j
    else: 
        if results == 'State' or results == 'StateFullFields' or results == 'Wind' or results == 'Vorticity':
            arr1 = np.zeros((Nx,Nz,nt))
        if results != 'Vorticity':
            arr2 = np.zeros((Nx,Nz,nt))
        if results != 'Wind' and results != 'StateFullFields' and results != 'Vorticity': 
            arr3 = np.zeros((Nx,Nz,nt))

    #Read in results
    for ii in range(0,nt):
 
        i_plots = ii
        print i_plots    
 
        if results == 'State':
            #dir_     = './Results/' + ProblemType + '/State/'
            dir_     = './Results/' + ProblemType + '/State_18/'
            #dir_     = './Results/' + ProblemType + '/State_1/'
            #dir_     = '/Users/pb412/State/'
            fnm1 = dir_ + 'Psi_' + str(i_plots) + '.txt'
            fnm2 = dir_ + 'TT_' + str(i_plots) + '.txt'
            fnm3 = dir_ + 'SS_' + str(i_plots) + '.txt'

        if results == 'StateFullFields':
            dir_     = './Results/' + ProblemType + '/StateFullFields/'
            #dir_     = '/Users/pb412/State/'
            fnm1 = dir_ + 'TT_fullfield_' + str(i_plots) + '.txt'
            fnm2 = dir_ + 'SS_fullfield_' + str(i_plots) + '.txt'

        if results == 'Wind':
            dir_ = './Results/' + ProblemType + '/Wind/'
            fnm1 = dir_ + 'u_' + str(i_plots) + '.txt'
            fnm2 = dir_ + 'w_' + str(i_plots) + '.txt' 

        if results == 'Vorticity':
            dir_ = './Results/' + ProblemType + '/Vorticity/'
            fnm1 = dir_ + 'vorticity_' + str(i_plots) + '.txt'

        if results == 'SpectralCoef':
            dir_ = './Results/' + ProblemType + '/SpectralCoef/'
            fnm1 = dir_ + 'Psi_hat_' + str(i_plots) + '.txt'
            fnm2 = dir_ + 'TT_hat_' + str(i_plots) + '.txt'
            fnm3 = dir_ + 'SS_hat_' + str(i_plots) + '.txt'

        if results == 'NaturalBasis':
            dir_ = './Results/' + ProblemType + '/NaturalBasis/'
            fnm1 = dir_ + 'sigma_1_' + str(i_plots) + '.txt'
            fnm2 = dir_ + 'sigma0_' + str(i_plots) + '.txt'
            fnm3 = dir_ + 'sigma1_' + str(i_plots) + '.txt'


        if results == 'SpectralCoef' or results == 'NaturalBasis':
            tmp1 = np.loadtxt(fnm1).view(complex)
            tmp2 = np.loadtxt(fnm2).view(complex)
            tmp3 = np.loadtxt(fnm3).view(complex)
        else:
            if results == 'State' or results == 'StateFullFields' or results == 'Wind' or results == 'Vorticity':
                tmp1 = np.loadtxt(fnm1)
            if results != 'Vorticity':
                tmp2 = np.loadtxt(fnm2)
            if results != 'Wind' and results != 'StateFullFields' and results != 'Vorticity':      
                tmp3 = np.loadtxt(fnm3)
   
        #pdb.set_trace()

        arr1[:,:,ii] = tmp1
        if results != 'Vorticity':
            arr2[:,:,ii] = tmp2
        if results != 'Wind' and results != 'StateFullFields' and results != 'Vorticity':
            arr3[:,:,ii] = tmp3



if Staircase == 1:

    x_idx = 40
    #dtPlot = 60
    dtPlot = 5
    ntPlot = int(nt/dtPlot)
    time_vec = np.arange(ntPlot)*dtPlot
    for tt in time_vec:
        plt.plot(arr2[x_idx,:,tt],z_grid)

    plt.show()





pdb.set_trace()

