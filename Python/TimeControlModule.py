# This program solves the 2D Boussinesq equation using the fourier
# method with particular symetries. 

import numpy as np
from numpy import reshape
#from numpy import fft
#import matplotlib.pyplot as plt
import CosineSineTransforms as cst
import sys 



def TimeControl(t, params, state, Nx, Nz, x_grid, z_grid, kk, kk_cosine, scheme, HyperTypes):
    """ This function outputs the maximum time step for the frequency and decay rate """
    g, rho0, ct, cs, bt, bs, nu, kappat, kappas = params
    
    hyper_Psi, hyper_T, hyper_S = HyperTypes
    
    psi = state[0]
    TT  = state[1]
    SS  = state[2]

    psi_hat = np.zeros((Nx,Nz))*1j

    # To find the cfl for u and w (though we're solveing psi, but I hope I don't have to compute
    # It might be better to put this in the rhs subroutine, but for now we'll put it here
    #

    psi_hat = cst.FFT_FST(Nx, Nz, psi)

    u   =  cst.iFFT_FCT_Z(Nx, Nz, psi_hat, kk_cosine)
    w   = -cst.iFFT_FST_X(Nx, Nz, psi_hat, kk)

    w_max = max(reshape(w.real,-1))
    u_max = max(reshape(u.real,-1))
    psi_max = max(reshape(psi.real, -1))

    vel_max = max(u_max, w_max)
    
    visc = nu
    
    if scheme == 'rk4':
        C_visc = 2.74 # Conservative for the real axis (dissipation)
        C_osc = 2.80 # Also conservative
    elif scheme == 'ab1':
        C_visc = 1.0
        C_osc = 1.0 # Unconditionally unstable for oscillations! There's a 1 here just to have a value.
    elif scheme == 'ab3':
        C_visc = .53
        C_osc = .71
    else:
        print "No time stepping scheme specified of type", scheme
        sys.exit("TimeControl exiting.")

    max_kk = max(np.abs(kk))
    max_kkcosine = max(np.abs(kk_cosine))

    #
    # Compute the relevant quantites for the time step due to oscillations
    #

    #
    # Here we have to do some work if the solution is growing or decaying. As of 25 Sep 2017
    # I'm only doing the case for the linear waves
    # We check if buoy_coeff is 0, + or - 1 and assign it a diffusion or oscillation characteristic. If it is blowing up
    # We'll need to check if it is being balanced by diffusion.
    #
    
    buoy_coeff = ct*bt-cs*bs
    BuoyOsc = 0.  # Initialize
    BuoyVisc = 0. # Initialize
    
    if buoy_coeff == 0:
        # No contribution from buoyancy, only use viscosity
        BuoyVisc = buoy_coeff
        BuoyOsc = 0
    elif buoy_coeff > 0:
        # In this case there is growth and decay, and in any case the only hope is if the growth is overrun by viscosity
        BuoyVisc = buoy_coeff
        BuoyOsc = 0.
    elif buoy_coeff < 0:
        # In this case, oscillations are supported so use
        BuoyVisc = 0.
        BuoyOsc = buoy_coeff

    dispersion_buoyancy = np.sqrt(max_kk**2*g/(max_kk**2+max_kkcosine**2)*np.abs(BuoyOsc))

    #print "buoy ", max_kkcosine, max_kk, g, ct*bt, cs*bs, ct*bt-cs*bs
    
    osc_max = max(dispersion_buoyancy,vel_max,psi_max)*max(max_kk, max_kkcosine)

    #print "Time Step Control (which one is larger) : Is it buoyancy? ", dispersion_buoyancy, "or max vel?", vel_max

    # Find the minimum decay rate
    abs_decay_Psi = np.abs(visc)
    abs_decay_T   = np.abs(kappat)
    abs_decay_S   = np.abs(kappas)
    
    if hyper_Psi == 2:
        Psi_visc_max = abs_decay_Psi*(max_kk**2 + max_kkcosine**2)  # For 2nd derivative damping  
    elif hyper_Psi == 4:
        Psi_visc_max = abs_decay_Psi*(max_kk**4 + max_kkcosine**4)
    elif hyper_Psi == 6:
        Psi_visc_max = abs_decay_Psi*(max_kk**6 + max_kkcosine**6)
    elif hyper_Psi == 8:
        Psi_visc_max = abs_decay_Psi*(max_kk**8 + max_kkcosine**8)
    else:
        print "TimeControl: No hyper viscosity hyper_Psi", hyper_Psi
        sys.exit("Time Control Exiting")

    if hyper_T == 2:
        T_visc_max = abs_decay_T*(max_kk**2 + max_kkcosine**2)  # For 2nd derivative damping  
    elif hyper_T == 4:
        T_visc_max = abs_decay_T*(max_kk**4 + max_kkcosine**4)
    elif hyper_T == 6:
        T_visc_max = abs_decay_T*(max_kk**6 + max_kkcosine**6)
    elif hyper_T == 8:
        T_visc_max = abs_decay_T*(max_kk**8 + max_kkcosine**8)
    else:
        print "TimeControl: No hyper viscosity hyper_T", hyper_T
        sys.exit("TimeControl Stopping")
       
    if hyper_S == 2:
        S_visc_max = abs_decay_S*(max_kk**2 + max_kkcosine**2)  # For 2nd derivative damping  
    elif hyper_S == 4:
        S_visc_max = abs_decay_S*(max_kk**4 + max_kkcosine**4)
    elif hyper_S == 6:
        S_visc_max = abs_decay_S*(max_kk**6 + max_kkcosine**6)
    elif hyper_S == 8:
        S_visc_max = abs_decay_S*(max_kk**8 + max_kkcosine**8)
    else:
        print "TimeControl: No hyper viscosity hyper_S", hyper_S
        sys.exit("TimeControl Stopping.")
        

    visc_max = max(Psi_visc_max, T_visc_max, S_visc_max)

    #print "viscMax = ", Psi_visc_max, T_visc_max, S_visc_max
    #print "viscMax2 = ", abs_decay_Psi, abs_decay_T, abs_decay_S
    
    # Check to see if either oscillations or decay are turned off. If so, don't test for them.
    #
    # For example, if it is only decay and you are testing for the osc CFL, the dt will decrease
    # with the decreasing amplitude! And you will never finish the calculation!
    #
    if visc_max == 0:
        dt_visc = 0.
    else:
        dt_visc = C_visc/visc_max

    if osc_max == 0:
        dt_osc = 0.
    else:
        dt_osc = C_osc/osc_max
    
    if np.abs(osc_max) == 0.:
        #  This is a decay problem only
        dt_max = dt_visc
    elif np.abs(visc) == 0.:
        # This is an oscillation problem only
       dt_max = dt_osc
    else:
        # This has both components of the problem
        dt_max = min(dt_osc, dt_visc) # This is for the 2/3 rule in dealiasing
    
    print "dt = ",dt_max, "dt_osc", dt_osc, "dt_decay ", dt_visc

    if dt_max < 1.e-6:
        print "Warning, dt max is too small for the calculation. Quitting.", dt_max
        sys.exit("TimeControl Stopping")
        
    #print "dt = ",dt_max, "dt_osc", dt_osc, "dt_decay ", dt_visc
    return dt_max

