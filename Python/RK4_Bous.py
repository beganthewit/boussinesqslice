# This program solves the 2D Boussinesq equation using the fourier
# method with particular symetries. 

import numpy as np
from numpy import *
from scipy import *
from numpy import fft
from scipy import fftpack
import matplotlib.pyplot as plt
import InitialConditions as ics
import CosineSineTransforms as cst
import TimeControlModule as ttm
import sys 
import pdb #to pause use pdb.set_trace()
import os


def rk4(t, dt, u, params, f_of_u):
    k1 = dt * f_of_u(t, params, u)
    k2 = dt * f_of_u(t + 0.5*dt, params, u + 0.5*k1)
    k3 = dt * f_of_u(t + 0.5*dt, params, u + 0.5*k2)
    k4 = dt * f_of_u(t + dt, params, u + k3)
    return t + dt, u + (k1 + 2.0*(k2 + k3) + k4)/6.0

def rhsBous(t, params, state):

    global LinearTerms, hyper_Psi, hyper_T, hyper_S, ForceExternal

    # Unpack the parameters    
    g, rho0, ct, cs, bt, bs, nu, kappat, kappas = params

    # Unpack the unknowns
    Psi  = state[0]
    TT   = state[1]
    SS   = state[2]
    
    # Take all the Fourier Transforms

    Psi_hat        = DeAlias * cst.FFT_FST(Nx, Nz, Psi)
    Psi_x          = cst.iFFT_FST_X( Nx,Nz,Psi_hat,kk)  
    Psi_z          = cst.iFFT_FCT_Z( Nx,Nz,Psi_hat,kk_cosine)
    Psi_xx         = cst.iFFT_FST_XX(Nx,Nz,Psi_hat,kk)
    Psi_zz         = cst.iFFT_FST_ZZ(Nx,Nz,Psi_hat,kk_cosine)
    Psi_nabla2_hat = cst. FFT_FST(   Nx,Nz,Psi_xx+Psi_zz)
    Psi_nabla2_x   = cst.iFFT_FST_X( Nx,Nz,Psi_nabla2_hat,kk)
    Psi_nabla2_z   = cst.iFFT_FCT_Z( Nx,Nz,Psi_nabla2_hat,kk_cosine)
    
    if hyper_Psi == 2:
        Psi_nabla_xx   = cst.iFFT_FST_XX (Nx,Nz,Psi_nabla2_hat,kk)
        Psi_nabla_zz   = cst.iFFT_FST_ZZ (Nx,Nz,Psi_nabla2_hat,kk_cosine)
        Psi_nablaDiss  = Psi_nabla_xx + Psi_nabla_zz
    elif hyper_Psi == 4:
        Psi_xxxx = cst.iFFT_FST_XXXX(Nx,Nz,Psi_hat,kk)
        Psi_zzzz = cst.iFFT_FST_ZZZZ(Nx,Nz,Psi_hat,kk_cosine)
        Psi_nabla4_hat = cst. FFT_FST( Nx,Nz,Psi_xxxx+Psi_zzzz)
        Psi_nabla_xxxx   = cst.iFFT_FST_XX (Nx,Nz,Psi_nabla4_hat,kk)
        Psi_nabla_zzzz   = cst.iFFT_FST_ZZ (Nx,Nz,Psi_nabla4_hat,kk_cosine)
        Psi_nablaDiss  = Psi_nabla_xxxx + Psi_nabla_zzzz
    elif hyper_Psi == 6:
        Psi_xx6 = cst.iFFT_FST_XX6(Nx,Nz,Psi_hat,kk)
        Psi_zz6 = cst.iFFT_FST_ZZ6(Nx,Nz,Psi_hat,kk_cosine)
        Psi_nabla6_hat = cst. FFT_FST( Nx,Nz,Psi_xx6+Psi_zz6)
        Psi_nabla_xx6   = cst.iFFT_FST_XX (Nx,Nz,Psi_nabla6_hat,kk)
        Psi_nabla_zz6   = cst.iFFT_FST_ZZ (Nx,Nz,Psi_nabla6_hat,kk_cosine)
        Psi_nablaDiss  = Psi_nabla_xx6 + Psi_nabla_zz6
    elif hyper_Psi == 8:
        Psi_xx8 = cst.iFFT_FST_XX8(Nx,Nz,Psi_hat,kk)
        Psi_zz8 = cst.iFFT_FST_ZZ8(Nx,Nz,Psi_hat,kk_cosine)
        Psi_nabla8_hat = cst. FFT_FST( Nx,Nz,Psi_xx8+Psi_zz8)
        Psi_nabla_xx8   = cst.iFFT_FST_XX (Nx,Nz,Psi_nabla8_hat,kk)
        Psi_nabla_zz8   = cst.iFFT_FST_ZZ (Nx,Nz,Psi_nabla8_hat,kk_cosine)
        Psi_nablaDiss  = Psi_nabla_xx8 + Psi_nabla_zz8
    else:
        print ("rhsBous: No hyper viscosity for hyper_Psi of type = ", hyper_Psi)
        sys.exit("rhsBous Stopping")

                
    TT_hat         = DeAlias * cst.FFT_FST(Nx,Nz,TT)
    TT_x           = cst.iFFT_FST_X(Nx,Nz,TT_hat,kk)
    TT_z           = cst.iFFT_FCT_Z(Nx,Nz,TT_hat,kk_cosine)
    if hyper_T == 2:
        TT_DissXX          = cst.iFFT_FST_XX(Nx,Nz,TT_hat,kk)
        TT_DissZZ          = cst.iFFT_FST_ZZ(Nx,Nz,TT_hat,kk_cosine)
    elif hyper_T == 4:
        TT_DissXX          = cst.iFFT_FST_XXXX(Nx,Nz,TT_hat,kk)
        TT_DissZZ          = cst.iFFT_FST_ZZZZ(Nx,Nz,TT_hat,kk_cosine)
    elif hyper_T == 6:
        TT_DissXX          = cst.iFFT_FST_XX6(Nx,Nz,TT_hat,kk)
        TT_DissZZ          = cst.iFFT_FST_ZZ6(Nx,Nz,TT_hat,kk_cosine)
    elif hyper_T == 8:
        TT_DissXX          = cst.iFFT_FST_XX8(Nx,Nz,TT_hat,kk)
        TT_DissZZ          = cst.iFFT_FST_ZZ8(Nx,Nz,TT_hat,kk_cosine)
    
    SS_hat         = DeAlias * cst.FFT_FST(Nx,Nz,SS)
    SS_x           = cst.iFFT_FST_X(Nx,Nz,SS_hat,kk)
    SS_z           = cst.iFFT_FCT_Z(Nx,Nz,SS_hat,kk_cosine)
    if hyper_S == 2:
        SS_DissXX          = cst.iFFT_FST_XX(Nx,Nz,SS_hat,kk)
        SS_DissZZ          = cst.iFFT_FST_ZZ(Nx,Nz,SS_hat,kk_cosine)
    elif hyper_S == 4:
        SS_DissXX          = cst.iFFT_FST_XXXX(Nx,Nz,SS_hat,kk)
        SS_DissZZ          = cst.iFFT_FST_ZZZZ(Nx,Nz,SS_hat,kk_cosine)
    elif hyper_S == 6:
        SS_DissXX          = cst.iFFT_FST_XX6(Nx,Nz,SS_hat,kk)
        SS_DissZZ          = cst.iFFT_FST_ZZ6(Nx,Nz,SS_hat,kk_cosine)
    elif hyper_S == 8:
        SS_DissXX          = cst.iFFT_FST_XX8(Nx,Nz,SS_hat,kk)
        SS_DissZZ          = cst.iFFT_FST_ZZ8(Nx,Nz,SS_hat,kk_cosine)
    
#   Linear rhs
    rhsPsi = g*(-ct*TT_x + cs*SS_x) + nu*Psi_nablaDiss
    rhsT   = - bt*Psi_x + kappat*(TT_DissXX+TT_DissZZ)
    rhsS   = - bs*Psi_x + kappas*(SS_DissXX+SS_DissZZ)

    if LinearTerms != 'True':
    #   Nonlinear rhs
        rhsT   += - Psi_z*TT_x + Psi_x*TT_z
        rhsS   += - Psi_z*SS_x + Psi_x*SS_z
        rhsPsi += Psi_x*Psi_nabla2_z - Psi_z*Psi_nabla2_x

       
    if ForceExternal == 'True':
        #Add an exeternal forcing
        #ParkRun = 14
        ParkRun = 18

        if ParkRun == 18:
            rho0 = 1090.95075
            drho0_dz = -425.9
        if ParkRun == 14:
            rho0 = 896.2416
            drho0_dz = -31.976

        drho0_dz13 = -122.09
        dgamma = 100./3
        dz_b = 2./100

        a0 = 100.
        z_a = Lz/2
        rhoprime13 = dgamma*z_a + a0*dz_b + dgamma/2*dz_b
        rhoprime = rhoprime13 * drho0_dz/drho0_dz13
        Spert0 = rhoprime * 1./(rho0*cs)
        A_f = Spert0/2
        #A_f = Spert0/10

        #n_int = 33
        n_int = 10
        #k1 = 2*np.pi/dz_b
        #k1 = 2*np.pi*n_int/(Lx+Lz)
        k1 = 2*np.pi*n_int/Lx
        k1 = k1/10.
        m1 = k1
        
        #kmag2 = k1**2 + m1**2
        #N2 = -g*(ct*bt-cs*bs)
        #omega = sqrt(k1**2/kmag2*N2) 
        #pdb.set_trace()
        omega = Lx*2*np.pi

        z_array = np.zeros((Nx,Nz)) + z_grid
        x_array = np.zeros((Nx,Nz)) + x_grid[:,np.newaxis]

        #rhsPsi += (m1**2/k1+k1)*A_f*np.sin(k1*x_array+m1*z_array-omega*t)
        rhsPsi += k1*A_f*np.sin(k1*x_array-omega*t)


    rhsPsi_hat  = cst.FFT_FST(Nx, Nz, rhsPsi)  # FFT the rhs
    rhsPsi      = cst.iFFT_FST_Helm(Nx, Nz, rhsPsi_hat, kk, kk_cosine) # Solve the helmholtz
    return np.array([rhsPsi, rhsT, rhsS])




# Set up domain and grid
# Set two lengths for width and height
Lx = 0.2
Lz = 0.45
#Set up grid size
#Nx = 20
#Nz = 45
#Nx = 40
#Nz = 90
Nx = 80
Nz = 180

dx = float(Lx)/Nx 
dz = float(Lz)/(Nz-1)

x_grid = np.arange(Nx)*dx
z_grid = np.arange(Nz)*dz
#pdb.set_trace()


#
# The wave number array (frequencies)
#
kk = np.fft.fftfreq(Nx,Lx/Nx)*2.*np.pi
#
# And similarly for the cosine series
#
kk_cosine = np.arange((Nz))*np.pi/Lz
#


# The following is to dealias a 2D array. It is a
# Logical expression that, when multiplied, selects part of the elements
# to keep, and sets others to zero. To use dealias on spectral coefficients
# F_hat use:
# F_hat_dealiased = DeAlias*F_hat
#
kkx,kkz=np.meshgrid(kk,kk_cosine, indexing='ij')

DeAlias = np.logical_and(
    np.less(np.abs(kkx*Lx/(2.*np.pi)),int(Nx/3)),
    np.less(np.abs(kkz*Lz/(np.pi)),int(2*Nz/3)))
#DeAlias=1.0


# Here's for the problem set up
#ProblemType = 'SimpleWaves'
ProblemType = 'Layers'
#ParkRun = -1
#ParkRun = 14
ParkRun = 18
#N2 = 0.35
#N2 = 1.19
N2 = 3.83
DiffusionFactor = 500
#ProblemType = 'KelvinHelmholtz'
nvars = 2
RandomPert = 1
PertFactor = 5

if ProblemType == 'SimpleWaves':
    TT, SS, Psi, params = ics.setProblem_SimpleWave(x_grid, z_grid, Nx, Nz, Lx, Lz)
elif ProblemType == 'Layers':
    TT, SS, Psi, params = ics.setProblem_Layers(x_grid, z_grid, Nx, Nz, Lx, Lz, ParkRun, DiffusionFactor, nvars, RandomPert, PertFactor, N2=N2)
elif ProblemType == 'KelvinHelmholtz':
    TT, SS, Psi, params = ics.setProblem_KelvinHelmholtz(x_grid, z_grid, Nx, Nz, Lx, Lz, ParkRun, MolecularDiffusion)
else:
    print ("There is no initial conditon type = ", InitialConditionType)
    sys.exit("RK4Bous Exiting")


# Say which type of viscosity here. Allowed values are 2, 4, 6, and 8.
# Do not forget that the visciosity has to be negative for 4 and 8.
# Also do not forget that by the time you get to 8 the order of magnitude for the viscosity is
# like 1.e-13 or even smaller.
#
hyper_Psi = 2  # Default
#hyper_Psi = 4
#params[6] = -.035e-9
#hyper_Psi = 6
#params[6] = 3.5e-6
#hyper_Psi = 8
#params[6] = -8.e-23

hyper_T = 2
#hyper_T = 4
#params[7] = -.0035
#hyper_T = 6
#params[7] = 2.5e-6
#hyper_T = 8
#params[7] = -4.e-23

hyper_S = 2
#params[8] = .1
#params[8] = .01
#hyper_S = 4
#params[8] = -3.5e-6
#hyper_S = 6
#params[8] = 2.5e-22
#hyper_S = 8
#params[8] = -4.e-21

HyperTypes = np.array([hyper_Psi, hyper_T, hyper_S])

#
# Saving data for later
#

Psi_initial = np.copy(Psi)
TT_initial = np.copy(TT)
SS_initial = np.copy(SS)

Psi0_hat = DeAlias*cst.FFT_FST(Nx, Nz, Psi_initial) 
TT0_hat  = DeAlias*cst.FFT_FST(Nx, Nz, TT_initial) 
SS0_hat  = DeAlias*cst.FFT_FST(Nx, Nz, SS_initial) 

#Look at spectral representation of ICs:
#Psi0_hat = DeAlias * cst.FFT_FST(Nx, Nz, Psi_initial)
#Psi_initial2 = cst.iFFT_FST(Nx, Nz, Psi0_hat)
#plt.plot(Psi_initial2[0,:],'r')
#plt.plot(Psi[0,:],'b')
#plt.show()
#pdb.set_trace()

#
#
# Here we can trial different unit tests
# -- Start out with using the fully nonlinear solution
# -- The unit tests will set the terms to linear
# -- make a selection for interactive plots
#

InteractivePlot = 'False'
LinearTerms = 'False'   
UnitTestLinear = 'None'   	# If you are not doing a unit test this needs to be set to None
#UnitTestLinear = 'Decay'  	# All coefficents to zero except for damping
#UnitTestLinear = 'SS=0'   	# SS starts out as zero and remains so, leaving a 2x2 system
#UnitTestLinear = 'TT=0'   	# TT starts out as zero and remains so, leaving a 2x2 system
ForceExternal = 'False'
PlotICs = 'True'


if UnitTestLinear == 'Decay':
    #
    # For the decay case you need to set g = 0 = bt = ct = bt = bs = 0
    # and make sure the rhs is linear
    #
    
    LinearTerms = 'True'
    params[0] = 0. # This sets g = 0
    params[2] = 0. # This sets ct = 0
    params[3] = 0. # This sets cs = 0
    params[4] = 0. # This sets bt = 0
    params[5] = 0. # This sets bs = 0

elif UnitTestLinear == 'SS=0':
    #
    # For this case you need to reset SS = 0
    # and nu = ks = kt = 0
    # and make sure the rhs is linear
    # also ct and cs = 0

    LinearTerms = 'True'
    
    SS  = np.zeros((Nx,Nz))*1j    

    params[3] = 0. # This is for cs = 0
    params[5] = 0. # This is for bs = 0
    params[6] = 0. # This sets nu = 0
    params[7] = 0. # This sets kappat = 0
    params[8] = 0. # This sets kappas = 0

elif UnitTestLinear == 'TT=0':
    #
    # For this case you need to reset TT = 0
    # and nu = ks = kt = 0
    # and make sure the rhs is linear
    #

    LinearTerms = 'True'
    TT  = np.zeros((Nx,Nz))*1j    
    
    params[2] = 0. # This is for ct = 0
    params[4] = 0. # This is for bt = 0
    params[6] = 0. # This sets nu = 0
    params[7] = 0. # This sets kappat = 0
    params[8] = 0. # This sets kappas = 0

else:
    LinearTerms = 'False'
    

if PlotICs == 'True':
    # Make a plot of the initial conitions 

    fig_Initial = plt.figure( 1 )

    plt.subplot(1, 3, 1, aspect = 'auto')
    p1, = plt.plot(TT[6,:].real,z_grid)
    p2, = plt.plot(SS[6,:].real,z_grid)
    p3, = plt.plot(Psi[6,:].real,z_grid)
    plt.legend([p1, p2, p3], ["TT", "SS", "Psi"])
    #plt.ylim(0,1.)
    #plt.xlim(-1.1,1.1)
    plt.title('T, S and Psi profiles in z')
    plt.ylabel('z')

    plt.subplot(1, 3, 2, aspect = 'auto')
    p1, = plt.plot(TT[:,6].real,x_grid)
    p2, = plt.plot(SS[:,6].real,x_grid)
    p3, = plt.plot(Psi[:,6].real,x_grid)
    plt.legend([p1, p2, p3], ["TT", "SS", "Psi"])
    #plt.ylim(0,1.)
    #plt.xlim(-1.1,1.1)
    plt.title('T, S and Psi profiles in x')
    plt.ylabel('x')

    plt.subplot(1, 3, 3, aspect = 'equal')
    plt.contourf(x_grid ,z_grid, Psi.real.transpose())
    plt.title('Contour plot of streamfunction')
    plt.xlabel('x')
    plt.ylabel('z')

    #plt.show( 1 )
    plt.savefig('Initial.png')


# Create the state variable
state = np.array([Psi, TT, SS])


# Time step.

#
# Initial time and Initial condition
#

t = 0
#
# For time stepping
#
dt = 1.0/600.0

#print "first dt = ", dt

dt = ttm.TimeControl(t, params, state, Nx, Nz, x_grid, z_grid, kk, kk_cosine, 'rk4', HyperTypes)

#print "second dt = ", dt
#raise

EndTime = 60*60
#EndTime = 60

#How many time slices:
#tfact = 1
tfact = 10
N_for_plot = EndTime * tfact + 1 

TimeInterval = EndTime/np.float_(N_for_plot-1)

print ("There will be ", N_for_plot, "snapshots of data saved, one every ", TimeInterval, "seconds.")

KE = np.zeros((N_for_plot))
PE = np.zeros((N_for_plot))
Etotal = np.zeros((N_for_plot))

time_array   = np.zeros((N_for_plot))
psi_plot_array   = np.zeros((N_for_plot))

#
# Set the initital values of the plot arrays
#

time_array[0] = 0.
psi_plot_array[0] = max(reshape(Psi_initial.real, -1))

#
# Print out the initial data
#
#print('%10f %10f %10f  %10f' % (t, max(reshape(Psi.real, -1)), max(reshape(TT.real, -1)), max(reshape(SS.real, -1))))

if InteractivePlot == 'True':

    plt.figure()            
    plt.ion() 


i_iterations = 0    # This is an interation counter
i_plots      = 0    # This is a plot counter, begin at one because the first array element is zero

#For writing results out to text files 
w2f_state = 'True' 
SpectralCoef = 'False'
w2f_spectral = 'False'


while t < EndTime - dt:

    #dt = ttm.TimeControl(t, params, state, Nx, Nz, x_grid, z_grid, kk, kk_cosine, 'rk4', HyperTypes)
    #t,state = rk4(t, dt, state, params, rhsBous)
    Psi = state[0]
    TT  = state[1]
    SS  = state[2]
 
    if UnitTestLinear == 'Decay':
        Psi_exact = ics.f_simple_exact_decay(t, x_grid, z_grid, Nx, Nz, Lx, Lz, kk, kk_cosine, Psi0_hat, params, 'Psi', HyperTypes)
        TT_exact = ics.f_simple_exact_decay(t, x_grid, z_grid, Nx, Nz, Lx, Lz, kk, kk_cosine, TT0_hat, params, 'TT', HyperTypes)
        SS_exact = ics.f_simple_exact_decay(t, x_grid, z_grid, Nx, Nz, Lx, Lz, kk, kk_cosine, SS0_hat, params, 'SS', HyperTypes)
    elif UnitTestLinear == 'SS=0':
        Psi_exact, TT_exact, SS_exact = \
                 ics.S0_simple_exact(t, x_grid, z_grid, Nx, Nz, Lx, Lz, kk, kk_cosine, Psi0_hat, TT0_hat, SS0_hat, params)
    elif UnitTestLinear == 'TT=0':
        Psi_exact, TT_exact, SS_exact = \
                 ics.T0_simple_exact(t, x_grid, z_grid, Nx, Nz, Lx, Lz, kk, kk_cosine, Psi0_hat, TT0_hat, SS0_hat, params)
    
    else:
        Psi_exact = np.zeros((Nx, Nz))
        TT_exact = np.zeros((Nx, Nz))
        SS_exact = np.zeros((Nx, Nz))
            
    if np.abs(t-i_plots*TimeInterval) < dt:

        #print "np.abs(t-i_plots*TimeInterval): ", np.abs(t-i_plots*TimeInterval) 

        time_array[i_plots] = t
        psi_plot_array[i_plots] = max(reshape(Psi.real, -1))
        
        
        if InteractivePlot == 'True':

            plt.subplot(3, 1, 1, aspect = 'auto')

            if t == 0:
                nlevels_S = 20
                range_S = 40
                dS = range_S/(nlevels_S-1)
                min_S = -20
                clevels_S = np.arange(nlevels_S)*dS + min_S 

            cnt = plt.contourf(x_grid ,z_grid, SS.real.transpose(), clevels_S, cmap=plt.get_cmap('RdBu'))
            if t == 0: plt.colorbar(cnt)
            plt.title('SS')
            plt.xlabel('x')
            plt.ylabel('z')

            plt.subplot(3, 1, 2, aspect = 'auto')

            if UnitTestLinear != 'None':

                #
                # In this case plot the exact solutions
                #
            
                cnt = plt.contourf(x_grid ,z_grid, Psi_exact.real.transpose(), cmap=plt.get_cmap('RdBu'))
                #plt.colorbar(cnt)
                plt.title('Psi Exact')
                plt.xlabel('x')
                plt.ylabel('z')

                plt.subplot(3, 1, 3, aspect = 'auto')
                cnt = plt.contourf(x_grid ,z_grid, (Psi-Psi_exact).real.transpose(), cmap=plt.get_cmap('RdBu'))
                #plt.colorbar(cnt)
                plt.title('Error between the two')
                plt.xlabel('x')
                plt.ylabel('z')

            else:
               
                #
                # For a non unit test, plot u and w.
                #

                #
                # Compute u and w
                #

                Psi_hat =  cst.FFT_FST(Nx, Nz, Psi)
                w       =  -cst.iFFT_FST_X( Nx,Nz,Psi_hat,kk)  
                u       =  cst.iFFT_FCT_Z( Nx,Nz,Psi_hat,kk_cosine)
                
                if t == 0:
                    nlevels_w = 20
                    range_w = 1.0
                    dw = range_w/(nlevels_w-1)
                    min_w = -0.5
                    clevels_w = np.arange(nlevels_w)*dw + min_w 

                cnt = plt.contourf(x_grid ,z_grid,w.real.transpose(), clevels_w, cmap=plt.get_cmap('RdBu'))
                if t == 0: plt.colorbar(cnt)
                plt.title('w')
                plt.xlabel('x')
                plt.ylabel('z')

                if t == 0:
                    nlevels_u = 20
                    range_u = 1.0
                    du = range_u/(nlevels_u-1)
                    min_u = -0.5
                    clevels_u = np.arange(nlevels_u)*du + min_u 

                plt.subplot(3, 1, 3, aspect = 'auto')
                cnt = plt.contourf(x_grid ,z_grid, u.real.transpose(), clevels_u, cmap=plt.get_cmap('RdBu'))
                if t == 0: plt.colorbar(cnt)
                plt.title('u')
                plt.xlabel('x')
                plt.ylabel('z')

                
            plt.show()
            plt.show(block=False)
            plt.pause(0.0001)

        psi_plot_array[i_plots] = max(real(Psi.ravel()))

        # For writing state to a file:
        if w2f_state == 'True':
        
            if i_plots == 0: 
                #Write out 2D-slice of state at each time point: 
                dir_state = './Results/' + ProblemType + '/State/'
            
                #Create directory if it doesn't exist:
                if not os.path.exists(dir_state):
                    os.makedirs(dir_state)

            fnm_psi = dir_state + 'Psi_' + str(i_plots) + '.txt'
            fnm_TT = dir_state + 'TT_' + str(i_plots) + '.txt'
            fnm_SS = dir_state + 'SS_' + str(i_plots) + '.txt'
            np.savetxt(fnm_psi,state[0])
            np.savetxt(fnm_TT,state[1])
            np.savetxt(fnm_SS,state[2])

        #Find spectral coefficients.
        #This can be independent of natural-basis analysis below.
        if SpectralCoef == 'True':
            Psi_hat = cst.FFT_FST(Nx,Nz,state[0])
            TT_hat = cst.FFT_FST(Nx,Nz,state[1])
            SS_hat = cst.FFT_FST(Nx,Nz,state[2])

            # For writing spectral coefficients to a file:
            if w2f_spectral == 'True':
                if i_plots == 0:
                    #pdb.set_trace()
                    dir_spectral = './Results/' + ProblemType + '/SpectralCoef/'

                    #Create directory if it doesn't exist:
                    if not os.path.exists(dir_spectral):
                        os.makedirs(dir_spectral)

                fnm_psi_hat = dir_spectral + 'Psi_hat_' + str(i_plots) + '.txt'
                fnm_TT_hat = dir_spectral + 'TT_hat_' + str(i_plots) + '.txt'
                fnm_SS_hat = dir_spectral + 'SS_hat_' + str(i_plots) + '.txt'
                np.savetxt(fnm_psi_hat,Psi_hat.view(float))
                np.savetxt(fnm_TT_hat,TT_hat.view(float))
                np.savetxt(fnm_SS_hat,SS_hat.view(float))

        i_plots +=1
    

    dt = ttm.TimeControl(t, params, state, Nx, Nz, x_grid, z_grid, kk, kk_cosine, 'rk4', HyperTypes)
    t,state = rk4(t, dt, state, params, rhsBous)

    i_iterations += 1

    #
    # For the output
    #

    
    if UnitTestLinear == 'None':
        print('%10f %10f %10f %10f %10f' % (t, dt, max(reshape(Psi.real, -1)),\
                                                   max(reshape(TT.real,  -1)),\
                                                   max(reshape(SS.real,  -1))))
    else:
        print('%10f %10f  %10f %10f %10f %10f  %10f %10f' % (t, dt, max(reshape(Psi.real, -1)),  max(reshape(Psi_exact.real, -1)), \
                                                      max(reshape(TT.real,  -1)),  max(reshape(TT_exact.real,  -1)),\
                                                      max(reshape(SS.real,  -1)),  max(reshape(SS_exact.real,  -1))))

#if w2f_state == 'True': f_state.close()
#pdb.set_trace()

#
#    Test decay problems
#

if UnitTestLinear == 'Decay':

    Psi_exact = ics.f_simple_exact_decay(time_array[i_plots-1], x_grid, z_grid, Nx, Nz, Lx, Lz, kk, kk_cosine, Psi0_hat, params, 'Psi', HyperTypes)
    TT_exact = ics.f_simple_exact_decay(time_array[i_plots-1], x_grid, z_grid, Nx, Nz, Lx, Lz, kk, kk_cosine, TT0_hat, params, 'TT', HyperTypes)
    SS_exact = ics.f_simple_exact_decay(time_array[i_plots-1], x_grid, z_grid, Nx, Nz, Lx, Lz, kk, kk_cosine, SS0_hat, params, 'SS', HyperTypes)
    Psi_decay_error = Psi - Psi_exact
    TT_decay_error  = TT -  TT_exact
    SS_decay_error  = SS -  SS_exact

    print ("The following difference between the computed solution and the exact solution should be close to zero.")
    print ("Testing Psi Decay Error ", max(reshape(Psi_decay_error, -1)))
    print ("Testing TT Decay  Error", max(reshape(TT_decay_error, -1)))
    print ("Testing SS Decay  Error", max(reshape(SS_decay_error, -1)))
    
elif UnitTestLinear == 'SS=0':

    Psi_exact, TT_exact, SS_exact = \
        ics.S0_simple_exact(t, x_grid, z_grid, Nx, Nz, Lx, Lz, kk, kk_cosine, Psi0_hat, TT0_hat, SS0_hat, params)
    Psi_S0_error = Psi.real - Psi_exact.real
    TT_S0_error  = TT.real  - TT_exact.real
    SS_S0_error  = SS.real  - SS_exact.real  # This one should remain zero

    #print "Psi_exact_coefficients", cst.FFT_FST(Nx, Nz, Psi_exact.real)[4,:].real
    #print "Psi_coefficients", cst.FFT_FST(Nx, Nz, Psi.real)[4,:].real

    fig_C = plt.figure(10)
    plt.plot(Psi.real[4,:], 'ro')
    plt.plot(Psi_exact[4,:].real, 'go')
    plt.title('spectral coefficients')
    plt.ylabel('value')
    plt.xlabel('number')
    plt.show(10)
    
    print ("The following difference between the computed solution and the exact solution should be close to zero.")
    print ("Testing Psi for S=0 no decay ", max(reshape(Psi_S0_error, -1)))
    print ("Testing TT S=0 no decay ", max(reshape(TT_S0_error, -1)))
    print ("Testing SS S=0 ", max(reshape(SS_S0_error, -1)))


elif UnitTestLinear == 'TT=0':

    Psi_exact, TT_exact, SS_exact = \
        ics.T0_simple_exact(t, x_grid, z_grid, Nx, Nz, Lx, Lz, kk, kk_cosine, Psi0_hat, TT0_hat, SS0_hat, params)
    Psi_T0_error = Psi.real - Psi_exact.real
    TT_T0_error  = TT.real - TT_exact.real # This one should remain zero
    SS_T0_error  = SS.real - SS_exact.real  
    
    print ("The following difference between the computed solution and the exact solution should be close to zero.")
    print ("Testing Psi for T=0 no decay ", max(reshape(Psi_T0_error, -1)))
    print ("Testing TT T=0 no decay ", max(reshape(TT_T0_error, -1)))
    print ("Testing SS T=0 no decay ", max(reshape(SS_T0_error, -1)))
    
if UnitTestLinear == 'None':

    # Use initial conditions to compare solution progress
    
    Psi_exact = np.copy(Psi_initial)
    TT_exact = np.copy(TT_initial)
    SS_exact = np.copy(SS_initial)


fig_Final = plt.figure( 1 )

plt.subplot(3, 3, 1, aspect = 'auto')
p1, = plt.plot(SS_exact[5,:],z_grid)
p2, = plt.plot(SS[5,:].real,z_grid)
plt.legend([p1, p2], ["SSexacct", "SS"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('T0 and Tf')
plt.ylabel('z')

plt.subplot(3, 3, 2, aspect = 'auto')
p1, = plt.plot(Psi_exact[5,:],z_grid)
p2, = plt.plot(Psi[5,:].real,z_grid)
plt.legend([p1, p2], ["Psiexact", "Psi"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('S0 and Sf')
plt.ylabel('z')

plt.subplot(3, 3, 3, aspect = 'auto')
p1, = plt.plot(TT[1,:].real-TT_exact[1,:],z_grid)
p2, = plt.plot(SS[1,:].real-SS_exact[1,:],z_grid)
plt.legend([p1, p2], ["TTexact-TT", "SSexact-SS"])
#plt.ylim(0,1.)
#plt.xlim(-1.1,1.1)
plt.title('T0-Tf and S0-Sf')
plt.ylabel('z')

#plt.subplot(2, 3, 4, aspect = 'equal')
plt.subplot(3, 3, 4)

cnt = plt.contourf(x_grid ,z_grid, SS.real.transpose(), cmap=plt.get_cmap('RdBu'))
plt.colorbar(cnt)
plt.title('SS')
plt.xlabel('x')
plt.ylabel('z')


#plt.subplot(2, 3, 5, aspect = 'equal')
plt.subplot(3, 3, 5)
cnt = plt.contourf(x_grid ,z_grid, TT.real.transpose(), cmap=plt.get_cmap('RdBu'))
plt.colorbar(cnt)
plt.title('TT')
plt.xlabel('x')
plt.ylabel('z')


#
# Compute u and w
#

Psi_hat =  cst.FFT_FST(Nx, Nz, Psi)
w       =  -cst.iFFT_FST_X( Nx,Nz,Psi_hat,kk)  
u       =  cst.iFFT_FCT_Z( Nx,Nz,Psi_hat,kk_cosine)



#plt.subplot(2, 3, 6, aspect = 'equal')
plt.subplot(3, 3, 6 )
cnt = plt.contourf(x_grid ,z_grid, u.real.transpose(), cmap=plt.get_cmap('RdBu'))
plt.colorbar(cnt)
plt.title('u')
plt.xlabel('x')
plt.ylabel('z')


#plt.subplot(2, 3, 4, aspect = 'equal')
plt.subplot(3, 3, 7)
cnt = plt.contourf(x_grid ,z_grid, w.real.transpose(), cmap=plt.get_cmap('RdBu'))
plt.colorbar(cnt)
plt.title('w')
plt.xlabel('x')
plt.ylabel('z')


#plt.subplot(2, 3, 5, aspect = 'equal')
plt.subplot(3, 3, 8)
cnt = plt.contourf(x_grid ,z_grid, Psi.real.transpose(), cmap=plt.get_cmap('RdBu'))
plt.colorbar(cnt)
plt.title('Psi')
plt.xlabel('x')
plt.ylabel('z')

#plt.subplot(2, 3, 6, aspect = 'equal')
plt.subplot(3, 3, 9 )
cnt = plt.contourf(x_grid ,z_grid, Psi.real.transpose()-Psi_exact.real.transpose(), cmap=plt.get_cmap('RdBu'))
plt.colorbar(cnt)
plt.title('Psi difference from initial')
plt.xlabel('x')
plt.ylabel('z')


plt.show( 1 )
