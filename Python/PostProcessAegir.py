#Code to post process dedalus results
  

#Load in required libraries:
import numpy as np
from numpy import *
from scipy import *
from numpy import fft
from scipy import fftpack
from scipy.signal import welch
import pdb #to pause execution use pdb.set_trace()
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import sys
import CosineSineTransforms as cst


#Passed variables:
if len(sys.argv) > 1:
    tIdx = int(sys.argv[1])


#Program control:
ProblemType = 'Layers'
#ProblemType = 'KelvinHelmholtz'
#ParkRun = 14
ParkRun = 18
#ParkRun = -1
#N2 = 0.1

#User must make sure correct data is read in for some analysis:
#var_nms = ['Psi']
#var_nms = ['S']
var_nms = ['Psi','S']
#var_nms = ['Psi','T','S']
nReadVars = len(var_nms)

#Choose which diagnostics to compute:
#n.b. code has been written to make each switch/variable 
#largely independent of the others. This makes it easier for the
#user and helped to make the code more object orientated/modular to 
#minimise repetition.
FullFields 		= 1
StatePsi 		= 0
StateS 			= 0
PE 			= 0
Wind 			= 0
dSdz 			= 0
dUdz 			= 0
N_local			= 1
Richardson 		= 0
SpectralAnalysisT 	= 0
TimeSeriesAnalysisPE 	= 0
FourierSinCosBasis 	= 0
Vorticity               = 0
KE           		= 0
PE         		= 0
TotalEnergy             = 0
SpectralAnalysisZ       = 0

NaturalBasis            = 0
nvars			= 2
BasisCheck1             = 0
BasisCheck2             = 0

#General statistical processing:
xMean = 0
tMean = 0
SlidingMean = 0

#Choose type of plot:
MakePlot = 1
PlotXZ = 1
PlotTZ = 0
PlotProfileZ = 0
MakeMovie = 0

#Write analysis to file
w2f_analysis = 1

#Options when reading data:
#dir_state = './Results/Layers/State/'
dir_state = './Results/Layers/State_18/'
#dir_state = './Results/Layers/State_0.1/'
#dir_state = './Results/Layers/State_0.01/'
ts = 0
#nfiles = 2
#nfiles = 95
nfiles = 30

#Model output/write timestep:
dt = .1
#dt = 1.

#Analysis timestep:
#(This is important for computations involving numerous large arrays - 
#it avoids exceeding system memory limits).
#Effectively we use a subset of the model output data for the analysis:
dt2 = dt
#dt2 = 1/5.
#dt2 = 1/2.
#dt2 = 1.
#dt2 = 2.

secPerFile = dt

#Choose sliding-window length for averaging.
#N.B. some data will be lost from start and end of time period.
#N.B. Choose an odd length to make window symmetric around some t point:
#Nt_mean = 1801
Nt_mean = 3001
wing = Nt_mean//2


#Set up grid and related objects:
Lx = 0.2
Lz = 0.45
factor = 1
#factor = 2
Nx = 80
Nz = 180
Nx = Nx*factor
Nz = Nz*factor

dx = float(Lx)/Nx
dz = float(Lz)/(Nz-1)

x = np.arange(Nx)*dx
z = np.arange(Nz)*dz

# The wave number array (frequencies)
kk = np.fft.fftfreq(Nx,Lx/Nx)*2.*np.pi
# And similarly for the cosine series
kk_cosine = np.arange((Nz))*np.pi/Lz

#Construct some general arrays for contour plots:
ntPerFile = secPerFile/dt
tq = dt2/dt
Nt = ntPerFile*nfiles/tq
Nt = int(Nt)

x2d = np.tile(x,(Nz,1))
z2d = np.tile(z,(Nx,1)).transpose()
t = np.arange(Nt)*dt2 + ts*secPerFile

t2d_z = np.tile(t,(Nz,1))
z2d_t = np.tile(z,(Nt,1)).transpose()


#Set physical constants and related objects:
g = 9.81
ct = 2*10**(-4.)
cs = 7.6*10**(-4.)

if ParkRun == 18:               #for lab run 18 of Park et al
    N2 = 3.83
    drho0_dz = -425.9		
    rho0 = -g/N2*drho0_dz
if ParkRun == 14:               #for lab run 14 of Park et al
    N2 = 0.35
    drho0_dz = -31.976
    rho0 = -g/N2*drho0_dz
if ParkRun < 1:
    N2_14 = 0.35
    drho0_dz_14 = -31.976
    rho0_14 = -g/N2_14*drho0_dz_14
        
    rho0 = rho0_14  		#assume not much difference which is reasonable
    drho0_dz =  -N2/g*rho0

if ProblemType == 'Layers':
    if nvars == 2:
        bt = 0.
        bs = -1./(rho0*cs)*drho0_dz
    if nvars == 3:
        bs = -1./(rho0*cs)*drho0_dz
        bt = -bs*0.001
if ProblemType == 'KelvinHelmholtz':
    bt = 0.
    bs = 0.

if FullFields == 1:
    drho0_dz13 = -122.09
    dgamma = 100./3
    dz_b = 2./100
    a0 = 100.
    z_a = Lz/2
    rhoprime13 = dgamma*z_a + a0*dz_b + dgamma/2*dz_b
    rhoprime = rhoprime13 * drho0_dz/drho0_dz13
    Spert0 = rhoprime * 1./(rho0*cs)

    TTop = 0.
    STop = Spert0
    Tbase = np.zeros(Nz) + TTop
    Sbase = -bs*(z-Lz) + STop


#Set general plotting parameters assuming A4 page size:
A4Width = 8.27
MarginWidth = 1
width = A4Width-2*MarginWidth
height = 4
#For scaling the A4 plot dimensions:
ScaleFactor = 1
#ScaleFactor = 0.7
width = width*ScaleFactor
height = height*ScaleFactor


#Read in Aegir results:

if 'Psi' in var_nms:
    Psi = np.zeros((Nt,Nx,Nz))
if 'T' in var_nms:
    T = np.zeros((Nt,Nx,Nz))
if 'S' in var_nms:
    S = np.zeros((Nt,Nx,Nz))

for jj in range(0,nReadVars):
    print('variable: ',var_nms[jj])
    for tt in range(ts,nfiles):

        print(tt)

        if var_nms[jj] == 'Psi':
            fnm_psi = dir_state + 'Psi_' + str(tt) + '.txt'
            Psi[tt,:,:] = np.loadtxt(fnm_psi)
        if var_nms[jj] == 'T':
            fnm_T = dir_state + 'TT_' + str(tt) + '.txt'
            T[tt,:,:] = np.loadtxt(fnm_T)
        if var_nms[jj] == 'S':
            fnm_S = dir_state + 'SS_' + str(tt) + '.txt'
            S[tt,:,:] = np.loadtxt(fnm_S)


#Define some useful functions:
def x_mean(f):
    data = np.mean(f,1)
    data = data.transpose()
    return data

def t_mean(f):
    #Average across full time period:
    data = np.mean(f,0)
    data = data.transpose()
    return data

def sliding_mean(f,Nt,Nx,Nz,wing):
    data = np.zeros((Nt,Nx,Nz))
    for tt in range(wing,Nt-wing):
        data[tt,:,:] = np.mean(f[tt-wing:tt+wing,:,:],0)
    return data

def d_dz(f,Nt,Nx,Nz,z):
    fz = np.zeros((Nt,Nx,Nz))
    for jj in range(1,Nz-1):
        dz = z[jj+1] - z[jj-1]
        for ii in range(1,Nx-1):
            df = f[:,ii,jj+1] - f[:,ii,jj-1]
            fz[:,ii,jj] = df/dz
    return fz

def d_dx(f,Nt,Nx,Nz,x):
    fx = np.zeros((Nt,Nx,Nz))
    for jj in range(1,Nz-1):
        for ii in range(1,Nx-1):
            dx = x[ii+1]-x[ii-1]
            df = f[:,ii+1,jj] - f[:,ii-1,jj]
            fx[:,ii,jj] = df/dx
    return fx


if FullFields == 1:
    #add base state:
    S += Sbase


if StatePsi == 1:
    data = Psi
    if MakePlot == 1:
        PlotTitle = ''
        FigNmBase = 'Psi'
        clevels = 50
        cmap = 'PRGn'


if StateS == 1:
    data = S
    if MakePlot == 1:
        PlotTitle = ''
        FigNmBase = 'S'
        clevels = 50
        cmap = 'PRGn'


if Wind == 1:

    #Compute Fourier-SinCos basis coefficients:
    Psi_hat = np.zeros( (Nt,Nx,Nz) )*1j

    for tt in range(0,Nt):
        Psi_hat[tt,:,:] = cst.FFT_FST(Nx,Nz,Psi[tt,:,:])
   
    #Compute components of wind.
    #This approach avoids truncation errors using finite differences: 
    data = cst.iFFT_FCT_Z( Nx,Nz,Psi_hat,kk_cosine)
    data2 = -cst.iFFT_FST_X(Nx,Nz,Psi_hat,kk)

    if MakePlot == 1:

        PlotTitle = 'u'
        PlotTitle2 = 'w'
        FigNmBase = 'Wind'
        cmap = 'bwr'

        nlevs = 81
        scaleU = 1.
        u_min = -2.0*scaleU
        u_max = 2.0*scaleU
        
        du = (u_max-u_min)/(nlevs-1)
        clevels = np.arange(nlevs)*du + u_min        

        nlevs = 101
        scaleW = 1./10000
        w_max = 1.*scaleW
        w_min = -1.*scaleW
        dw = (w_max-w_min)/(nlevs-1)
        clevels2 = np.arange(nlevs)*dw + w_min

        clevels = 50
        clevels2= 50


if Vorticity == 1:

    #Compute Fourier-SinCos basis coefficients:
    Psi_hat = np.zeros( (Nt,Nx,Nz) )*1j

    for tt in range(0,Nt):
        Psi_hat[tt,:,:] = cst.FFT_FST(Nx,Nz,Psi[tt,:,:])

    #Compute vorticity.
    #This approach avoids truncation errors using finite differences:
    Psi_xx = cst.iFFT_FST_XX(Nx,Nz,Psi_hat,kk)
    Psi_zz = cst.iFFT_FST_ZZ(Nx,Nz,Psi_hat,kk_cosine)
    data = -(Psi_xx + Psi_zz)


if KE == 1:
    #Compute Fourier-SinCos basis coefficients:
    Psi_hat = np.zeros( (Nt,Nx,Nz) )*1j

    for tt in range(0,Nt):
        Psi_hat[tt,:,:] = cst.FFT_FST(Nx,Nz,Psi[tt,:,:])

    #Compute components of wind.
    #This approach avoids truncation errors using finite differences: 
    u = cst.iFFT_FCT_Z( Nx,Nz,Psi_hat,kk_cosine)
    w = -cst.iFFT_FST_X(Nx,Nz,Psi_hat,kk)

    data = 1./2*( u**2 + v**2 )


if PE == 1:
    #N.B. we need to convert from perturbation S to full S.
    #Compute a mean full salinity using base state (assuming S'<<Sbase):
    S0 = mean(Sbase)
    #Compute density from salinity using equation of state for full fields:
    rho = rho0*(1 + cs*(S-S0))

    #Compute a new z array to reduce following loop
    #There are a large number of time points - we wish to avoid looping 
    #over the time axis:
    z_arr = np.tile(z,(Nt,1))

    #Compute potential energy per unit volume for each grid point:
    PE = np.zeros((Nt,Nx,Nz))
    for ii in range(0,Nx):
        PE[:,ii,:] = S[:,ii,:]*g*z_arr

    #Find total PE in each column at each time:
    PEcolumn = np.zeros((Nt,Nx))
    for ii in range(0,Nx):
        PEcolumn[:,ii] = sum(PE[:,ii,:],1)


if dSdz == 1:    
    data = d_dz(S,Nt,Nx,Nz,z)

    if MakePlot == 1:

        PlotTitle = r'$\partial S/\partial z$ (g/kg/m)'
        FigNmBase = 'dSdz'
        cmap = 'PRGn'

        nlevs = 41
        if ParkRun == 18: 
            SzMin = -1000
            SzMax = 1000
        if ParkRun == 14:
            SzMin = -100
            SzMax = 100
        if ParkRun < 1:
            SzMin = -25
            SzMax = 25

        dSz = (SzMax-SzMin)/(nlevs-1)
        clevels = np.arange(nlevs)*dSz + SzMin


if dUdz == 1:

    u = d_dz(Psi,Nt,Nx,Nz,z)
    w = -d_dx(Psi,Nt,Nx,Nz,x)
    data = d_dz(u,Nt,Nx,Nz,z)
    data2 = d_dz(w,Nt,Nx,Nz,z)

    if MakePlot == 1:

        PlotTitle = r'$\partial u/\partial z$'
        PlotTitle2 = r'$\partial w/\partial z$'
        FigNmBase = 'dUdz' 
        cmap = 'bwr'

        nlevs = 41
        uz_max = 20.
        uz_min = -20.
        duz = (uz_max-uz_min)/(nlevs-1)
        clevels = np.arange(nlevs)*duz + uz_min

        nlevs = 41
        wz_max = 20.
        wz_min = -20.
        dwz = (wz_max-wz_min)/(nlevs-1)
        clevels2 = np.arange(nlevs)*dwz + wz_min


if N_local == 1:
    Sz = d_dz(S,Nt,Nx,Nz,z)

    N_local = np.zeros((Nt,Nx,Nz))
    for jj in range(1,Nz-1):
        for ii in range(1,Nx-1):
            N_local[:,ii,jj] = sqrt(-g*cs*Sz[:,ii,jj])

    data = N_local

    if MakePlot == 1:
        PlotTitle = 'N'
        FigNmBase = 'N'
        cmap = 'brg'

        nlevs = 41
        NRange = 3.
        dN = NRange/(nlevs-1)
        clevels = np.arange(nlevs)*dN 


if Richardson == 1:
    Sz = d_dz(S,Nt,Nx,Nz,z)
    u = d_dz(Psi,Nt,Nx,Nz,z)
    uz = d_dz(u,Nt,Nx,Nz,z)

    N2_local = np.zeros((Nt,Nx,Nz)) 
    Ri_local = np.zeros((Nt,Nx,Nz))
    for jj in range(1,Nz-1):
        for ii in range(1,Nx-1):
            N2_local[:,ii,jj] = -g*cs*Sz[:,ii,jj] 
            Ri_local[:,ii,jj] = N2_local[:,ii,jj]/uz[:,ii,jj]**2
            
            #Set inf points to arbitrary value for plotting purposes 
            #Python contour routine will not colour inf regions
            #LogicalInf = np.isinf( Ri_local[:,ii,jj] )
            #idxsInf = np.where( LogicalInf == 1 ) 
            #if len(idxsInf[0]) != 0:
            #    Ri_local[idxsInf[0],ii,jj] = -999
            #N.B. there are only a very few inf points so this code section is not essential - it has minimal impact on plots.

    data = Ri_local

    if MakePlot == 1:
        PlotTitle = 'Ri'
        FigNmBase = 'Ri'
        cmap = 'PRGn'

        nlevs = 41
        RiC = 1/4.
        RiRange = 2.
        dRi = RiRange/(nlevs-1)
        clevels = np.arange(nlevs)*dRi - (RiRange/2.-RiC)


if FourierSinCosBasis == 1:

    data = S
    #data = Psi

    #Compute Fourier-SinCos basis coefficients:    
    data_hat = np.zeros( (Nt,Nx,Nz) )*1j

    for tt in range(0,Nt):
        tmp = data[tt,:,:]
        data_hat[tt,:,:] = cst.FFT_FST(Nx,Nz,tmp)
    
    if MakePlot == 1:
        
        count1 = 0
        #linewidthvec = [3,3,3,3,3,3,3,0.2,0.2,0.2,0.2,0.2,0.2,0.2]
        #linecolorvec = ['k','b','c','g','y','m','r','k','b','c','g','y','m','r'] 
        #linecolorvec = ['black','silver','blue','cyan','lightblue','green','orange','gold','red','m'] 
        linecolorvec = ['black','silver','m','red','blue','cyan','lightblue','green','orange','gold'] 
        
        for jj in range(0,Nz):
            for ii in range(0,int(Nx/2.)):
            
                #f = abs(data_hat[:,ii,jj])
                f = abs(data_hat[:,ii,jj])**2
                 
                #limit = 0
                #PowerLimit = 10**(2)
                #PowerLimit = 5*10**(2)
                PowerLimit = 10**(3)
                #PowerLimit = 5*10**(4)
                #PowerLimit = 5*10**(5)
                if sum(f) > PowerLimit: 

                    label = str(ii) + ',' + str(jj)
                    color = linecolorvec[count1]

                    #plt.semilogy(f, linewidth=linewidth, color=color, label=label)
                    plt.semilogy(t, f, linewidth=2, color=color, label=label)
                    #plt.semilogy(f)
         
                    count1 = count1 + 1
        
        print(count1)
        plt.xlabel('t (s)')
        plt.ylim(10**(-4),10**(2))
        plt.legend()
        plt.show()
        

if SpectralAnalysisT == 1:
   
    data = S
    #data = Psi

    #Look at time series for point(s) in domain:
    Welch = 1

    signal_f = 1./dt				#Sampling frequency (needs to be units 1/s for Welch method)
    BigFreqArr = np.zeros((Nx,Nz))   
    f0 = 0.1

    if Welch == 0:
        SpectralCoef = np.zeros((int(Nt/2.)+1,Nx,Nz))
        freqvec = np.arange(Nt/2.+1)*1./Nt      #assumes dt=1
                                                #Note array length is Nt/2 and so max freq. will be Nyquist freq.
        freqvec         = freqvec*signal_f      #uses actual signal frequency (Dedalus timestep)
        #freqvec        = freqvec*2*np.pi       #converts to angular frequency (rad/s) - but less intuitive
    
    if Welch == 1:
        #Essentially this method divides signal into a number of segments that are nperseg points long. 
        #A form of spectral analysis (periodogram) is performed on each segment.
        #Then an average periodogram is computed from the set of periodograms.
        #By default the segments overlap and the overlap is nperseg/2.
        #So if nperseg is longer then we have higher frequency resolution but possibly more noise.
        dnmntr = 5
        nperseg = int(Nt/dnmntr)
        SpectralCoef = np.zeros((int(nperseg/2.)+1,Nx,Nz))

    #Perform spectral analysis on time series at each grid point:
    for jj in range(0,Nz):
        for ii in range(0,Nx):

            ts = data[:,ii,jj]

            if Welch == 0: 
                #ts = ts*np.hanning(Nt)		#apply Hanning window function to make periodic signal
                ts_hat = abs( np.fft.fft(ts) )
                #ts_power = ts_hat**2
                SpectralCoef[:,ii,jj] = ts_hat[0:int(Nt/2.)+1]

                #Remove slow flows:
                fIdxVec = np.where(freqvec <= f0)
                fIdx = max(fIdxVec[0])

                ts_hat2 = ts_hat[fIdx:int(Nt/2.)+1]
                idx = np.where( ts_hat2 == max(ts_hat2) )
                BigFreqArr[ii,jj] = freqvec[idx[0] + fIdx]  
            
            if Welch == 1:
                freqvec, psd = welch( ts,
                                      fs=signal_f,		# sampling rate
                                      window='hanning',		# apply a Hanning window before taking the DFT
                                      nperseg=nperseg,		# compute periodograms of nperseg-long segments of ts
                                      detrend='constant')	# detrend ts by subtracting the mean
                
                SpectralCoef[:,ii,jj] = psd
                #Remove slow flows:
                fIdxVec = np.where(freqvec <= f0)
                fIdx = max(fIdxVec[0])
                
                ts_hat2 = psd[fIdx:int(nperseg/2.)+1]
                #Find dominant frequencies:
                idx = np.where( ts_hat2 == max(ts_hat2) )[0]
                BigFreqArr[ii,jj] = freqvec[idx[0] + fIdx]

    if MakePlot == 1:

        fig = plt.figure(1, figsize=(width,height))
        #grid = plt.GridSpec(1, 4, wspace=0.5, hspace=0.)
        grid = plt.GridSpec(1, 2, wspace=0.5, hspace=0.)
 
        #ax1 = fig.add_subplot(grid[0,:2])
        #ax1.set_xlim(0,Lx)
        #ax1.set_ylim(0,Lz)

        #rangeF = 5.
        #nf = 51 
        #df = rangeF/(nf-1)
        #clevels = np.arange(nf)*df
        #i1 = ax1.contourf(x2d,z2d,BigFreqArr.transpose(),clevels,cmap='seismic',extend="both")
        #plt.contourf(x2d,z2d,BigFreqArr.transpose(),cmap='seismic')
        #fig.colorbar(i1, orientation="horizontal")
        #plt.colorbar(i1)

        xIdx = int(Nx/2.)
        #nz = 8
        nz = 1
        dz = Nz/nz
        #zIdx0 = 0
        zIdx0 = Nz/2
        zIdxs = np.arange(nz)*dz + zIdx0
        xlim = 5

        for ii in range(0,nz):

            f = SpectralCoef[:,xIdx,zIdxs[ii]]

            if ii == 0:
                #ax2 = fig.add_subplot(grid[0,2])
                ax2 = fig.add_subplot(grid[0,0])
                ax2.set_xlabel(r'$f$' ' (Hz)')
                ax2.set_xlim(0,xlim)
            ax2.plot(freqvec,f)

            if ii == 0:
                #ax3 = fig.add_subplot(grid[0,3])
                ax3 = fig.add_subplot(grid[0,1])
                ax3.set_xlabel(r'$f$' ' (Hz)')
                ax3.set_xlim(0,xlim)
            ax3.semilogy(freqvec,f)

            #Show position of analysis points on contour plot:
            #ax1.plot( [x[xIdx],x[xIdx]], [z[zIdxs[ii]],z[zIdxs[ii]]], '+' )

        plt.show()


if TimeSeriesAnalysisPE == 1:    
    #Look at time series of change of total PE in each column
    Welch = 1
    signal_f = 1./dt                             #Sampling frequency (needs to be units 1/s for Welch method)
 
    if Welch == 1:
        dnmntr = 50
        nperseg = int(Nt/dnmntr)
        SpectralCoef = np.zeros((int(nperseg/2.)+1,Nx))

    #Perform spectral analysis on time series of change of total PE in each column:
    for ii in range(0,Nx):
        ts = PEcolumn[:,ii]
        if Welch == 1:
            freqvec, psd = welch( ts,
                                  fs=signal_f,              # sampling rate
                                  window='hanning',         # apply a Hanning window before taking the DFT
                                  nperseg=nperseg,          # compute periodograms of nperseg-long segments of ts
                                  detrend='constant')       # detrend ts by subtracting the mean

            SpectralCoef[:,ii] = psd

    if MakePlot == 1:
        xIdx = 20

        plt.figure(1)
        plt.subplot(121) 
        plt.plot(freqvec,SpectralCoef[:,xIdx])
        plt.xlabel(r'$f$' ' (Hz)')
        plt.subplot(122)
        plt.semilogy(freqvec,SpectralCoef[:,xIdx])
        plt.xlabel(r'$f$' ' (Hz)')

        plt.show()


if NaturalBasis == 1:

    # Eigenvectors are not a fnc of time.
    # There is one eigenvector for each alpha.
    # Eigenvectors are assumed to be a fnc of k and m wavenumbers 
    # (see structure of NaturalFreq.py).
    # An eigenvector, for some k and m, has nvar elements.
    ivec_1 = np.zeros((Nx,Nz,nvars))
    if nvars == 3: ivec0 = np.zeros((Nx,Nz,nvars))
    ivec1 = np.zeros((Nx,Nz,nvars))

    if BasisCheck1 == 1:
        State2 = np.zeros((Nx,Nz,Nt,nvars))

    if BasisCheck2 == 1:
        #Store wavenumber-vector magnitude array to transform back to original State 
        #in NaturalFreq.py 
        kmag_arr = np.zeros((Nx,Nz))

    for tt in range(0,Nt):

        print(tt)
 
        Psi_hat 	= cst.FFT_FST(Nx,Nz,Psi[tt,:,:])
        if nvars ==3: 
            T_hat 	= cst.FFT_FST(Nx,Nz,T[tt,:,:])
        S_hat 		= cst.FFT_FST(Nx,Nz,S[tt,:,:])

        # There is one sigma for each alpha.
        # Sigmas are complex and depend on k and m wavenumbers, and vary through time, 
        # so here we re-define the arrays for each time point in the loop. 
        # n.b. _1: alpha=-1, 0: alpha=0, 1: alpha=+1 
        sigma_1 = np.zeros((Nx,Nz))*1j
        if nvars == 3: sigma0 = np.zeros((Nx,Nz))*1j
        sigma1 = np.zeros((Nx,Nz))*1j

        #Loop over wavenumbers:
        for jj in range(1,Nz):
            for ii in range(0,Nx):

                k = kk[ii]
                n = kk_cosine[jj]

                kvec    = np.array([k,n])
                kmag    = np.linalg.norm(kvec)
                if (BasisCheck2==1) and (tt==0): kmag_arr[ii,jj] = kmag

                if nvars == 3: c1 = sqrt(-ct/bt)
                c2          = sqrt(cs/bs)
                c3          = abs(k)/kmag*sqrt(-(ct*bt-cs*bs))          #N.B. omega = c3*sqrt(g)

                #Transform spectral coefficients - required to make 
                #linear operator skew hermitian
                Psi_hat[ii,jj]        	= Psi_hat[ii,jj]*kmag
                if nvars == 3:
                    T_hat[ii,jj]       	= T_hat[ii,jj]*sqrt(g)*c1
                S_hat[ii,jj]           	= S_hat[ii,jj]*sqrt(g)*c2

                if k != 0:
                #k=0 is a special case with different eigenvectors.

                    if nvars == 3:
                        #Construct eigenvectors:
                        r0      = np.array([0,c2/c1,1]).real
                        r0mag   = np.linalg.norm(r0)
                        r0      = r0/r0mag

                        r1      = np.array([-c3/c2,-c1/c2,1]).real
                        r1mag   = np.linalg.norm(r1)
                        r1      = r1/r1mag

                        r_1     = np.array([c3/c2,-c1/c2,1]).real
                        r_1mag  = np.linalg.norm(r_1)
                        r_1     = r_1/r_1mag

                    if nvars == 2:
                        r1      = np.array([-abs(k)/k,1])
                        r1mag   = np.linalg.norm(r1)
                        r1      = r1/r1mag

                        r_1     = np.array([abs(k)/k,1])
                        r_1mag  = np.linalg.norm(r_1)
                        r_1     = r_1/r_1mag

                if k == 0:

                    if nvars == 3:
                        #Construct eigenvectors:
                        r0      = np.array([0,1,0]).real
                        r0mag   = np.linalg.norm(r0)
                        r0      = r0/r0mag

                        r1      = np.array([-1,0,1]).real
                        r1mag   = np.linalg.norm(r1)
                        r1      = r1/r1mag

                        r_1     = np.array([1,0,1]).real
                        r_1mag  = np.linalg.norm(r_1)
                        r_1     = r_1/r_1mag

                    if nvars == 2:
                        r1      = np.array([0,1])
                        r1mag   = np.linalg.norm(r1)
                        r1      = r1/r1mag

                        r_1     = np.array([1,0])
                        r_1mag  = np.linalg.norm(r_1)
                        r_1     = r_1/r_1mag

                #Store eigenvectors for printing later:
                #There are n eigenvectors, each with nvar components, where each component is assumed to 
                #depend on k and m wavenumbers (see structure of NaturalFreq.py). 
                #Eigenvectors are time invariant. 
                #There are many ways you could print the eigenvectors, each with shape (Nx,Nz,nvar).
                #I looped over the index with nvar elements and printed out each 2D array of (Nx,Nz) numbers. 
                #Python doesn't like printing 3D arrays.
                #I simply reverse this procedure when reading in my eigenvectors in NaturalFreq.py.
                if tt == 0:
                    ivec_1[ii,jj,:] = r_1
                    if nvars == 3: ivec0[ii,jj,:] = r0
                    ivec1[ii,jj,:] = r1

                #construct matrix of eigenvectors for some k and n, and
                #construct vector of spectral coefficients for some k,n 
                if nvars == 3:
                    EigenVecs = np.array([r_1,r0,r1]).transpose()
                    upsilon = np.array([Psi_hat[ii,jj],T_hat[ii,jj],S_hat[ii,jj]])
                else:
                    EigenVecs = np.array([r_1,r1]).transpose()
                    upsilon = np.array([Psi_hat[ii,jj],S_hat[ii,jj]])

                #Make transformation to find amplitudes of Natural basis 
                EigenVecs_inv = linalg.inv(EigenVecs)
                sigma_kn = np.mat(EigenVecs_inv) * np.mat(upsilon).transpose()

                if nvars == 3:
                    sigma_1[ii,jj] = sigma_kn[0]
                    sigma0[ii,jj] = sigma_kn[1]
                    sigma1[ii,jj] = sigma_kn[2]
                if nvars == 2:
                    sigma_1[ii,jj] = sigma_kn[0]
                    sigma1[ii,jj] = sigma_kn[1]

        if BasisCheck1 == 1:
            #Reverse transform back to the new State:
            if nvars == 3:
                State2[:,:,tt,0] = cst.iFFT_FST(Nx,Nz,Psi_hat)
                State2[:,:,tt,1] = cst.iFFT_FST(Nx,Nz,T_hat)
                State2[:,:,tt,2] = cst.iFFT_FST(Nx,Nz,S_hat)
            if nvars == 2:
                State2[:,:,tt,0] = cst.iFFT_FST(Nx,Nz,Psi_hat)
                State2[:,:,tt,1] = cst.iFFT_FST(Nx,Nz,S_hat)

        # For writing natural basis to a file:
        if w2f_analysis == 1:

            #First write the coefficients (sigmas) of the natural basis:
            if tt == 0:
                dir_sigma = './Results/' + ProblemType + '/NaturalBasis/'
                #Create directory if it doesn't exist:
                if not os.path.exists(dir_sigma):
                    os.makedirs(dir_sigma)

            fnm_sigma_1 = dir_sigma + 'sigma_1_' + str(tt) + '.txt'
            fnm_sigma1 = dir_sigma + 'sigma1_' + str(tt) + '.txt'
            np.savetxt(fnm_sigma_1,sigma_1.view(float))
            np.savetxt(fnm_sigma1,sigma1.view(float))
            if nvars == 3:
                fnm_sigma0 = dir_sigma + 'sigma0_' + str(tt) + '.txt'
                np.savetxt(fnm_sigma0,sigma0.view(float))

            #Then write out the eigenvectors (as explained above): 
            if tt == 0:
                dir_ivec = './Results/' + ProblemType + '/NaturalBasis/'
                #Create directory if it doesn't exist:
                if not os.path.exists(dir_ivec):
                    os.makedirs(dir_ivec)

                for ll in range(0,nvars):
                    #For complex eigenvectors:
                    #for nn in range(0,2):
                        #fnm_ivec_1 = dir_ivec + 'ivec_1_' + str(ll+1) + str(nn+1) + '.txt'
                        #fnm_ivec0 = dir_ivec + 'ivec0_' + str(ll+1) + str(nn+1) + '.txt'
                        #fnm_ivec1 = dir_ivec + 'ivec1_' + str(ll+1) + str(nn+1) + '.txt'
                        #pdb.set_trace()
                        #ivec_1_float = ivec_1.view(float)
                        #ivec0_float = ivec0.view(float)
                        #ivec1_float = ivec1.view(float)
                        #pdb.set_trace()
                    fnm_ivec_1 = dir_ivec + 'ivec_1_' + str(ll+1) + '.txt'
                    fnm_ivec1 = dir_ivec + 'ivec1_' + str(ll+1) + '.txt'
                    np.savetxt(fnm_ivec_1,ivec_1[:,:,ll])
                    np.savetxt(fnm_ivec1,ivec1[:,:,ll])
                    if nvars == 3:
                        fnm_ivec0 = dir_ivec + 'ivec0_' + str(ll+1) + '.txt'
                        np.savetxt(fnm_ivec0,ivec0[:,:,ll])

            if (BasisCheck2==1) and (tt==0):
                fnm_kmag = dir_ivec + 'kmag_arr.txt'
                np.savetxt(fnm_kmag,kmag_arr)


#Generic stastical processing:
if xMean == 1:
    data = x_mean(data)
    if 'data2' in locals(): data2 = x_mean(data2)
    #data has shape (Nt,Nz)
if tMean == 1:
    data = t_mean(data)
    if 'data2' in locals(): data2 = t_mean(data2)
    #data has shape (Nx,Nz)
if SlidingMean == 1:
    data = sliding_mean(data,Nt,Nx,Nz,wing)
    if 'data2' in locals(): data2 = sliding_mean(data2,Nt,Nx,Nz,wing)
    #data has shape (Nt,Nx,Nz)


#Plotting section:
#The intention was to try and avoid repeating the bulk of the code below 
#numerous times for different variables. Repitition has certainly been 
#significantly reduced, however, it is very hard to make a fully general
#plotting program for research purposes, which are exploratory. Currently 
#this section excludes the time-series analysis and spectral methods sections.
if PlotXZ==1 or PlotTZ==1:
    if PlotXZ == 1:
        #We define 3 options here:
        #1) consider spatial field at some t.
        #2) consider spatial field averaged across all t.
        #3) apply a sliding time-mean to data and then consider field at some t. 
        #If an average is desired then the correct switch should be used.
        if tMean != 1:
            if MakeMovie != 1:
                try:
                    tIdx
                except NameError:
                    Nhr = 0
                    Nmin = StartMin
                    Nsec = 0
                    time = Nhr*(60*60) + Nmin*60 + Nsec
                    tIdx = int((time-t[0])/dt/tq)

                data = data[tIdx,:,:].transpose()
                if 'data2' in locals():
                    data2 = data2[tIdx,:,:].transpose()
     
        #Set arrays for contouring:
        xgrid = x2d
        ygrid = z2d

    if PlotTZ == 1:
        #We define 3 options here:
        #1) consider how a single column evolves over time.
        #2) consider how the average column evolves over time.
        #3) apply a sliding time-mean to the data and then consider a single column.
        #If an average is desired then the correct switch should be used.
        if xMean != 1:
            xIdx = int(Nx/2.)
            data = data[:,xIdx,:].transpose()
            if 'data2' in locals():
                data2 = data2[:,xIdx,:].transpose()

        #Set arrays for contouring:
        xgrid = t2d_z
        ygrid = z2d_t

    if MakeMovie != 1:
            
        fig=plt.figure(figsize=(width,height))
        if 'data2' in locals():
            grid = plt.GridSpec(1, 2, wspace=0.4, hspace=0.0)
            ax1 = fig.add_subplot(grid[0,0])
        if PlotProfileZ == 1:
            grid = plt.GridSpec(1, 2, wspace=0.4, hspace=0.0)
            ax1 = fig.add_subplot(grid[0,0])
        else:
            grid = plt.GridSpec(1, 1, wspace=0.0, hspace=0.0)
            ax1 = fig.add_subplot(grid[0,0])

        i1=ax1.contourf(xgrid,ygrid,data,clevels,cmap=cmap,extend="both")
        fig.colorbar(i1)
        ax1.set_ylim(0,Lz)
        ax1.set_ylabel('z (m)')
        if PlotXZ == 1: 
            ax1.set_xlim(0,Lx)
            ax1.set_xlabel('x (m)')
            ax1.set_title( PlotTitle + ", " + str("%04d" % t[tIdx]) + " s" )
        if PlotTZ == 1:
            ax1.set_xlabel('t (s)')
            ax1.set_title(PlotTitle)

        if PlotProfileZ == 1:

            xIdx1 = int(Nx/2.)
            #xIdx2 = int(Nx/2.)/2
            #xIdx3 = int(Nx/2.)*3/2
            #xIdx4 = 2
            #xIdx5 = Nx-3

            #ax1.plot([x[xIdx1],x[xIdx1]],[z[0],z[Nz-1]], 'b')
            #ax1.plot([x[xIdx2],x[xIdx2]],[z[0],z[Nz-1]], 'g')
            #ax1.plot([x[xIdx3],x[xIdx3]],[z[0],z[Nz-1]], 'c')
            #ax1.plot([x[xIdx4],x[xIdx4]],[z[0],z[Nz-1]], 'm')
            #ax1.plot([x[xIdx5],x[xIdx5]],[z[0],z[Nz-1]], 'y')

            ax2 = fig.add_subplot(grid[0,1])
            ax2.set_ylim(0,Lz)
            ax2.plot(S[tIdx,xIdx1,:],z, 'b')
            #ax2.plot(S[tIdx,xIdx2,:],z, 'g')
            #ax2.plot(S[tIdx,xIdx3,:],z, 'c')
            #ax2.plot(S[tIdx,xIdx4,:],z, 'm')
            #ax2.plot(S[tIdx,xIdx5,:],z, 'y')

            ax2.set_xlabel('S (g/kg)')
            ax2.set_xlim(0,300)

        if 'data2' in locals():
            ax2 = fig.add_subplot(grid[0,1])
            i2=ax2.contourf(xgrid,ygrid,data2,clevels2,cmap=cmap,extend="both")
            fig.colorbar(i2)
            ax2.set_ylim(0,Lz)
            if PlotXZ == 1: ax2.set_xlim(0,Lx)
            ax2.set_title( PlotTitle2 + ", " + str("%04d" % t[tIdx]) + " s" )

        plt.show()

    if MakeMovie == 1:
        #If you wish to read in all model outputs, make a sliding average on full output,
        #but then only plot a sub-sample of the data, then you can define a plot 
        #interval here:
        if dt == dt2:
            dt_plot = 2.
            dplot = int(dt_plot/dt)
        else:
            dplot = 1

        FigPath = dir_state + 'images/'
        for tt in range(0,Nt,dplot):
            fig=plt.figure(figsize=(width,height))
            if 'data2' in locals():
                grid = plt.GridSpec(1, 2, wspace=0.4, hspace=0.0)
                ax1 = fig.add_subplot(grid[0,0])
            if PlotProfileZ == 1:
                grid = plt.GridSpec(1, 2, wspace=0.4, hspace=0.0)
                ax1 = fig.add_subplot(grid[0,0])
            else:
                grid = plt.GridSpec(1, 1, wspace=0.0, hspace=0.0)
                ax1 = fig.add_subplot(grid[0,0])

            i1=ax1.contourf(xgrid,ygrid,data[tt,:,:].transpose(),clevels,cmap=cmap,extend="both")
            fig.colorbar(i1)
            ax1.set_xlim(0,Lx)
            ax1.set_ylim(0,Lz)
            ax1.set_xlabel('x (m)')
            ax1.set_ylabel('z (m)')
            ax1.set_title(PlotTitle + ", " + str("%04d" % t[tt]) + " s")


            if PlotProfileZ == 1:
                xIdx1 = int(Nx/2.)
                #xIdx2 = int(Nx/2.)/2
                #xIdx3 = int(Nx/2.)*3/2
                #xIdx4 = 2
                #xIdx5 = Nx-3

                #ax1.plot([x[xIdx1],x[xIdx1]],[z[0],z[Nz-1]], 'b')
                #ax1.plot([x[xIdx2],x[xIdx2]],[z[0],z[Nz-1]], 'g')
                #ax1.plot([x[xIdx3],x[xIdx3]],[z[0],z[Nz-1]], 'c')
                #ax1.plot([x[xIdx4],x[xIdx4]],[z[0],z[Nz-1]], 'm')
                #ax1.plot([x[xIdx5],x[xIdx5]],[z[0],z[Nz-1]], 'y')

                ax2 = fig.add_subplot(grid[0,1])
                ax2.set_ylim(0,Lz)
                ax2.plot(S[tt,xIdx1,:],z, 'b')
                #ax2.plot(S[tIdx,xIdx2,:],z, 'g')
                #ax2.plot(S[tIdx,xIdx3,:],z, 'c')
                #ax2.plot(S[tIdx,xIdx4,:],z, 'm')
                #ax2.plot(S[tIdx,xIdx5,:],z, 'y')
                ax2.set_xlabel('S (g/kg)')
                ax2.set_xlim(0,300)

            if 'data2' in locals():
                ax2 = fig.add_subplot(grid[0,1])
                i2=ax2.contourf(xgrid,ygrid,data2[tt,:,:].transpose(),clevels2,cmap=cmap,extend="both")
                fig.colorbar(i2)
                ax2.set_ylim(0,Lz)
                ax2.set_ylabel('z (m)')
                if PlotXZ == 1: 
                    ax2.set_xlim(0,Lx)
                    ax2.set_xlabel('x (m)')
                if PlotTZ == 1:
                    ax2.set_xlabel('t (s)')
                ax2.set_title(PlotTitle2 + ", " + str("%04d" % t[tt]) + " s")

            FigNm = FigNmBase + '_' + str("%04d" % t[tt]) + '.png'
            fig.savefig(FigPath+FigNm)
            plt.close(fig)















pdb.set_trace()
