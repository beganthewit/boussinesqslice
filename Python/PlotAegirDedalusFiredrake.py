#Script to plot and compare model results


#Import required libraries:
import h5py    
import numpy as np    
import matplotlib.pyplot as plt
from scipy.interpolate import interp2d
import pdb #to pause execution use pdb.set_trace()


#Programme control:
ReadDedalusResults = 1
ReadAegirResults = 0
ReadFiredrakeResults = 0
InterpolateDedalus = 0
CompareResults = 0
FullFields = 1
ProblemType = 'Layers'
ParkRun = 14
#ParkRun = 18


#Set up grid and related objects:
Lx = 0.2
Lz = 0.45
Nx = 80
Nz = 180
nt = 695

dx = float(Lx)/Nx
dz = float(Lz)/(Nz-1)

x_grid = np.arange(Nx)*dx
z_grid = np.arange(Nz)*dz


#Set physical constants and related objects:
g = 9.81
ct = 2*10**(-4.)
cs = 7.6*10**(-4.)

if ParkRun == 18:               #for lab run 18 of Park et al
    rho0 = 1090.95075
    drho0_dz = -425.9
if ParkRun == 14:               #for lab run 14 of Park et al
    rho0 = 896.2416
    drho0_dz = -31.976

if ProblemType == 'Layers':
    bt = 0.
    bs = -1./(rho0*cs)*drho0_dz

    drho0_dz13 = -122.09
    dgamma = 100./3
    dz_b = 2./100
    a0 = 100.
    z_a = Lz/2
    rhoprime13 = dgamma*z_a + a0*dz_b + dgamma/2*dz_b
    rhoprime = rhoprime13 * drho0_dz/drho0_dz13
    Spert0 = rhoprime * 1./(rho0*cs)

if ProblemType == 'KelvinHelmholtz':
    bt = 0.
    bs = 0.


#Read in results:

if ReadDedalusResults == 1:
    #Read in Dedalus results:
    #fnm_dedalus = 'snapshots_s1_A.h5'	#ICs set using spectral coefficients
    #fnm_dedalus = 'snapshots_s1_B.h5'	#ICs set in real space
    #fnm_dedalus = 'snapshots_s1.h5'
    fnm_dedalus = 'State_s60.h5'
    file = h5py.File(fnm_dedalus,'r+')    
    psi_dedalus = file['tasks']['psi']
    SS_dedalus = file['tasks']['S']

if ReadAegirResults == 1:
    #Read in Aegir results:
    Nx	= 80
    Nz	= 180
    nt	= 60
    Psi_aegir 	= np.zeros((Nx,Nz,nt))
    TT_aegir 	= np.zeros((Nx,Nz,nt))
    SS_aegir 	= np.zeros((Nx,Nz,nt))
    dir_state = '/Users/pb412/Documents/career/exeter/bbppm/boussinesqslice/Python/Results/KelvinHelmholtz/State/'
    for tt in range(0,nt):
        fnm_psi 	= dir_state + 'Psi_' + str(tt) + '.txt'
        fnm_TT 	= dir_state + 'TT_' + str(tt) + '.txt'
        fnm_SS 	= dir_state + 'SS_' + str(tt) + '.txt'
        Psi_aegir[:,:,tt] 	= np.loadtxt(fnm_psi)
        TT_aegir[:,:,tt] 	= np.loadtxt(fnm_TT)
        SS_aegir[:,:,tt] 	= np.loadtxt(fnm_SS)

if ReadFiredrakeResults == 1:
    #Read in Firedrake results:
    #fnm_firedrake = ""
    #self.data = Dataset(fnm_firedrake, "r")
    #self.grp = self.data.groups["Psi"]
    print('incomplete')


if FullFields == 1:
    #Add background fields to perturbations:
    STop = Spert0
    Sbase = -bs*(z_grid-Lz) + STop
    SS_dedalus += Sbase    


if InterpolateDedalus == 1:
    #Interpolate Dedalus results onto grid used by Aegir:

    #Read in Dedalus grid (written out from Dedalus model):
    dir_grid = './'
    fnm_gridx = dir_grid + 'XGridDedalus.txt'
    fnm_gridz = dir_grid + 'ZGridDedalus.txt'
    x_dedalus = np.loadtxt(fnm_gridx)
    z_dedalus = np.loadtxt(fnm_gridz)

    #Re-construct Aegir grid:
    Lx = 0.2
    Lz = 0.45
    dx = float(Lx)/Nx
    dz = float(Lz)/(Nz-1)
    x_aegir = np.arange(Nx)*dx
    z_aegir = np.arange(Nz)*dz
    #pdb.set_trace()

    #Interpolate Dedalus results onto Aegir grid:
    Psi_dedalus2 = np.zeros((Nx,Nz,nt))
    for tt in range(0,nt):
        f = interp2d(x_dedalus, z_dedalus, Psi_dedalus[tt,:,:].transpose(), kind='cubic')
        Psi_dedalus2[:,:,tt] = f(x_aegir, z_aegir).transpose()


if CompareResults == 1:
    #Compare results by finding differences:
    #pdb.set_trace()
    data = Psi_dedalus2 - Psi_aegir

    #Plot differences:
    t =10
    plt.figure(1)
    nlevels = 24
    psi_min = -0.0023
    dpsi = np.abs(psi_min)/(nlevels-1)
    levels = np.arange(nlevels)*dpsi + psi_min
    #pdb.set_trace()

    plt.subplot(221)
    cp = plt.contourf(x_aegir,z_aegir,Psi_aegir[:,:,t].transpose(),levels)
    plt.title('Streamfunction in Aegir at t =' + str(t) + ' s')
    plt.xlabel('x')
    plt.ylabel('z')
    plt.colorbar(cp)

    plt.subplot(222)
    cp = plt.contourf(x_aegir,z_aegir,Psi_dedalus2[:,:,t].transpose(),levels)
    plt.title('Streamfunction in Dedalus at t=' + str(t) + ' s' )
    plt.xlabel('x')
    plt.ylabel('z')
    plt.colorbar(cp)

    plt.subplot(223)
    cp = plt.contourf(x_aegir,z_aegir,data[:,:,t].transpose(),nlevels)
    plt.title('Streamfunction differences at t=' + str(t) + ' s')
    plt.xlabel('x')
    plt.ylabel('z')
    plt.colorbar(cp)

    plt.subplot(224)
    plt.plot(data[int(Nx/2),int(Nz/2),:])
    plt.title('Change of streamfunction differences over time for central grid point')
    plt.xlabel('t (s)')

    plt.show()


pdb.set_trace()
