#Code to compute State from the Natural basis and for Frequency Averaging


#Load in required libraries:
import numpy as np
from numpy import *
from scipy import *
from numpy import fft
from scipy import fftpack
import CosineSineTransforms as cst
import pdb #to pause execution use pdb.set_trace()
import os
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.signal import welch


#Program control:
ProblemType 			= 'Layers'
#ProblemType 			= 'KelvinHelmholtz'
ParkRun 			= 18
DiffusionFactor			= 500
BasisCheck2 			= 0
nvars 				= 2
sigma3D 			= 1
FindMainBasisParts 		= 1
PlotMainBasisParts 		= 0
PlotState_MainBasis		= 0
ConvergePowerLimit 		= 1
AegirTransforms			= 1
AnalyseMainBasisParts		= 0
SpectralAnalysis		= 0
DispersionRelation		= 0
Resonances			= 1
LinearNonLinear			= 0
NearResonanceSearch		= 1


#Set up grid and related objects:
Lx = 0.2
Lz = 0.45
Nx = 80
Nz = 180
Nt = 10

dt = .1
#dt = 1.

dx = float(Lx)/Nx
dz = float(Lz)/(Nz-1)

x = np.arange(Nx)*dx
z = np.arange(Nz)*dz

t = np.arange(Nt)*dt

# The wave number array (frequencies)
kk = np.fft.fftfreq(Nx,Lx/Nx)*2.*np.pi
# And similarly for the cosine series
kk_cosine = np.arange((Nz))*np.pi/Lz
#pdb.set_trace()


#Set physical constants and related objects:
g = 9.81
ct = 2*10**(-4.)
cs = 7.6*10**(-4.)

if ParkRun == 18:               #for lab run 18 of Park et al
    N2 = 3.83
    drho0_dz = -425.9
    rho0 = -g/N2*drho0_dz
if ParkRun == 14:               #for lab run 14 of Park et al
    N2 = 0.35
    drho0_dz = -31.976
    rho0 = -g/N2*drho0_dz

if ProblemType == 'Layers':
    bs = -1./(rho0*cs)*drho0_dz
    if nvars == 3: bt = -bs*0.001
    if nvars == 2: bt = 0
if ProblemType == 'KelvinHelmholtz':
    bt = 0.
    bs = 0.

# Physical Diffusion:
nu = 1.*10**(-6.)
kappat = 1.4*10**(-7.)
kappas = 1.4*10**(-7.)
nu = nu * DiffusionFactor
kappat = kappat * DiffusionFactor
kappas = kappas * DiffusionFactor


#Set time independent objects:
dir_sigma = './Results/' + ProblemType + '/NaturalBasis/'
dir_ivec = './Results/' + ProblemType + '/NaturalBasis/'

State = np.zeros((Nx,Nz,Nt,nvars))

ivec_1 = np.zeros((Nx,Nz,nvars))
if nvars == 3: ivec0 = np.zeros((Nx,Nz,nvars))
ivec1 = np.zeros((Nx,Nz,nvars))

if BasisCheck2 == 1:
    #Define transforms to get back to State:
    fnm_kmag = dir_ivec + 'kmag_arr.txt'
    t1 = np.loadtxt(fnm_kmag)
    if nvars == 3: t2 = sqrt(-g*ct/bt)
    t3 = sqrt(g*cs/bs)

if sigma3D == 1:
    sigma_1_3D = np.zeros((Nx,Nz,Nt))*1j
    if nvars == 3: sigma0_3D = np.zeros((Nx,Nz,Nt))*1j
    sigma1_3D = np.zeros((Nx,Nz,Nt))*1j


#Set general plotting parameters assuming A4 page size:
A4Width = 8.27
MarginWidth = 1
width = A4Width-2*MarginWidth
height = 4
#For scaling the A4 plot dimensions:
ScaleFactor = 1
#ScaleFactor = 0.7
width = width*ScaleFactor
height = height*ScaleFactor


#Reconstruct State using the Natural Basis:
for tt in range(0,Nt):
    
    print(tt)
 
    fnm_sigma_1 = dir_sigma + 'sigma_1_' + str(tt) + '.txt'
    fnm_sigma1 = dir_sigma + 'sigma1_' + str(tt) + '.txt'
    sigma_1 = np.loadtxt(fnm_sigma_1).view(complex)
    sigma1 = np.loadtxt(fnm_sigma1).view(complex)
    if nvars == 3:
        fnm_sigma0 = dir_sigma + 'sigma0_' + str(tt) + '.txt'
        sigma0 = np.loadtxt(fnm_sigma0).view(complex)

    if sigma3D == 1:
        sigma_1_3D[:,:,tt] = sigma_1
        if nvars == 3: sigma0_3D[:,:,tt] = sigma0
        sigma1_3D[:,:,tt] = sigma1

    if tt == 0:
        for ll in range(0,nvars):
            #for nn in range(0,2):
            #    fnm_ivec_1 = dir_ivec + 'ivec_1_' + str(ll+1) + str(nn+1) + '.txt'
            #    fnm_ivec0 = dir_ivec + 'ivec0_' + str(ll+1) + str(nn+1) + '.txt'
            #    fnm_ivec1 = dir_ivec + 'ivec1_' + str(ll+1) + str(nn+1) + '.txt'
                #pdb.set_trace()
                #ivec_1[:,:,2*ll+nn] = np.loadtxt(fnm_ivec_1)
                #ivec0[:,:,2*ll+nn] = np.loadtxt(fnm_ivec0) 
                #ivec1[:,:,2*ll+nn] = np.loadtxt(fnm_ivec1)
            fnm_ivec_1 = dir_ivec + 'ivec_1_' + str(ll+1) + '.txt'
            fnm_ivec1 = dir_ivec + 'ivec1_' + str(ll+1) + '.txt'   
            ivec_1[:,:,ll] = np.loadtxt(fnm_ivec_1)
            ivec1[:,:,ll] = np.loadtxt(fnm_ivec1)
            if nvars == 3:
                fnm_ivec0 = dir_ivec + 'ivec0_' + str(ll+1) + '.txt'
                ivec0[:,:,ll] = np.loadtxt(fnm_ivec0) 

        if BasisCheck2 == 1:
            for jj in range(1,Nz):             
                for ii in range(0,Nx):
                   
                    if nvars == 3: 
                        ivec_1[ii,jj,0] = ivec_1[ii,jj,0]/t1[ii,jj]
                        ivec0[ii,jj,0] = ivec0[ii,jj,0]/t1[ii,jj]
                        ivec1[ii,jj,0] = ivec1[ii,jj,0]/t1[ii,jj]

            	        ivec_1[ii,jj,1] =  ivec_1[ii,jj,1]/t2
            	        ivec0[ii,jj,1] =  ivec0[ii,jj,1]/t2
            	        ivec1[ii,jj,1] =  ivec1[ii,jj,1]/t2

            	        ivec_1[ii,jj,2] =  ivec_1[ii,jj,2]/t3
            	        ivec0[ii,jj,2] =  ivec0[ii,jj,2]/t3
            	        ivec1[ii,jj,2] =  ivec1[ii,jj,2]/t3

                    if nvars == 2:
                        ivec_1[ii,jj,0] = ivec_1[ii,jj,0]/t1[ii,jj]
                        ivec1[ii,jj,0] = ivec1[ii,jj,0]/t1[ii,jj]

                        ivec_1[ii,jj,1] =  ivec_1[ii,jj,1]/t3
                        ivec1[ii,jj,1] =  ivec1[ii,jj,1]/t3

 
        #ivec_1 = ivec_1.view(complex)
        #ivec0 = ivec0.view(complex)
        #ivec1 = ivec1.view(complex)
        #pdb.set_trace()
    
    for vv in range(0,nvars):
        
        if nvars == 3:    
            fnc = np.multiply(sigma_1,ivec_1[:,:,vv]) + np.multiply(sigma0,ivec0[:,:,vv]) +\
		  np.multiply(sigma1,ivec1[:,:,vv])
        if nvars == 2:
            fnc = np.multiply(sigma_1,ivec_1[:,:,vv]) + np.multiply(sigma1,ivec1[:,:,vv])
        State[:,:,tt,vv] = cst.iFFT_FST(Nx,Nz,fnc)

        #f1 = np.multiply(sigma_1,ivec_1[:,:,vv]) 
        #f2 = np.multiply(sigma0,ivec0[:,:,vv])
        #f3 = np.multiply(sigma1,ivec1[:,:,vv])
        #State[:,:,tt,vv] = cst.iFFT_FST(Nx,Nz,f1) + cst.iFFT_FST(Nx,Nz,f2) + cst.iFFT_FST(Nx,Nz,f3)


#Analyse the Natural Basis of State:
if FindMainBasisParts == 1:

    if PlotMainBasisParts==1:
        fig = plt.figure(1, figsize=(width,height))
        grid = plt.GridSpec(1, 1, wspace=0., hspace=0.)
        ax1 = fig.add_subplot(grid[0,0])
        #ax1.set_ylim(-10,10)
        #ax1.set_ylim(10**(-10),100)
        ax1.set_ylabel('')
        ax1.set_xlabel(r'$t$ (s)')
 
        #linewidthvec = [3,3,3,3,3,3,3,0.2,0.2,0.2,0.2,0.2,0.2,0.2]
        #linecolorvec = ['k','b','c','g','y','m','r','k','b','c','g','y','m','r'] 
        #linecolorvec = ['black','silver','blue','cyan','lightblue','green','orange','gold','red','m'] 
        linecolorvec = ['black','silver','m','red','blue','cyan','lightblue','green','orange','gold']

    #Initialise objects required to find main basis parts by using 
    #a convergence method. The method compares power spectrums of State 
    #and a truncated natural basis for vertical structure of State.
    if ConvergePowerLimit == 1:
        if AegirTransforms == 0:
            Welch = 0
            signal_f = 1./dz                                            	#Sampling frequency (needs to be units 1/s for Welch method)

            if Welch == 0:
                SpectralCoef0    	= np.zeros((int(Nz/2.)+1,Nt))
                SpectralCoef1    	= np.zeros((int(Nz/2.)+1,Nt))
                freqvec         	= np.arange(int(Nz/2.)+1)*(1./Nz)       #assumes dz=1
                         	                                           	#Note array length is Nz/2 and so max freq. will be Nyquist freq.
                freqvec         	= freqvec*signal_f                      #uses actual signal frequency (Aegir vertical resolution)
                #freqvec        	= freqvec*2*np.pi                       #converts to angular frequency (rad/m) - but less intuitive 

    	    if Welch == 1:
                dnmntr 		= 50
                nperseg 	= int(Nz/dnmntr)
                SpectralCoef0 	= np.zeros((int(nperseg/2.)+1,Nt))
                SpectralCoef1 	= np.zeros((int(nperseg/2.)+1,Nt))
   
        if AegirTransforms == 1:
            SpectralCoef0	= np.zeros((Nz,Nt))
            SpectralCoef1       = np.zeros((Nz,Nt))
 
        PowerLimit  	= 10
        ErrorLimit  	= 70 
        epsilon     	= 100
        ts 		= 3

    if ConvergePowerLimit == 0: 
        PowerLimit 	= 5
        ErrorLimit 	= 1
        epsilon 	= 2 
        ts 		= 3
    
    #Iterate until error (denoted epsilon) is smaller than chosen limit (a % error):
    count0 = 0
    while epsilon > ErrorLimit:

        #Dynamic lists to store main basis parts during search for given error limit: 
        ks1 = []
        ns1 = []
        ks2 = []
        ns2 = []
        ks3 = []
        ns3 = []

        #re-set counters at start of each iteration:
        count1 = 0
        count2 = 0
        count3 = 0

        #Find main parts of Natural basis given some PowerLimit:
        for jj in range(0,Nz):
            for ii in range(0,int(Nx/2)):

                lambdaX = ii
                lambdaZ = jj

                f1 = abs(sigma_1_3D[ii,jj,:])
                if nvars == 3: f2 = abs(sigma0_3D[ii,jj,:])
                f3 = abs(sigma1_3D[ii,jj,:])

                if sum(f1) > PowerLimit:
                    if PlotMainBasisParts==1 and ConvergePowerLimit == 0:
                        label1 = 'sigma_1' + ', ' + '(' + str(lambdaX) + ', ' + str(lambdaZ) + ')'
                        #color1 = linecolorvec[count1]
                        #ax1.semilogy(t, f1, linewidth=2, linestyle='-', label=label1)
                        ax1.plot(t, f1, linewidth=2, linestyle='-', label=label1)

                    count1 = count1 + 1

                    ks1.append(lambdaX)
                    ns1.append(lambdaZ)

                if (nvars==3) and (sum(f2)>PowerLimit):
                    if PlotMainBasisParts==1 and ConvergePowerLimit == 0:
                        label2 = 'sigma0' + ', ' + '(' + str(lambdaX) + ', ' + str(lambdaZ) + ')'
                        #color2 = linecolorvec[count2]
                        #ax1.semilogy(t, f2, linewidth=2, linestyle='--', label=label2)
                        ax1.plot(t, f2, linewidth=2, linestyle='--', label=label2)

                    count2 = count2 + 1

                    ks2.append(lambdaX)
                    ns2.append(lambdaZ)

                FastMode2=1
                if (FastMode2==1) and (sum(f3)>PowerLimit):
                    #We need to consider these parts when converging to a PowerLimit, however, 
                    #there is no need to plot these sigma1 parts as they are just the negative of the sigma_1 ones:
                    PlotFastMode2 = 0
                    if PlotFastMode2 == 1 and ConvergePowerLimit == 0:
                        label3 = 'sigma1' + ', ' + '(' + str(lambdaX) + ', ' + str(lambdaZ) + ')'
                        #color3 = linecolorvec[count3]
                        #ax1.semilogy(t, f3, linewidth=2, linestyle=':', label=label3)
                        ax1.plot(t, f3, linewidth=2, linestyle=':', label=label3)

                    count3 = count3 + 1

                    ks3.append(lambdaX)
                    ns3.append(lambdaZ)

        print('count1: ', count1)
        print('count2: ', count2)
        print('count3: ', count3)

        #Estimate State using a truncated Natural basis:
        Estimate 	= np.zeros((Nx,Nz,Nt,nvars))
        epsilon_vec 	= np.zeros(Nt)
        MainFreqIdx0 	= np.zeros(Nt, dtype=np.int8)
        MainFreqIdx1 	= np.zeros(Nt, dtype=np.int8)

        sigma_1_main		= np.zeros((Nx,Nz,Nt))*1j
        sigma0_main		= np.zeros((Nx,Nz,Nt))*1j
        sigma1_main		= np.zeros((Nx,Nz,Nt))*1j
        sigma_1_main[ks1,ns1,:]		= sigma_1_3D[ks1,ns1,:]
        if nvars == 3: 
            sigma0_main[ks2,ns2,:]	= sigma0_3D[ks2,ns2,:]
        sigma1_main[ks3,ns3,:]		= sigma1_3D[ks3,ns3,:]
        
        for tt in range(ts, Nt):
            for vv in range(0,nvars):
                if nvars == 3:
                    fnc = np.multiply(sigma_1_main[:,:,tt],ivec_1[:,:,vv]) + \
                          np.multiply(sigma0_main[:,:,tt],ivec0[:,:,vv]) + np.multiply(sigma1_main[:,:,tt],ivec1[:,:,vv])
                if nvars == 2:
                    fnc = np.multiply(sigma_1_main[:,:,tt],ivec_1[:,:,vv]) + np.multiply(sigma1_main[:,:,tt],ivec1[:,:,vv])
                Estimate[:,:,tt,vv] = cst.iFFT_FST(Nx,Nz,fnc)

            if ConvergePowerLimit == 1: 

                if nvars == 3: varIdx = 2
                if nvars == 2: varIdx = 1

                if AegirTransforms == 0:
                    f0 = np.mean(State[:,:,tt,varIdx], axis=0)
                    f1 = np.mean(Estimate[:,:,tt,varIdx], axis=0)
                    #f0 = State[int(Nx/2.),:,tt,varIdx]
                    #f1 = Estimate[int(Nx/2.),:,tt,varIdx]

                    if Welch == 0:
                        #pdb.set_trace()
                        f0_hat = abs( np.fft.fft(f0) )
                        f1_hat = abs( np.fft.fft(f1) )
                        #power0 = f0_hat**2
                        #power1 = f1_hat**2
                        SpectralCoef0[:,tt] = f0_hat[0:int(Nz/2.)+1]
                        SpectralCoef1[:,tt] = f1_hat[0:int(Nz/2.)+1]

                    if Welch == 1:
                        freqvec, psd0 = welch( f0,
                                   fs=signal_f,              # sampling rate
                                   window='hanning',         # apply a Hanning window before taking the DFT
                                   nperseg=nperseg,          # compute periodograms of nperseg-long segments of ts
                                   detrend='constant')       # detrend ts by subtracting the mean
                        freqvec, psd1 = welch( f1,
                                   fs=signal_f,              # sampling rate
                                   window='hanning',         # apply a Hanning window before taking the DFT
                                   nperseg=nperseg,          # compute periodograms of nperseg-long segments of ts
                                   detrend='constant')       # detrend ts by subtracting the mean

                        SpectralCoef0[:,tt] = psd0
                        SpectralCoef1[:,tt] = psd1
          
                if AegirTransforms == 1:
                    f0_hat = abs(cst.FFT_FST(Nx,Nz,State[:,:,tt,varIdx]))
                    f1_hat = abs(cst.FFT_FST(Nx,Nz,Estimate[:,:,tt,varIdx]))
                    #pdb.set_trace()
                    SpectralCoef0[:,tt] = np.mean(f0_hat, axis=0)
                    SpectralCoef1[:,tt] = np.mean(f1_hat, axis=0)
 
                MainFreqIdx0[tt] = int(np.where( SpectralCoef0[:,tt] == np.max(SpectralCoef0[:,tt]) )[0])
                MainFreqIdx1[tt] = int(np.where( SpectralCoef1[:,tt] == np.max(SpectralCoef1[:,tt]) )[0])

                print('%.3f' % MainFreqIdx0[tt], '%.3f' % MainFreqIdx1[tt])
                if MainFreqIdx0[tt] != MainFreqIdx1[tt]: print('Main frequencies are not the same!')

                epsilon_vec[tt] = (SpectralCoef0[MainFreqIdx0[tt],tt] - \
       				   SpectralCoef1[MainFreqIdx0[tt],tt])/SpectralCoef0[MainFreqIdx0[tt],tt]*100

        if ConvergePowerLimit == 1:        
            epsilon = np.max(abs(epsilon_vec))
            print('% error: ', epsilon)
            print('PowerLimit: ', PowerLimit)
            print(' ')
            if (epsilon > ErrorLimit):
                PowerLimit /= 2.

        if ConvergePowerLimit == 0: epsilon = ErrorLimit
        count0 += 1


    if PlotMainBasisParts==1:    
        plt.xlabel(r'$t$ (s)')
        #plt.ylim(10**(-4),10**(2))
        plt.legend()
        plt.show()

    if PlotState_MainBasis == 1:
        #plt.contourf(Estimate[:,:,Nt-1].transpose(),20)
        if nvars==3: plt.contourf(Estimate[int(Nx/2.),:,:,2],50)
        if nvars==2: plt.contourf(Estimate[int(Nx/2.),:,:,1],50)
        plt.colorbar()
        plt.show()


if AnalyseMainBasisParts == 1:

    Nk_1 = len(ks1)
    Nk1 = len(ks3)

    if SpectralAnalysis == 1:
        Welch = 0
        signal_f = 1./dt	#Sampling frequency (needs to be units 1/s for Welch method)
 
        if Welch == 0:
            SpectralCoef0       = np.zeros((int(Nt/2.)+1,Nk_1))
            SpectralCoef1       = np.zeros((int(Nt/2.)+1,Nk1))
            freqvec             = np.arange(int(Nt/2.)+1)*(1./Nt)       #assumes dt=1
                                                                        #Note array length is Nt/2 and so max freq. will be Nyquist freq.
            freqvec             = freqvec*signal_f                      #uses actual signal frequency (Aegir timestep resolution)
            #freqvec            = freqvec*2*np.pi                       #converts to angular frequency (rad/s) - but less intuitive 

        if Welch == 1:
            dnmntr              = 2.
            nperseg             = int(Nt/dnmntr)
            SpectralCoef0       = np.zeros((int(nperseg/2.)+1,Nk_1))
            SpectralCoef1       = np.zeros((int(nperseg/2.)+1,Nk1))

    for i in range(0,Nk_1):

        if SpectralAnalysis == 1:
            f0 = sigma_1_3D[ks1[i],ns1[i],:]

            if Welch == 0:
                f0_hat = abs( np.fft.fft(f0) )
                #power0 = f0_hat**2
                SpectralCoef0[:,i] = f0_hat[0:int(Nt/2.)+1]

            if Welch == 1:
                freqvec, psd0 = welch( f0,
                                       fs=signal_f,              # sampling rate
                                       window='hanning',         # apply a Hanning window before taking the DFT
                                       nperseg=nperseg,          # compute periodograms of nperseg-long segments of ts
                                       detrend='constant')       # detrend ts by subtracting the mean
                SpectralCoef0[:,i] = psd0

        if DispersionRelation == 1:
            kmag = sqrt(kk[ks1[i]]**2 + kk_cosine[ns1[i]]**2)
            omega_k = abs(kk[ks1[i]])/kmag*sqrt(-g*(ct*bt-cs*bs)) 
            print(omega_k, sqrt(N2))

    for i in range(0,Nk1):

        if SpectralAnalysis == 1:
            f1 = sigma1_3D[ks3[i],ns3[i],:]

            if Welch == 0:
                f1_hat = abs( np.fft.fft(f1) )
                #power1 = f1_hat**2
                SpectralCoef1[:,i] = f1_hat[0:int(Nt/2.)+1]

            if Welch == 1:
                freqvec, psd1 = welch( f1,
                                       fs=signal_f,              # sampling rate
                                       window='hanning',         # apply a Hanning window before taking the DFT
                                       nperseg=nperseg,          # compute periodograms of nperseg-long segments of ts
                                       detrend='constant')       # detrend ts by subtracting the mean
                SpectralCoef1[:,i] = psd1

        if DispersionRelation == 1:
            kmag = sqrt(kk[ks3[i]]**2 + kk_cosine[ns3[i]]**2)
            omega_k = abs(kk[ks3[i]])/kmag*sqrt(-g*(ct*bt-cs*bs))     
            print(omega_k, sqrt(N2))

    if SpectralAnalysis == 1:
        plt.semilogy(freqvec,SpectralCoef0[:,0])
        plt.semilogy(freqvec,SpectralCoef0[:,1])
        plt.semilogy(freqvec,SpectralCoef0[:,2])
        plt.semilogy(freqvec,SpectralCoef0[:,3])
        plt.semilogy(freqvec,SpectralCoef0[:,4])
        plt.semilogy(freqvec,SpectralCoef0[:,5])
        plt.semilogy(freqvec,SpectralCoef0[:,6])
        plt.semilogy(freqvec,SpectralCoef0[:,7])
        #plt.semilogy(freqvec,SpectralCoef0[:,8])
        #plt.semilogy(freqvec,SpectralCoef0[:,9])
        plt.show()




if Resonances == 1:
    #From equation (308) in CodeEquations.pdf.
    
    #Find wavenumber space for our problem.
    #Note that it was necessary to round numbers before
    #applying relational operators to avoid serious rounding errors.

    triad_x_mask 	= np.zeros((Nx,Nx,Nx), dtype=bool)
    Ntriads_x		= np.zeros((Nx))
    for i in range(0,Nx):
        for i1 in range(0,Nx):
            for i2 in range(0,Nx):
                k1 = kk[i1]
                k2 = kk[i2]
                ksum = k1 + k2
                logical = round(ksum,5) == round(kk[i],5)
		if k1 == 0 and k2 == 0: logical = False 
                #logical = True
                triad_x_mask[i,i1,i2] = logical
        Ntriads_x[i] = sum(triad_x_mask[i,:,:])
    Ntriads_x = Ntriads_x.astype(int)

    #Plot wavenumber space for x-direction:
    #plt.plot(np.sort(kk),Ntriads_x)
    #plt.contourf(sum(triad_x_mask,0), 1, colors=['white','black'])
    #plt.colorbar()
    #plt.show()
    #pdb.set_trace()

    #This is how to get all points at once (but doesn't give you 
    #points for each k):
    #k1,k2 = np.meshgrid(kk,kk)
    #k1plusk2 = k1 + k2
    #mask_x = np.isin(k1plusk2, kk)     
    #idxs = np.where(mask_x==True)
    #np.array(idxs).shape 

    triad_z_mask        = np.zeros((Nz,Nz,Nz), dtype=bool)
    Ntriads_z           = np.zeros((Nz))
    for j in range(1,Nz):
        for j1 in range(1,Nz):
            for j2 in range(1,Nz):
                n1 = kk_cosine[j1]
                n2 = kk_cosine[j2]
                nsum1 = n1 + n2
                nsum2 = n1 - n2
                nsum3 = n2 - n1
                logical = round(nsum1,5) == round(kk_cosine[j],5) or\
                          round(nsum2,5) == round(kk_cosine[j],5) or\
                          round(nsum3,5) == round(kk_cosine[j],5)
                #logical = True   
                triad_z_mask[j,j1,j2] = logical
        Ntriads_z[j] = sum(triad_z_mask[j,:,:])
    Ntriads_z = Ntriads_z.astype(int)

    #Plot wavenumber space for z-direction:
    #plt.plot(np.sort(kk_cosine),Ntriads_z)
    #plt.imshow(sum(triad_z_mask,0), origin='lower', cmap='binary')
    #plt.contourf(kk_cosine,kk_cosine,triad_z_mask[0,:,:], 1, colors=['white','black'])
    #plt.colorbar()
    #plt.show()
    #pdb.set_trace()

    if LinearNonLinear == 1:

        #Choose time points:
        #nt = Nt
        nt = 20
        t = np.arange(nt)*dt

        #Fast wave averaging parameters:
        epsilon = 1.
        tau = 1.
        nt2 = (tau-0)/dt
        tvec = np.arange(nt2)*dt/epsilon

        #Initialise array to store result:
        sigma_t = np.zeros((Nx,Nz,nt))*1j

        si = 1
        ei = 2
        sj = 1
        ej = 2

        for tt in range(0,nt):
 
            print(" ")
            print("t",tt)
            #subset of k,n points:
        
            for i in range(si,ei):
                for j in range(sj,ej):

                    print('i,j',i,j)

                    k = kk[i]
                    n = kk_cosine[j]
                    kmag = sqrt(k**2 + n**2)
                    omega_k = abs(k)/kmag*sqrt(-g*(ct*bt-cs*bs))

            	    linear 	= -sqrt(g*cs*bs)*1j*k/kmag*(\
                            	  sigma_1_3D[i,j,tt]*ivec_1[i,j,0] +\
			   	  sigma1_3D[i,j,tt]*ivec1[i,j,0] )
                    diffusion  	= -kappas*kmag**2*(\
                            	  sigma_1_3D[i,j,tt]*ivec_1[i,j,1] +\
                                  sigma1_3D[i,j,tt]*ivec1[i,j,1] )

                    #Initialise non-linear term:
                    nonLinear = 0
                    tmp = 0

                    #Only loop over required points for some k,n:
                    idxs_triads_x = np.array(np.where(triad_x_mask[i,:,:] == True))
                    idxs_triads_z = np.array(np.where(triad_z_mask[j,:,:] == True))

                    count0 = 0
                    for count_i in range(0,len(idxs_triads_x[0,:])):
                        for count_j in range(0,len(idxs_triads_z[0,:])):

                            print(count0)

                            i1 = idxs_triads_x[1,count_i]
                            i2 = idxs_triads_x[0,count_i]
                            j1 = idxs_triads_z[1,count_j]
 			    j2 = idxs_triads_z[0,count_j]

                            k1 = kk[i1]
                            k2 = kk[i2]
                            n1 = kk_cosine[j1]
                            n2 = kk_cosine[j2]
                        
                            kmag1 = sqrt(k1**2 + n1**2)
			    kmag2 = sqrt(k2**2 + n2**2)
                            omega_k1 = abs(k1)/kmag1*sqrt(-g*(ct*bt-cs*bs))
                            omega_k2 = abs(k2)/kmag2*sqrt(-g*(ct*bt-cs*bs))

                            if (kmag1 != 0): 

			        tmp += \
			        (sigma_1_3D[i1,j1,tt]*ivec_1[i1,j1,0] + sigma1_3D[i1,j1,tt]*ivec1[i1,j1,0])*\
                                (sigma_1_3D[i2,j2,tt]*ivec_1[i2,j2,1] + sigma1_3D[i2,j2,tt]*ivec1[i2,j2,1])*\
			        (1j*k1*n2-1j*k2*n1)/kmag1

                            #(ivec_1[i2,j2,0]*ivec_1[i1,j1,1]/kmag2/sqrt(g*cs*bs) - ivec_1[i1,j1,0]*ivec_1[i2,j2,1]/kmag1/sqrt(g*cs*bs)) +\
                            #1./tau*sum(exp(-1j*(-omega_k1-omega_k2+omega_k)*tvec)) +\
                            #exp(-1j*(-omega_k1-omega_k2+omega_k)*t[tt]) +\
			    #alpha1 = -1, alpha2 = +1:
			    #sigma_1_3D[i1,j1,tt]*sigma1_3D[i2,j2,tt]*\
                            #(ivec1[i2,j2,0]*ivec_1[i1,j1,1]/kmag2/sqrt(g*cs*bs) - ivec_1[i1,j1,0]*ivec1[i2,j2,1]/kmag1/sqrt(g*cs*bs)) +\
                            #1./tau*sum(exp(-1j*(-omega_k1+omega_k2+omega_k)*tvec)) +\
                            #exp(-1j*(-omega_k1+omega_k2+omega_k)*t[tt]) +\
			    #alpha1 = +1, alpha2 = -1:
                            #sigma1_3D[i1,j1,tt]*sigma_1_3D[i2,j2,tt]*\
                            #(ivec_1[i2,j2,0]*ivec1[i1,j1,1]/kmag2/sqrt(g*cs*bs) - ivec1[i1,j1,0]*ivec_1[i2,j2,1]/kmag1/sqrt(g*cs*bs)) +\
                            #1./tau*sum(exp(-1j*(omega_k1-omega_k2+omega_k)*tvec)) +\
                            #exp(-1j*(omega_k1-omega_k2+omega_k)*t[tt]) +\
 			    #alpha1 = +1, alpha2 = +1:
			    #sigma1_3D[i1,j1,tt]*sigma1_3D[i2,j2,tt]*\
                            #(ivec1[i2,j2,0]*ivec1[i1,j1,1]/kmag2/sqrt(g*cs*bs) - ivec1[i1,j1,0]*ivec1[i2,j2,1]/kmag1/sqrt(g*cs*bs)) \
                            #1./tau*sum(exp(-1j*(omega_k1+omega_k2+omega_k)*tvec)) +\
                            #exp(-1j*(omega_k1+omega_k2+omega_k)*t[tt]) +\

                            #alpha = +1:
                            #alpha1 = -1, alpha2 = -1:
                            #sigma_1_3D[i1,j1,tt]*sigma_1_3D[i2,j2,tt]*\
                            #(ivec_1[i2,j2,0]*ivec_1[i1,j1,1]-ivec_1[i1,j1,0]*ivec_1[i2,j2,1]) +\
                            #1./tau*sum(exp(-1j*(-omega_k1-omega_k2-omega_k)*tvec)) +\
                            #exp(-1j*(-omega_k1-omega_k2-omega_k)*t[tt]) +\
                            #alpha1 = -1, alpha2 = +1:
                            #sigma_1_3D[i1,j1,tt]*sigma1_3D[i2,j2,tt]*\
                            #(ivec1[i2,j2,0]*ivec_1[i1,j1,1]-ivec_1[i1,j1,0]*ivec1[i2,j2,1]) +\
                            #exp(-1j*(-omega_k1+omega_k2-omega_k)*t[tt]) +\
                            #alpha1 = +1, alpha2 = -1:
                            #sigma1_3D[i1,j1,tt]*sigma_1_3D[i2,j2,tt]*\
                            #(ivec_1[i2,j2,0]*ivec1[i1,j1,1]-ivec1[i1,j1,0]*ivec_1[i2,j2,1]) +\
                            #1./tau*sum(exp(-1j*(omega_k1-omega_k2-omega_k)*tvec)) +\
                            #exp(-1j*(omega_k1-omega_k2-omega_k)*t[tt]) +\
                            #alpha1 = +1, alpha2 = +1:
                            #sigma1_3D[i1,j1,tt]*sigma1_3D[i2,j2,tt]*\
                            #(ivec1[i2,j2,0]*ivec1[i1,j1,1]-ivec1[i1,j1,0]*ivec1[i2,j2,1])\
                            #1./tau*sum(exp(-1j*(omega_k1+omega_k2-omega_k)*tvec))
                            #exp(-1j*(omega_k1+omega_k2-omega_k)*t[tt])
			    #) 

                                count0 += 1       
                                #print(linear, nonLinear, diffusion)

                    #nonLinear = tmp 
                    sigma_t[i,j,tt] = linear + nonLinear + diffusion


        #check values:
        dsigma_dt = np.zeros((Nx,Nz,nt))*1j

        for tt in range(1,nt-1):
            for i in range(si,ei):
                for j in range(sj,ej):

                    dsigma_dt[i,j,tt] = (sigma_1_3D[i,j,tt+1] - sigma_1_3D[i,j,tt-1])/(2*dt) * ivec_1[i,j,1] +\
                                        (sigma1_3D[i,j,tt+1] - sigma1_3D[i,j,tt-1])/(2*dt) * ivec1[i,j,1]


        plt.plot(dsigma_dt[1,1,:], '-ok')
        plt.plot(sigma_t[1,1,:], '-ob')
        plt.ylim((-1,1))
        plt.show()


    if NearResonanceSearch == 1:
        epsilon = 0.1
        domega = []
        alphavec = [-1,1]
        Nk = len(ks1+ks3)

        domegaArr = np.ones((Nk,Nx,Nz,Nz,2,2))
        #pdb.set_trace()

        #Define, k, n  and alpha based on main basis elements search above:
        for v in range(0,1):
           
            count0 = 0
 
            if v < len(ks1):
                i = ks1[v]
                j = ns1[v]
                alpha = 0
            else:
                i = ks3[v]
                j = ns3[v]
                alpha = 1
          
            k = kk[i]
            n = kk_cosine[j] 
            kmag = sqrt(k**2 + n**2)
            omega_k = alphavec[alpha]*abs(k)/kmag*sqrt(-g*(ct*bt-cs*bs))

            #Only loop over required points for some k,n:
            idxs_triads_x = np.array(np.where(triad_x_mask[i,:,:] == True))
            idxs_triads_z = np.array(np.where(triad_z_mask[j,:,:] == True))

            for count_i in range(0,len(idxs_triads_x[0,:])):
                for count_j in range(0,len(idxs_triads_z[0,:])):

                    i1 = idxs_triads_x[0,count_i]
                    i2 = idxs_triads_x[1,count_i]
                    j1 = idxs_triads_z[0,count_j]
                    j2 = idxs_triads_z[1,count_j]

                    k1 = kk[i1]
                    k2 = kk[i2]
                    n1 = kk_cosine[j1]
                    n2 = kk_cosine[j2]

                    #for i1 in range(0,Nx):
                    #    k1 = kk[i1]
                    #    k2 = k - k1

                    #    for j1 in range(0,Nz):
                    #        n1 = kk_cosine[j1]
                    #        for j2 in range(0,Nz):
                    #            n2 = kk_cosine[j2]
     		
                    #            ksum = k1 + k2
                    #            logical_x = round(ksum,5) == round(k,5)		
 		    #		nsum1 = n1 + n2
                    #	 	nsum2 = n1 - n2
                    #		nsum3 = n2 - n1
                    #		logical_z = round(nsum1,5) == round(kk_cosine[j],5) or\
                    #      	            round(nsum2,5) == round(kk_cosine[j],5) or\
                    #      		    round(nsum3,5) == round(kk_cosine[j],5)
                    #
                    #            if logical_x == True and logical_z == True:                                   

                    kmag1 = sqrt(k1**2 + n1**2)
                    kmag2 = sqrt(k2**2 + n2**2)

                    for alpha1 in range(0,2):
                        for alpha2 in range(0,2):
                                        
                            omega_k1 = alphavec[alpha1]*abs(k1)/kmag1*sqrt(-g*(ct*bt-cs*bs))
                            omega_k2 = alphavec[alpha2]*abs(k2)/kmag2*sqrt(-g*(ct*bt-cs*bs))

                            tmp = abs(omega_k1 + omega_k2 - omega_k)
			    if tmp <= epsilon:
                                domegaArr[v,i1,j1,j2,alpha1,alpha2] = tmp
                                domega.append(tmp)

			    count0 += 1
                            print(count0)
pdb.set_trace()
